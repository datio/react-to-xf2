// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"os"
)

func (o *Import) migrate() error {
	// Create all those tables that would autocommit. Commit and then re-initiate a txn.
	if err = o.createUserCache(); err != nil {
		return err
	}

	if err = o.createAvatarCache(); err != nil {
		return err
	}

	if err = o.createMediaUsageCache(); err != nil {
		return err
	}

	if err = o.createMediaCache(); err != nil {
		return err
	}

	if err = o.createThreadCache(); err != nil {
		return err
	}

	if err = o.createPostCache(); err != nil {
		return err
	}

	if err = o.createConversationMessageCache(); err != nil {
		return err
	}

	err = xf2.Txn.Commit()
	if err != nil {
		return err
	}
	xf2.Txn, err = xf2.Db.Begin()
	if err != nil {
		return err
	}

	// Warmup.
	if err = o.preparePhraseLanguages(); err != nil {
		return err
	}

	if err = o.setBoardOptions(); err != nil {
		return err
	}

	// Import custom user fields.
	if err = o.importCustomUserFields(); err != nil {
		return err
	}

	// Import user group definitions.
	if err = o.importUserGroups(); err != nil {
		return err
	}

	// Import categories and forums.
	if err = o.importNodes(); err != nil {
		return err
	}

	// Set usergroup permissions.
	if err = o.importPermissions(); err != nil {
		return err
	}

	// Import user-related data.
	if err = o.importUsers(); err != nil {
		return err
	}

	// import media-related data.
	if err = o.importMedia(); err != nil {
		return err
	}

	// import forum post-related data.
	if err = o.importPosts(); err != nil {
		return err
	}

	// import personal conversation-related data.
	if err = o.importConversations(); err != nil {
		return err
	}

	return xf2.Txn.Commit()
}

type phrase struct {
	PhraseID      int
	LanguageID    int
	Title         string
	PhraseText    string
	GlobalCache   bool
	AddonID       string
	VersionID     int
	VersionString string

	PhraseGroup string
}

var phraseLanguageIDs = []int{0} // Includes the master language ID (0).

func (o *Import) preparePhraseLanguages() error {
	var (
		languageIDRows *sql.Rows
		languageID     int
	)

	languageIDRows, err = xf2.Txn.Query(`
        SELECT language_id
        FROM xf_language
        `)
	if err != nil {
		return err
	}

	for languageIDRows.Next() {
		if err = languageIDRows.Scan(
			&languageID,
		); err != nil {
			return err
		}
		phraseLanguageIDs = append(phraseLanguageIDs, languageID)
	}

	return err
}

func (o *Import) importPhrases(phrases []*phrase) error {
	var res sql.Result
	var phraseID int64

	for _, p := range phrases {
		res, err = xf2.Txn.Exec(`
            INSERT IGNORE INTO xf_phrase
            SET language_id = ?,
                title = ?,
                phrase_text = ?
        `, p.LanguageID, p.Title, p.PhraseText,
		)
		if err != nil {
			return err
		}
		phraseID, err = res.LastInsertId()
		if err != nil {
			return err
		}

		if phraseID == 0 {
			continue
		}

		for _, languageID := range phraseLanguageIDs {
			_, err = xf2.Txn.Exec(`
                INSERT IGNORE INTO xf_phrase_compiled
                SET language_id = ?,
                    title = ?,
                    phrase_text = ?
            `, languageID, p.Title, p.PhraseText,
			)
			if err != nil {
				return err
			}

			_, err = xf2.Txn.Exec(`
                INSERT IGNORE INTO xf_phrase_map
                SET language_id = ?,
                    title = ?,
                    phrase_id = ?,
                    phrase_group = ?
            `, languageID, p.Title, phraseID, p.PhraseGroup,
			)
			if err != nil {
				return err
			}
		}
	}

	return err
}

func (o *Import) setBoardOptions() error {
	var updateOptionsCache bool

	var messagesPerPage int
	err = xf2.Txn.QueryRow(`
        SELECT option_value
        FROM xf_option
        WHERE option_id = 'messagesPerPage'
    `).Scan(&messagesPerPage)
	if err != nil {
		return AddErrorInfo(err, "select from xf_option messagesPerPage")
	}

	if messagesPerPage != 25 {
		_, err = xf2.Txn.Exec(`
            UPDATE xf_option
            SET option_value = 25
            WHERE option_id = 'messagesPerPage'
        `)
		if err != nil {
			return AddErrorInfo(err, "update xf_option")
		}

		updateOptionsCache = true

		fmt.Println("xenforo option 'messagesPerPage' set to 25")
	}

	var bookmarksPerPage int
	err = xf2.Txn.QueryRow(`
            SELECT option_value
            FROM xf_option
            WHERE option_id = 'th_bookmarksPerPage_bookmarks'
        `).Scan(&bookmarksPerPage)
	if err != nil {
		return AddErrorInfo(err, "select from xf_option th_bookmarksPerPage_bookmarks")
	}

	if bookmarksPerPage != 25 {
		_, err = xf2.Txn.Exec(`
            UPDATE xf_option
            SET option_value = 25
            WHERE option_id = 'th_bookmarksPerPage_bookmarks'
        `)
		if err != nil {
			return err
		}

		updateOptionsCache = true

		fmt.Println("xenforo option 'bookmarksPerPage' set to 25")
	}

	var postEditHistoryDays string
	err = xf2.Txn.QueryRow(`
            SELECT option_value
            FROM xf_option
            WHERE option_id = 'editHistory'
        `).Scan(&postEditHistoryDays)
	if err != nil {
		return AddErrorInfo(err, "select from xf_option editHistory")
	}

	if postEditHistoryDays != `{"enabled":"1","length":"0"}` {
		_, err = xf2.Txn.Exec(`
            UPDATE xf_option
            SET option_value = '{"enabled":"1","length":"0"}'
            WHERE option_id = 'editHistory'
        `)
		if err != nil {
			return err
		}

		updateOptionsCache = true

		fmt.Println("xenforo option 'editHistory' set to 0 (do not prune)")
	}

	var changeLogLengthDays int
	err = xf2.Txn.QueryRow(`
            SELECT option_value
            FROM xf_option
            WHERE option_id = 'changeLogLength'
        `).Scan(&changeLogLengthDays)
	if err != nil {
		return AddErrorInfo(err, "select from xf_option changeLogLength")
	}

	if changeLogLengthDays != 0 {
		_, err = xf2.Txn.Exec(`
            UPDATE xf_option
            SET option_value = 0
            WHERE option_id = 'changeLogLength'
        `)
		if err != nil {
			return err
		}

		updateOptionsCache = true

		fmt.Println("xenforo option 'changeLogLength' set to 0 (do not prune)")
	}

	if updateOptionsCache {
		_, err = xf2.Txn.Exec(`
            DELETE from xf_data_registry
            WHERE data_key = 'options'
        `)
		if err != nil {
			return err
		}
	}

	return err
}

func (o *Import) createEmptyDirIfNotExists(dirPath string, createEmptyHTLMFile bool) error {
	_, err = os.Stat(dirPath)
	if os.IsNotExist(err) {
		if err = os.MkdirAll(dirPath, 0755); err != nil {
			return err
		}
	} else {
		return nil
	}

	// XenForo index.html files include a space.
	if createEmptyHTLMFile {
		err = ioutil.WriteFile(dirPath+"index.html", []byte(" "), 0644)
	}

	return err
}
