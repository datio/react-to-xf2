// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

import (
	"crypto/md5"
	"database/sql"
	"errors"
	"fmt"
	"html"
	"io"
	"io/ioutil"
	"math"
	"net/url"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"image"
	"image/gif"
	"image/jpeg"
	"image/png"

	_ "golang.org/x/image/bmp"  // Understand BMPs.
	_ "golang.org/x/image/webp" // Understand WEBPs.

	"github.com/Machiel/slugify"
	"github.com/cespare/xxhash"
	"github.com/elgs/gostrgen"
	"github.com/oliamb/cutter"
	"github.com/rkravchik/resize"
	"github.com/xtgo/set"
)

// mediaUsage contains the privacy settings for the media items.
type mediaUsage struct {
	MediaID     int
	MediaType   string
	MediaHash   string
	PostID      int
	PostType    string // content_type: conversation, forum
	ViewPrivacy string // public, private, shared
	ViewUsers   []int
}

type mediaUsageKey struct {
	MediaID   int
	MediaHash string
}

type album struct {
	AlbumID     int
	OldAlbumID  int
	Title       string
	UserID      int
	ViewPrivacy string
	ViewUsers   []int
}

type albumUsageKey struct {
	UserID    int
	AlbumID   int
	ViewUsers string
}

type mediaDocument struct {
	DocumentID          int
	FolderID            int
	LibraryID           int
	Filename            string
	Extension           string
	Name                string
	Md5key              string
	Description         string
	Data                string // Might contain php serialized data such as height/width, exif etc. Many records are truncated.
	Size                int
	Deleted             bool
	RecordTimestampUnix int64
	Album               *album
}

func (o *Import) importMedia() error {
	doImport, ok := o.Config.Get("mfxf2.import_motorforum_media").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_motorforum_media' in config")
	}

	if !doImport {
		return nil
	}

	// Excplicitly commit any previous transaction.
	err = xf2.Txn.Commit()
	if err != nil {
		return err
	}
	xf2.Txn, err = xf2.Db.Begin()
	if err != nil {
		return err
	}

	mediaUsageMap := map[mediaUsageKey]*mediaUsage{}

	privateByDefault, ok := o.Config.Get("mfxf2.media_albums_private_by_default").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.media_albums_private_by_default' in config")
	}

	if privateByDefault {
		err = o.prepareMediaUsageMap(mediaUsageMap)
		if err != nil {
			return err
		}
	}

	albumMap := map[albumUsageKey]*album{}

	err = o.prepareAlbumMap(albumMap, mediaUsageMap, privateByDefault)
	if err != nil {
		return err
	}

	return o.insertAlbumsAndMedia()
}

func (o *Import) createMediaUsageCache() error {
	_, err = xf2.Txn.Exec(`
        CREATE TABLE IF NOT EXISTS xf_datio_mfxf2_media_usage_cache (
            media_id INT NOT NULL,
            media_type ENUM('album', 'media') NOT NULL,
            media_hash VARCHAR(32) NOT NULL DEFAULT '',
            post_id INT NOT NULL,
            post_type ENUM('forum', 'conversation') NOT NULL,
            view_privacy ENUM('public', 'private', 'shared') NOT NULL,
            view_users MEDIUMBLOB NOT NULL,
            PRIMARY KEY (media_id, media_type)
        )
    `)

	return err
}

var messageMediaRe = regexp.MustCompile(`(?i)\[doclib\s*=\s*([0-9]+)\s*,\s*([a-z0-9]{32})\s*,\s*[0-9]+.*]|http://(?:www\.)motor-forum\.nl/download\.php/download_document/([0-9]+)/([a-z0-9]{32})`)

func (o *Import) prepareMediaUsageMap(mum map[mediaUsageKey]*mediaUsage) error {
	// Determines with whom the private media documents should be shared.
	if err = o.prepareConversationParticipantsMap(); err != nil {
		return err
	}

	var lastPrivateMessageID, lastForumMessageID, lastPrivateMessageNewID, lastForumMessageNewID int
	lastPrivateMessageID, lastForumMessageID, err = o.prepareMediaUsageMapFromCache(mum)
	if err != nil {
		return err
	}

	var (
		messageRows  *sql.Rows
		messageID    int
		discussionID int
		content      string
	)

	messageRows, err = react.Txn.Query(`
        SELECT pm.MessageID,
               pm.DiscussionID,
               pmr.Content
        FROM F_Privatemessagesraw pmr
        INNER JOIN F_Privatemessages pm ON pmr.MessageID = pm.MessageID
        WHERE pm.MessageID > ?
          AND (pmr.Content LIKE '%download_document%' OR pmr.Content LIKE '%doclib%')
        ORDER BY pm.MessageID
    `, lastPrivateMessageID)
	if err != nil {
		return err
	}

	fmt.Print("resolving media privacy (private).")
	var countThousand int
	var countTen int

	for messageRows.Next() {
		countThousand++
		if countThousand == 1000 {
			countTen++
			if countTen == 10 {
				fmt.Print("\nresolving media privacy (private)")
				countTen = 0
			}

			fmt.Print(".")
			countThousand = 1
		}

		err = messageRows.Scan(&messageID, &discussionID, &content)
		if err != nil {
			return err
		}

		messageItemsSubmatches := messageMediaRe.FindAllStringSubmatch(content, -1)
		if len(messageItemsSubmatches) == 0 {
			continue
		}

		for _, messageItemsSubmatch := range messageItemsSubmatches {
			var mediaID int
			if len(messageItemsSubmatch[1]) > 0 {
				mediaID, err = strconv.Atoi(messageItemsSubmatch[1])
				if err != nil {
					return err
				}
			} else if len(messageItemsSubmatch[3]) > 0 {
				mediaID, err = strconv.Atoi(messageItemsSubmatch[3])
				if err != nil {
					return err
				}
			} else {
				continue
			}

			var mediaHash string
			if len(messageItemsSubmatch[2]) > 0 {
				mediaHash = messageItemsSubmatch[2]
			} else if len(messageItemsSubmatch[4]) > 0 {
				mediaHash = messageItemsSubmatch[4]
			} else {
				continue
			}

			lastPrivateMessageNewID = messageID

			viewUsers, ok := conversationParticipantsMap[discussionID]
			if !ok {
				viewUsers = make([]int, 0)
			}

			key := mediaUsageKey{MediaID: mediaID, MediaHash: mediaHash}
			mu, ok := mum[key]
			if ok {
				if mu.ViewPrivacy == "public" {
					continue
				} else if mu.ViewPrivacy == "shared" {
					viewUsers = append(viewUsers, mu.ViewUsers...)

					data := sort.IntSlice(viewUsers)
					n := set.Uniq(data)
					viewUsers = viewUsers[:n]
				}
			}

			mum[key] = &mediaUsage{
				MediaID:     mediaID,
				MediaType:   "media",
				MediaHash:   mediaHash,
				PostID:      lastPrivateMessageNewID,
				PostType:    "conversation",
				ViewPrivacy: "shared",
				ViewUsers:   viewUsers,
			}
		}
	}

	fmt.Print("\nplease wait while forum posts are being prepared...\n")

	messageRows, err = react.Txn.Query(`
        SELECT MessageID,
               Content
        FROM F_Messagesraw
        WHERE MessageID > ?
          AND (Content LIKE '%download_document%' OR Content LIKE '%doclib%')
        ORDER BY MessageID
    `, lastForumMessageID)
	if err != nil {
		return err
	}

	fmt.Print("resolving media privacy (public).")
	countThousand = 0
	countTen = 0

	for messageRows.Next() {
		countThousand++
		if countThousand == 1000 {
			countTen++
			if countTen == 10 {
				fmt.Print("\nresolving media privacy (public)")
				countTen = 0
			}

			fmt.Print(".")
			countThousand = 1
		}

		err = messageRows.Scan(&messageID, &content)
		if err != nil {
			return err
		}

		messageItemsSubmatches := messageMediaRe.FindAllStringSubmatch(content, -1)
		if len(messageItemsSubmatches) == 0 {
			continue
		}

		for _, messageItemsSubmatch := range messageItemsSubmatches {
			var mediaID int
			if len(messageItemsSubmatch[1]) > 0 {
				mediaID, err = strconv.Atoi(messageItemsSubmatch[1])
				if err != nil {
					return err
				}
			} else if len(messageItemsSubmatch[3]) > 0 {
				mediaID, err = strconv.Atoi(messageItemsSubmatch[3])
				if err != nil {
					return err
				}
			} else {
				continue
			}

			var mediaHash string
			if len(messageItemsSubmatch[2]) > 0 {
				mediaHash = messageItemsSubmatch[2]
			} else if len(messageItemsSubmatch[4]) > 0 {
				mediaHash = messageItemsSubmatch[4]
			} else {
				continue
			}

			lastForumMessageNewID = messageID

			key := mediaUsageKey{MediaID: mediaID, MediaHash: mediaHash}
			_, ok := mum[key]
			if ok {
				mum[key].ViewPrivacy = "public"
				if len(mum[key].ViewUsers) > 0 {
					mum[key].ViewUsers = mum[key].ViewUsers[:0]
				}
			} else {
				mum[key] = &mediaUsage{
					MediaID:     mediaID,
					MediaType:   "media",
					MediaHash:   mediaHash,
					PostID:      lastForumMessageNewID,
					PostType:    "forum",
					ViewPrivacy: "public",
				}
			}
		}
	}
	fmt.Print("\n")

	for _, mu := range mum {
		if mu.PostType == "conversation" && mu.PostID <= lastPrivateMessageID {
			continue
		}
		if mu.PostType == "forum" && mu.PostID <= lastForumMessageID {
			continue
		}

		if mu.MediaID > 4294967295 {
			continue
		}

		var viewUsersStr []string
		for _, viewUserInt := range mu.ViewUsers {
			viewUsersStr = append(viewUsersStr, strconv.Itoa(viewUserInt))
		}
		viewUsersJoined := strings.Join(viewUsersStr, ",")

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_datio_mfxf2_media_usage_cache
            SET media_id = ?,
                media_type = ?,
                media_hash = ?,
                post_id = ?,
                post_type = ?,
                view_privacy = ?,
                view_users = ?
            ON DUPLICATE KEY
            UPDATE media_hash = ?,
                   post_id = ?,
                   post_type = ?,
                   view_privacy = ?,
                   view_users = ?
        `,
			mu.MediaID, mu.MediaType, mu.MediaHash, mu.PostID, mu.PostType, mu.ViewPrivacy, viewUsersJoined,
			mu.MediaHash, mu.PostID, mu.PostType, mu.ViewPrivacy, viewUsersJoined,
		)

		if err != nil {
			return AddErrorInfo(err, "insert into xf_datio_mfxf2_media_usage_cache")
		}
	}

	return err
}

func (o *Import) prepareMediaUsageMapFromCache(mum map[mediaUsageKey]*mediaUsage) (int, int, error) {
	var lastPrivateMessageID, lastForumMessageID int
	var (
		mediaUsageCacheRows *sql.Rows
		mediaID             int
		mediaType           string
		mediaHash           string
		postID              int
		postType            string
		viewPrivacy         string
		viewUsers           string
	)

	mediaUsageCacheRows, err = xf2.Txn.Query(`
        SELECT media_id,
               media_type,
               media_hash,
               post_id,
               post_type,
               view_privacy,
               view_users
        FROM xf_datio_mfxf2_media_usage_cache
        ORDER BY post_id, media_type, media_id
    `)
	if err != nil {
		return 0, 0, err
	}

	for mediaUsageCacheRows.Next() {
		err = mediaUsageCacheRows.Scan(&mediaID, &mediaType, &mediaHash, &postID, &postType, &viewPrivacy, &viewUsers)
		if err != nil {
			return 0, 0, err
		}

		var viewUsersInt []int
		for _, viewUserStr := range strings.Split(viewUsers, ",") {
			viewUserInt, _ := strconv.Atoi(viewUserStr)

			if viewUserInt != 0 {
				viewUsersInt = append(viewUsersInt, viewUserInt)
			}
		}

		key := mediaUsageKey{MediaID: mediaID, MediaHash: mediaHash}
		mum[key] = &mediaUsage{
			MediaID:     mediaID,
			MediaType:   mediaType,
			MediaHash:   mediaHash,
			PostID:      postID,
			PostType:    postType,
			ViewPrivacy: viewPrivacy,
			ViewUsers:   viewUsersInt,
		}

		if postType == "conversation" {
			lastPrivateMessageID = postID
		} else if postType == "forum" {
			lastForumMessageID = postID
		}
	}

	return lastPrivateMessageID, lastForumMessageID, err
}

type privateFolder struct {
	FolderID      int
	ParentFolders []int
	UserID        int
	Title         string
}

var mediaDocuments []*mediaDocument
var folderIDToTagsMap = map[int][]*tag{}
var albumIDToMediaIDsMap = map[int][]int{}

const timestampLayout = "2006-01-02 15:04:05" // For parsing SQL timestamps.
// var invalidUrlEncodeStrPreReplace = strings.NewReplacer("%CB", "Ë", "%e5", "å", "%20", "Ȁ", "%fb", "ﬀ", "%DF", "ß", "E9", "é")

func (o *Import) prepareAlbumMap(aum map[albumUsageKey]*album, mum map[mediaUsageKey]*mediaUsage, privateByDefault bool) error {
	var (
		privateFolderRows *sql.Rows
		folderID          int
		parentID          int
		userID            int
		title             string
	)

	privateFolderRows, err = react.Txn.Query(`
        SELECT FolderID,
               ParentID,
               UserID,
               Title
        FROM F_PrivateFolders
        WHERE FolderID > 100 AND LENGTH(Title) <= 100
        ORDER BY FolderID
        `)
	if err != nil {
		return AddErrorInfo(err, "select from F_PrivateFolders")
	}

	var privateFolders []*privateFolder

	parentFolderMap := map[int]int{}

	for privateFolderRows.Next() {
		err = privateFolderRows.Scan(&folderID, &parentID, &userID, &title)
		if err != nil {
			return err
		}

		privateFolders = append(privateFolders, &privateFolder{
			FolderID:      folderID,
			ParentFolders: []int{parentID},
			UserID:        userID,
			Title:         html.UnescapeString(title),
		})

		parentFolderMap[folderID] = parentID
	}

	for _, folder := range privateFolders {
		for _, parentFolderID := range folder.ParentFolders {
			if parentFolderID < 14 {
				continue // No need to iterate over the default folders.
			}

			parentFolderIDToAdd, ok := parentFolderMap[parentFolderID]
			if !ok {
				continue
			}

			if parentFolderIDToAdd >= 14 {
				folder.ParentFolders = append(folder.ParentFolders, parentFolderIDToAdd)
			}

			for {
				parentFolderIDToAdd, ok = parentFolderMap[parentFolderIDToAdd]
				if !ok {
					break
				}

				if parentFolderIDToAdd >= 14 {
					folder.ParentFolders = append(folder.ParentFolders, parentFolderIDToAdd)
				}
			}
		}
	}

	err = o.prepareTags()
	if err != nil {
		return err
	}

	for _, folder := range privateFolders {
		if strings.Contains(folder.Title, "Documenten") {
			continue
		}

		slugifiedTitle := slugify.Slugify(folder.Title)

		t, ok := tagTitleToTagMap[slugifiedTitle]
		if !ok {
			_, err = o.insertTag(&tag{
				TagTitle:    folder.Title,
				TagURL:      slugifiedTitle,
				LastUseDate: int(currentTimeEpoch),
			})
			if err != nil {
				return err
			}

			t = tagTitleToTagMap[slugifiedTitle]
		}

		_, ok = folderIDToTagsMap[folder.FolderID]
		if !ok {
			folderIDToTagsMap[folder.FolderID] = []*tag{t}
		} else {
			folderIDToTagsMap[folder.FolderID] = append(folderIDToTagsMap[folder.FolderID], t)
		}

		for _, parentFolderID := range folder.ParentFolders {
			_, ok = folderIDToTagsMap[parentFolderID]
			if ok {
				folderIDToTagsMap[folder.FolderID] = append(folderIDToTagsMap[folder.FolderID], folderIDToTagsMap[parentFolderID]...)
			}
		}
	}

	// Prepare albums.
	// ---------------- todo: refactor.
	// Prepare media.

	var (
		documentRows    *sql.Rows
		documentID      int
		libraryID       int
		filename        string
		extension       string
		name            string
		md5key          string
		description     string
		data            string
		size            int
		deleted         bool
		recordtimestamp string
	)

	userIDToAlbumCountMap := map[int]int{}

	sharedAlbumTitleTemplate, ok := o.Config.Get("mfxf2.shared_album_title").(string)
	if !ok {
		return errors.New("could not find value of 'mfxf2.shared_album_title' in config")
	}
	publicAlbumTitleTemplate, ok := o.Config.Get("mfxf2.public_album_title").(string)
	if !ok {
		return errors.New("could not find value of 'mfxf2.public_album_title' in config")
	}
	privateAlbumTitleTemplate, ok := o.Config.Get("mfxf2.private_album_title").(string)
	if !ok {
		return errors.New("could not find value of 'mfxf2.private_album_title' in config")
	}

	documentRows, err = react.Txn.Query(`
        SELECT F_Documents.DocumentID,	
               COALESCE(F_DocumentSets.FolderID, '0') AS FolderID,
               F_Documents.LibraryID,
               F_Documents.Filename,
               F_Documents.Extension,
               F_Documents.Name,
               F_Documents.Md5key,
               F_Documents.Description,
               F_Documents.Data,
               F_Documents.Size,
               IF(F_Documents.Deleted = '', 0, F_Documents.Deleted) AS Deleted,
               F_Documents.Recordtimestamp,
               COALESCE(F_Users.UserID, '0') AS UserID
        FROM F_Documents
        LEFT JOIN F_PrivateFolders ON F_PrivateFolders.FolderID = F_Documents.LibraryID
        LEFT JOIN F_Users ON F_Users.UserID = F_PrivateFolders.UserID
        LEFT JOIN F_DocumentSets ON F_DocumentSets.DocumentID = F_Documents.DocumentID
        GROUP BY F_Documents.DocumentID
    `)
	if err != nil {
		return AddErrorInfo(err, "select documents")
	}

	lastRecordTimestamp := "1970-01-01 00:00:00"

	for documentRows.Next() {
		err = documentRows.Scan(&documentID, &folderID, &libraryID, &filename, &extension, &name, &md5key, &description, &data, &size, &deleted, &recordtimestamp,
			&userID)
		if err != nil {
			return err
		}

		if userID == 0 {
			userID = 236370
		}

		var t time.Time
		if recordtimestamp == "0000-00-00 00:00:00" {
			recordtimestamp = lastRecordTimestamp
		} else {
			lastRecordTimestamp = recordtimestamp
		}

		t, err = time.Parse(timestampLayout, recordtimestamp)
		if err != nil {
			return err
		}

		mKey := mediaUsageKey{MediaID: documentID, MediaHash: md5key}
		mu, ok := mum[mKey]
		if !ok {
			viewPrivacy := "private"
			if !privateByDefault {
				viewPrivacy = "public"
			}

			mum[mKey] = &mediaUsage{
				MediaID:     documentID,
				MediaType:   "media",
				MediaHash:   md5key,
				PostID:      0,
				PostType:    "forum",
				ViewPrivacy: viewPrivacy,
			}

			mu = mum[mKey]
		}

		albumID := folderID
		if libraryID < albumID || albumID < 14 {
			albumID = libraryID
		}

		var viewUsersStr []string
		for _, viewUserInt := range mu.ViewUsers {
			if viewUserInt == 0 || viewUserInt == userID {
				continue
			}

			viewUsersStr = append(viewUsersStr, strconv.Itoa(viewUserInt))
		}

		albumCount, ok := userIDToAlbumCountMap[userID]
		if !ok {
			albumCount = 2
		} else if mu.ViewPrivacy == "shared" {
			albumCount++
		}

		albumIDCnt := albumCount
		albumTitle := sharedAlbumTitleTemplate

		if mu.ViewPrivacy == "public" {
			albumIDCnt = 0
			albumTitle = publicAlbumTitleTemplate
		} else if mu.ViewPrivacy == "private" {
			albumIDCnt = 1
			albumTitle = privateAlbumTitleTemplate
		}

		username, ok := userIDToUsernameMap[userID]
		if ok {
			albumTitle = strings.Replace(albumTitle, "{username}", username, -1)
		} else {
			albumTitle = strings.Replace(albumTitle, "{username}'s", "", -1)
		}

		xfAlbumIDStr := fmt.Sprintf("1%06d%02d", userID, albumIDCnt) // MotorForum media attachment IDs are prepended with a '1'.

		var xfAlbumID int
		xfAlbumID, err = strconv.Atoi(xfAlbumIDStr)
		if err != nil {
			return AddErrorInfo(err, "xfAlbumID")
		}

		aKey := albumUsageKey{UserID: userID, AlbumID: xfAlbumID, ViewUsers: strings.Join(viewUsersStr, ",")}
		a, ok := aum[aKey]
		if !ok {
			aum[aKey] = &album{
				AlbumID:     xfAlbumID,
				OldAlbumID:  albumID,
				Title:       albumTitle,
				UserID:      userID,
				ViewUsers:   mu.ViewUsers,
				ViewPrivacy: mu.ViewPrivacy,
			}

			a = aum[aKey]
		}

		// filename = invalidUrlEncodeStrPreReplace.Replace(filename)
		// name = invalidUrlEncodeStrPreReplace.Replace(name)

		albumIDToMediaIDsMap[a.AlbumID] = append(albumIDToMediaIDsMap[a.AlbumID], documentID)

		mediaDocuments = append(mediaDocuments, &mediaDocument{
			DocumentID:          documentID,
			FolderID:            folderID,
			LibraryID:           libraryID,
			Filename:            filename,
			Extension:           extension,
			Name:                name,
			Md5key:              md5key,
			Description:         html.UnescapeString(description),
			Size:                size,
			Deleted:             deleted,
			RecordTimestampUnix: t.Unix(),
			Album: &album{
				AlbumID:     a.AlbumID,
				OldAlbumID:  a.OldAlbumID,
				Title:       a.Title,
				UserID:      a.UserID,
				ViewPrivacy: a.ViewPrivacy,
				ViewUsers:   a.ViewUsers,
			},
		})
	}

	return err
}

func (o *Import) createMediaCache() error {
	_, err = xf2.Txn.Exec(`
        CREATE TABLE IF NOT EXISTS xf_datio_mfxf2_media_cache (
            media_id INT NOT NULL,
            media_hash VARCHAR(32) NOT NULL,
            album_id INT NOT NULL, 
            user_id INT NOT NULL,
            source_file_hash VARCHAR(16) NOT NULL,
            success TINYINT NOT NULL,
            log BLOB,
            date DATETIME NOT NULL ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (media_id, media_hash)
        )
    `)

	return err
}

type uniqueFileKey struct {
	UserID  int
	AlbumID int
	XxHash  string
}

var thumbnailOptionRe = regexp.MustCompile(`"width":.*?([0-9]+).*?"height":.*?([0-9]+)`)

func (o *Import) insertAlbumsAndMedia() error {
	albumCacheMap := map[int]bool{}
	var (
		albumRows *sql.Rows
		aID       int
	)

	albumRows, err = xf2.Txn.Query(`
        SELECT album_id FROM xf_mg_album ORDER BY album_id
    `)
	if err != nil {
		return err
	}

	for albumRows.Next() {
		err = albumRows.Scan(&aID)
		if err != nil {
			return err
		}

		albumCacheMap[aID] = true
	}

	fmt.Println("inserting albums...")
	for _, m := range mediaDocuments {
		_, ok := albumCacheMap[m.Album.AlbumID]
		if !ok {
			var r string
			r, err = gostrgen.RandGen(8, gostrgen.Lower|gostrgen.Digit, "", "")
			if err != nil {
				return err
			}

			var mediaItemsStr []string
			for _, mediaItemsInt := range albumIDToMediaIDsMap[m.Album.AlbumID] {
				mediaItemStr := strconv.Itoa(mediaItemsInt)
				mediaItemsStr = append(mediaItemsStr, mediaItemStr)
			}
			mediaItems := fmt.Sprintf("[%s]", strings.Join(mediaItemsStr, ","))

			var viewUsersStr []string
			for _, viewUserInt := range m.Album.ViewUsers {
				if viewUserInt == 0 || viewUserInt == m.Album.UserID {
					continue
				}

				_, err = xf2.Txn.Exec(`
					INSERT IGNORE INTO xf_mg_shared_map_view
					SET album_id = ?,
					user_id = ?
				`, m.Album.AlbumID, viewUserInt)

				viewUserStr := strconv.Itoa(viewUserInt)
				viewUsersStr = append(viewUsersStr, viewUserStr)
			}
			viewUsers := fmt.Sprintf("[%s]", strings.Join(viewUsersStr, ","))

			username, ok := userIDToUsernameMap[m.Album.UserID]
			if !ok {
				username = "N/A"
			}

			categoryID := 1
			if m.Album.ViewPrivacy != "public" {
				categoryID = 0
			}

			_, err = xf2.Txn.Exec(`
                INSERT IGNORE INTO xf_mg_album
                SET album_id = ?,
                    category_id = ?,
                    album_hash = MD5(CONCAT(album_id, ?)),
                    title = ?,
                    create_date = ?,
                    media_item_cache = ?,
                    view_privacy = ?,
                    view_users = ?,
                    add_users = '[]',
                    user_id = ?,
                    username = ?,
                    media_count = ?,
                    thumbnail_date = 0
            `, m.Album.AlbumID, categoryID, r, m.Album.Title, m.RecordTimestampUnix, mediaItems, m.Album.ViewPrivacy, viewUsers, m.Album.UserID, username,
				len(albumIDToMediaIDsMap[m.Album.AlbumID]),
			)
			if err != nil {
				return AddErrorInfo(err, "insert into xf_mg_album")
			}

			_, err = xf2.Txn.Exec(`
                INSERT IGNORE INTO xf_mg_album_watch
                SET user_id = ?,
                    album_id = ?,
                    notify_on = 'media_comment',
                    send_alert = 1,
                    send_email = 0
            `, m.Album.UserID, m.Album.AlbumID,
			)
			if err != nil {
				return AddErrorInfo(err, "insert into xf_mg_album_watch")
			}

			albumCacheMap[m.Album.AlbumID] = true
		}
	}
	// return nil // debug

	// insert albums
	// -------------- todo: refactor.
	// insert media

	reactAttachmentDir, ok := o.Config.Get("React.attachment_dir").(string)
	if !ok {
		return errors.New("could not find value of 'React.attachment_dir' in config")
	}

	xf2RootDir, ok := o.Config.Get("XenForo2.xenforo_directory").(string)
	if !ok {
		return errors.New("could not find value of 'XenForo2.xenforo_directory' in config")
	}

	var serializedOptionValue string
	var thumbnailWidth, thumbnailHeight int
	err = xf2.Txn.QueryRow(`
        SELECT option_value FROM xf_option
        WHERE option_id = 'xfmgThumbnailDimensions'
    `).Scan(&serializedOptionValue)
	if err != nil {
		return err
	}

	optionItemsMatches := thumbnailOptionRe.FindStringSubmatch(serializedOptionValue)
	thumbnailWidth, err = strconv.Atoi(optionItemsMatches[1])
	if err != nil {
		return AddErrorInfo(err, "thumbnailWidth")
	}

	thumbnailHeight, err = strconv.Atoi(optionItemsMatches[2])
	if err != nil {
		return AddErrorInfo(err, "thumbnailHeight")
	}

	if thumbnailHeight == 0 || thumbnailWidth == 0 {
		thumbnailHeight = 300
		thumbnailWidth = 300
	}

	mediaCacheMap := map[int]bool{}
	var (
		mediaCacheRows  *sql.Rows
		importedMediaID int
		importedAlbumID int
		importedUserID  int
		sourceFileHash  string
		importSuccess   bool
	)

	mediaCacheRows, err = xf2.Txn.Query(`
        SELECT media_id,
               album_id,
               user_id,
               source_file_hash,
               success
        FROM xf_datio_mfxf2_media_cache ORDER BY media_id
    `)
	if err != nil {
		return err
	}

	var uniqueFileToMediaIDMap = map[uniqueFileKey]int{}
	for mediaCacheRows.Next() {
		err = mediaCacheRows.Scan(&importedMediaID, &importedAlbumID, &importedUserID, &sourceFileHash, &importSuccess)
		if err != nil {
			return err
		}

		mediaCacheMap[importedMediaID] = true

		if importSuccess {
			uniqueFileToMediaIDMap[uniqueFileKey{importedUserID, importedAlbumID, sourceFileHash}] = importedMediaID
		}
	}

	// todo: Populate media related rows from cache, when the cache and files exists but there are no media/album records.

	var totalMedia float64

	err = react.Txn.QueryRow(`
        SELECT COUNT(*) FROM F_Documents    
	`).Scan(&totalMedia)
	if err != nil {
		return err
	}

	totalMediaToInsert := totalMedia - float64(len(mediaCacheMap))

	verbose, ok := o.Config.Get("mfxf2.verbose_output").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.verbose_output' in config")
	}

	fmt.Print("inserting media.")
	var countThousand int
	var countTen int
	var countMediaIterated int
	var mediaInserted int

	for _, m := range mediaDocuments {
		_, ok = mediaCacheMap[m.DocumentID]
		if ok {
			continue
		}

		countMediaIterated++
		countThousand++
		if countThousand == 1000 {
			countTen++
			if countTen == 10 {
				fmt.Printf(" %.2f%%", (float64(countMediaIterated)/totalMediaToInsert)*100)
				fmt.Print("\ninserting media")
				countTen = 0

				if mediaInserted > 0 { // A guard for when the db dump is updated but the attachment files haven't been sync'd.
					err = xf2.Txn.Commit()
					if err != nil {
						return err
					}
				} else {
					err = xf2.Txn.Rollback()
					if verbose {
						fmt.Println("media import txn rollback")
					}
					if err != nil {
						return err
					}
				}
				xf2.Txn, err = xf2.Db.Begin()
				if err != nil {
					return err
				}

				err = react.Db.Ping()
				if err != nil {
					return AddErrorInfo(err, "keepalive react db")
				}

				mediaInserted = 0
			}

			fmt.Print(".")
			countThousand = 1
		}

		attachmentSourceDir := fmt.Sprintf("%s%d/%d", reactAttachmentDir, m.Album.OldAlbumID, m.DocumentID)

		// Check the source directory.
		_, err = os.Stat(attachmentSourceDir)
		var newAlbumID int
		if os.IsNotExist(err) {
			if m.Album.OldAlbumID != m.FolderID {
				newAlbumID = m.FolderID
			} else if m.Album.OldAlbumID != m.LibraryID {
				newAlbumID = m.LibraryID
			}

			if newAlbumID == 0 {
				_ = o.upsertMediaCache(m, sourceFileHash, false, fmt.Sprintf("failed: path not found (%s)", attachmentSourceDir))
				if verbose {
					fmt.Println(fmt.Sprintf("failed: path not found (%s)", attachmentSourceDir))
				}
				continue
			}
			attachmentSourceDir = fmt.Sprintf("%s%d/%d", reactAttachmentDir, newAlbumID, m.DocumentID)

			_, err = os.Stat(attachmentSourceDir)
			if os.IsNotExist(err) {
				attachmentSourceDir2 := fmt.Sprintf("%s%d/%d", reactAttachmentDir, newAlbumID-100, m.DocumentID)
				_, err = os.Stat(attachmentSourceDir2)

				if os.IsNotExist(err) {
					_ = o.upsertMediaCache(m, sourceFileHash, false, fmt.Sprintf("failed: new path not found (%s)", attachmentSourceDir))
					if verbose {
						fmt.Println(fmt.Sprintf("failed: new path not found (%s)", attachmentSourceDir))
					}
					continue
				}

				attachmentSourceDir = attachmentSourceDir2
			} else {
				// There's been a error on how the default album was decided, update all the album ID fields before proceeding.
				// Do not import yet, as the new album could yet to be initialized, but log to see if such an occurrence can indeed happen.
				_ = o.upsertMediaCache(m, sourceFileHash, false, fmt.Sprintf("failed: new path found (%s)", attachmentSourceDir))
				if verbose {
					fmt.Println(fmt.Sprintf("failed: new path found (%s)", attachmentSourceDir))
				}
				continue
			}
		}

		// Don't depend on the db extracted data for the media filename as malformed encoding might result to a mismatch.
		// Instead, get the largest file in the directory. This heuristic is valid as the files we want are larger than their thumbnails.
		var files []os.FileInfo
		files, err = ioutil.ReadDir(attachmentSourceDir)

		if len(files) == 0 {
			_ = o.upsertMediaCache(m, sourceFileHash, false, fmt.Sprintf("failed: empty path (%s)", attachmentSourceDir))
			if verbose {
				fmt.Println(fmt.Sprintf("failed: empty path (%s)", attachmentSourceDir))
			}
			continue
		}

		var largestFileIndex int
		var largestSize int64

		for i, file := range files {
			if file.IsDir() {
				if verbose {
					fmt.Println("file is dir")
				}
				continue
			}

			size := file.Size()
			if size > largestSize {
				largestSize = size
				largestFileIndex = i
			}
		}

		attachmentSourcePath := fmt.Sprintf("%s/%s", attachmentSourceDir, files[largestFileIndex].Name())
		if files[largestFileIndex].IsDir() {
			_ = o.upsertMediaCache(m, sourceFileHash, false, fmt.Sprintf("failed: file is dir (%s)", attachmentSourcePath))
			if verbose {
				fmt.Println(fmt.Sprintf("failed: file is dir (%s)", attachmentSourcePath))
			}
			continue
		}

		var imageFile *os.File
		var attachmentImage image.Image

		imageFile, err = os.OpenFile(attachmentSourcePath, os.O_RDONLY, 0644)
		if err != nil {
			_ = o.upsertMediaCache(m, sourceFileHash, false, fmt.Sprintf("failed: open file (%s)", attachmentSourcePath))
			if verbose {
				fmt.Println(fmt.Sprintf("failed: open file (%s)", attachmentSourcePath))
			}
			continue
		}

		// Calculate original file hash.
		_, err = imageFile.Seek(0, 0)
		if err != nil {
			return err
		}
		x := xxhash.New()
		if _, err = io.Copy(x, imageFile); err != nil {
			_ = imageFile.Close()
			return err
		}
		sourceFileHash := fmt.Sprintf("%x", x.Sum64())

		uniqueMediaID, isDuplicate := uniqueFileToMediaIDMap[uniqueFileKey{m.Album.UserID, m.Album.OldAlbumID, sourceFileHash}]
		if isDuplicate {
			_ = o.upsertMediaCache(m, sourceFileHash, false, fmt.Sprintf("failed: duplicate media (%d of %d)", m.DocumentID, uniqueMediaID))
			if verbose {
				fmt.Println(fmt.Sprintf("failed: duplicate media (%d of %d)", m.DocumentID, uniqueMediaID))
			}
			_ = imageFile.Close()
			continue
		}

		_, err = imageFile.Seek(0, 0)
		if err != nil {
			return err
		}

		var format string
		attachmentImage, format, err = image.Decode(imageFile)
		if err != nil {
			_ = o.upsertMediaCache(m, sourceFileHash, false, fmt.Sprintf("failed: decode image (%s)", attachmentSourcePath))
			if verbose {
				fmt.Println(fmt.Sprintf("failed: decode image (%s)", attachmentSourcePath))
			}
			_ = imageFile.Close()
			continue
		}

		var attachmentImageGIF *gif.GIF
		if format == "gif" {
			_, err = imageFile.Seek(0, 0)
			if err != nil {
				return err
			}

			attachmentImageGIF, err = gif.DecodeAll(imageFile)
		}

		_ = imageFile.Close()

		subDir := int(math.Floor(float64(m.DocumentID) / 1000))
		err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sinternal_data/attachments/%d/", xf2RootDir, subDir), true)
		if err != nil {
			return err
		}

		tempPath := fmt.Sprintf("%sinternal_data/attachments/%d/%d_temp.data", xf2RootDir, subDir, m.DocumentID)
		imageFile, err = os.Create(tempPath)
		if err != nil {
			return err
		}

		if format == "gif" {
			err = gif.EncodeAll(imageFile, attachmentImageGIF)
			if err != nil {
				err = jpeg.Encode(imageFile, attachmentImage, &jpeg.Options{Quality: 100})
			}
		} else {
			err = jpeg.Encode(imageFile, attachmentImage, &jpeg.Options{Quality: 100})
		}
		if err != nil {
			return err
		}

		// Calculate file hash.
		_, err = imageFile.Seek(0, 0)
		if err != nil {
			return err
		}
		h := md5.New()
		if _, err = io.Copy(h, imageFile); err != nil {
			_ = imageFile.Close()
			return err
		}
		mediaFileHash := fmt.Sprintf("%x", h.Sum(nil))

		var imageFileInfo os.FileInfo
		imageFileInfo, err = imageFile.Stat()
		imageFileSize := imageFileInfo.Size()

		_ = imageFile.Close()

		properPath := fmt.Sprintf("%sinternal_data/attachments/%d/%d-%s.data", xf2RootDir, subDir, m.DocumentID, mediaFileHash)
		err = os.Rename(tempPath, properPath)
		if err != nil {
			return err
		}

		// Create media thumbnails.
		b := attachmentImage.Bounds()
		originalImageWidth := b.Max.X
		originalImageHeight := b.Max.Y

		if b.Max.X >= b.Max.Y {
			if float64(b.Max.Y)/float64(b.Max.X) < 0.001 {
				_ = o.upsertMediaCache(m, sourceFileHash, false, fmt.Sprintf("failed: illegal aspect ratio (%s)", attachmentSourcePath))
				continue
			}
			attachmentImage = resize.Resize(0, uint(thumbnailHeight), attachmentImage, resize.Lanczos3)
		} else {
			if float64(b.Max.X)/float64(b.Max.Y) < 0.001 {
				_ = o.upsertMediaCache(m, sourceFileHash, false, fmt.Sprintf("failed: illegal aspect ratio (%s)", attachmentSourcePath))
				continue
			}
			attachmentImage = resize.Resize(uint(thumbnailWidth), 0, attachmentImage, resize.Lanczos3)
		}

		attachmentImage, err = cutter.Crop(attachmentImage, cutter.Config{
			Width:  thumbnailWidth,
			Height: thumbnailHeight,
			Mode:   cutter.Centered,
		})
		if err != nil {
			return err
		}

		err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sdata/xfmg/thumbnail/%d/", xf2RootDir, subDir), true)
		if err != nil {
			return err
		}

		imageFile, err = os.Create(fmt.Sprintf("%sdata/xfmg/thumbnail/%d/%d-%s.jpg", xf2RootDir, subDir, m.DocumentID, m.Md5key))
		if err != nil {
			return err
		}
		b = attachmentImage.Bounds()

		newThumbnailWidth := b.Max.X
		newThumbnailHeight := b.Max.Y

		_ = jpeg.Encode(imageFile, attachmentImage, &jpeg.Options{Quality: 100})
		_ = imageFile.Close()

		err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sdata/attachments/%d/", xf2RootDir, subDir), true)
		if err != nil {
			return err
		}

		imageFile, err = os.Create(fmt.Sprintf("%sdata/attachments/%d/%d-%s.jpg", xf2RootDir, subDir, m.DocumentID, mediaFileHash))
		if err != nil {
			return err
		}

		_ = png.Encode(imageFile, attachmentImage)
		_ = imageFile.Close()

		_ = o.upsertMediaCache(m, sourceFileHash, true, fmt.Sprintf("ok: (%s)", attachmentSourceDir))
		uniqueFileToMediaIDMap[uniqueFileKey{m.Album.UserID, m.Album.OldAlbumID, sourceFileHash}] = m.DocumentID

		username, ok := userIDToUsernameMap[m.Album.UserID]
		if !ok {
			username = "N/a"
		}

		folderIDTags := folderIDToTagsMap[m.FolderID]
		libraryIDTags := folderIDToTagsMap[m.LibraryID]

		var t []*tag
		if len(folderIDTags) > len(libraryIDTags) {
			t = folderIDTags
		} else {
			t = libraryIDTags
		}

		tagsSerialized := fmt.Sprintf("a:%d:{", len(t))
		for _, t := range t {
			tagsSerialized += fmt.Sprintf(`i:%d;a:2:{s:3:"tag";s:%d:"%s";s:7:"tag_url";s:%d:"%s";}`,
				t.TagID, len(t.TagTitle), t.TagTitle, len(t.TagURL), t.TagURL)
		}
		tagsSerialized += "}"

		for _, t := range t {
			_, err = xf2.Txn.Exec(`
                INSERT IGNORE INTO xf_tag_content
                SET content_type = 'xfmg_media',
                    content_id = ?,
                    tag_id = ?,
                    add_user_id = ?,
                    add_date = ?,
                    visible = 1,
                    content_date = ?
                ON DUPLICATE KEY
                UPDATE content_date = ?
            `,
				m.DocumentID, t.TagID, m.Album.UserID, m.RecordTimestampUnix, m.RecordTimestampUnix,
				m.RecordTimestampUnix,
			)
			if err != nil {
				return err
			}
		}

		mediaState := "visible"
		if m.Deleted {
			mediaState = "deleted"
		}

		originalFilename := html.UnescapeString(m.Filename)
		m.Filename, err = url.QueryUnescape(html.UnescapeString(m.Filename))
		if err != nil || len(m.Filename) == 0 {
			m.Filename = originalFilename
		}

		originalName := html.UnescapeString(m.Name)
		m.Name, err = url.QueryUnescape(html.UnescapeString(m.Name))
		if err != nil || len(m.Name) == 0 {
			m.Name = originalName
		}

		if len(m.Name) == 0 {
			m.Name = m.Filename
		}

		hasBeenRedone := false
	redo:
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_mg_media_item
            SET media_id = ?,
                media_hash = ?,
                title = ?,
                description = ?,
                media_date = ?,
                media_type = 'image',
                media_state = ?,
                album_id = ?,
                category_id = 0,
                user_id = ?,
                username = ?,
                custom_fields = 'a:0:{}',
                exif_data = '[]',
                thumbnail_date = ?,
                tags = ?
            ON DUPLICATE KEY
            UPDATE media_hash = ?,
                   title = ?,
                   description = ?,
                   media_date = ?,
                   media_type = 'image',
                   media_state = ?,
                   album_id = ?,
                   category_id = 0,
                   user_id = ?,
                   username = ?,
                   custom_fields = 'a:0:{}',
                   exif_data = '[]',
                   thumbnail_date = ?,
                   tags = ?
        `,
			m.DocumentID, m.Md5key, m.Name, m.Description, m.RecordTimestampUnix, mediaState, m.Album.AlbumID, m.Album.UserID, username, m.RecordTimestampUnix,
			tagsSerialized,
			m.Md5key, m.Name, m.Description, m.RecordTimestampUnix, mediaState, m.Album.AlbumID, m.Album.UserID, username, m.RecordTimestampUnix,
			tagsSerialized,
		)
		if err != nil {
			errorString := err.Error()
			if strings.Contains(errorString, "Incorrect string value") && strings.Contains(errorString, "for column 'title'") && !hasBeenRedone {
				m.Name = originalName
				m.Filename = originalFilename

				hasBeenRedone = true
				goto redo
			} else {
				return AddErrorInfo(err, fmt.Sprintf("insert into xf_mg_media_item: %d", m.DocumentID))
			}
		}
		_, err = xf2.Txn.Exec(`
            INSERT IGNORE INTO xf_mg_media_watch
            SET user_id = ?,
                media_id = ?,
                notify_on = 'comment',
                send_alert = 1,
                send_email = 0
        `,
			m.Album.UserID, m.DocumentID,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_mg_media_watch")
		}

		_, err = xf2.Txn.Exec(`
            INSERT IGNORE INTO xf_attachment
            SET attachment_id = ?,
                data_id = ?,
                content_type = 'xfmg_media',
                content_id = ?,
                attach_date = ?,
                unassociated = 0
            ON DUPLICATE KEY
            UPDATE data_id = ?,
                   content_type = 'xfmg_media',
                   content_id = ?,
                   attach_date = ?,
                   unassociated = 0
        `,
			m.DocumentID, m.DocumentID, m.DocumentID, m.RecordTimestampUnix,
			m.DocumentID, m.DocumentID, m.RecordTimestampUnix,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_attachment")
		}

		_, err = xf2.Txn.Exec(`
            INSERT IGNORE INTO xf_attachment_data
            SET data_id = ?,
                user_id = ?,
                upload_date = ?,
                filename = ?,
                file_size = ?,
                file_hash = ?,
                width = ?,
                height = ?,
                thumbnail_width = ?,
                thumbnail_height = ?,
                attach_count = 1
            ON DUPLICATE KEY
            UPDATE user_id = ?,
                   upload_date = ?,
                   filename = ?,
                   file_size = ?,
                   file_hash = ?,
                   width = ?,
                   height = ?,
                   thumbnail_width = ?,
                   thumbnail_height = ?,
                   attach_count = 1
        `,
			m.DocumentID, m.Album.UserID, m.RecordTimestampUnix, m.Filename, imageFileSize, mediaFileHash, originalImageWidth, originalImageHeight, newThumbnailWidth, newThumbnailHeight,
			m.Album.UserID, m.RecordTimestampUnix, m.Filename, imageFileSize, mediaFileHash, originalImageWidth, originalImageHeight, newThumbnailWidth, newThumbnailHeight,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_attachment_data")
		}

		mediaInserted++
	}
	fmt.Printf(" 100%%\n")

	if mediaInserted > 0 { // A guard for when the db dump is updated but the attachment files haven't been sync'd.
		err = xf2.Txn.Commit()
		if err != nil {
			return err
		}
	} else {
		err = xf2.Txn.Rollback()
		if err != nil {
			return err
		}
	}

	xf2.Txn, err = xf2.Db.Begin()

	return err
}

func (o *Import) upsertMediaCache(m *mediaDocument, sourceFileHash string, success bool, log string) error {
	_, err = xf2.Txn.Exec(`
        INSERT INTO xf_datio_mfxf2_media_cache
        SET media_id = ?,
            media_hash = ?,
            album_id = ?,
            user_id = ?,
            source_file_hash = ?,
            success = ?,
            log = ?,
            date = ?
        ON DUPLICATE KEY
        UPDATE album_id = ?,
               user_id = ?,
               source_file_hash = ?,
               success = ?,
               log = ?,
               date = ?
    `,
		m.DocumentID, m.Md5key, m.Album.AlbumID, m.Album.UserID, sourceFileHash, success, log, currentTime,
		m.Album.AlbumID, m.Album.UserID, sourceFileHash, success, log, currentTime,
	)

	return err
}
