// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

import (
	"database/sql"
	"errors"
	"fmt"
	"html"
	"io"
	"math"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"image"
	_ "image/gif" // Understand GIFs.
	jpeg "image/jpeg"
	_ "image/png" // Understand PNGs.

	_ "golang.org/x/image/bmp" // Understand BMPs.

	"github.com/adam-hanna/randomstrings"
	"github.com/cespare/xxhash"
	"github.com/oliamb/cutter"
	"github.com/rkravchik/resize" // https://github.com/nfnt/resize/pull/51
	"golang.org/x/crypto/sha3"
)

// Preferences deduced from 'F_Users.Reactpreferences'.
const (
	showOnline         = 1 << iota // Show user's online status.
	_                              // showReplyThread // [N/A]
	_                              // showIcon //Display avatars. [N/A]
	_                              // showWebIcon // Display external avatars. // [N/A]
	_                              // compress
	_                              // flipOrder
	_                              // skipConfirmation
	_                              // passwordExpires
	_                              // httpHeaders
	filterNodes                    // Front page nodes filter. Relates to 'F_Users.Forumfilter'. // Import as a custom user field. ✔️
	_                              // profileMessages
	_                              // quickReply
	showLastAction                 // Show user's current activity.
	allowPM                        // Start conversations with user. If set to false, while 'allowPmBuddiesOnly' is true, allow messaging from followers only.
	allowPmBuddiesOnly             // Followed people only.
)

// Preferences deduced from 'F_Userpreferences.Preferences'.
const (
	showSignature       = 1        // Show signatures under messages.
	showWWW             = 2        // Display users' homepage link icons in posts. Import as a custom user field. ✔️
	showSubtitle        = 64       // Display subtitles of users in posts. Import as a custom user field. ✔️
	_                              // linkToLastPage = 1024 // [N/A]
	showNodeDescription = 2048     // Import as a custom user field. ✔️
	_                              // showTextualPaginator  = 4096 // [N/A]
	_                              // defaultAdvancedSearch = 8192 // [N/A]
	showMotorshopper    = 524288   // Import as a custom user field. ✔️
	showMotormeuk       = 1048576  // Import as a custom user field. ✔️
	showBanners         = 2097152  // Import as a custom user field. ✔️
	activeTopicFilter   = 4194304  // Import as a custom user field. ✔️
	_                              // wideTemplate          = 8388608 // [N/A]
	showShopping        = 16777216 // Import as a custom user field. ✔️
)

// Preferences deduced from 'F_Userpreferences.Textualpreferences'.
//  defaultsearchorder [relevance|start|startrev|message|messagerev] // [N/A]
//  customrefresh                                                    // [N/A]
//  customcolor                                                      // [N/A]
//  activetopics_filter                                              // Import as a custom user field. ✔️
//  maximum_messagecount                                             // Import as a custom user field. ✔️
//  myreact_item_limit_motorshopper                                  // Import as a custom user field. ✔️
//  myreact_item_limit_motorshopper_reviews                          // Import as a custom user field. ✔️
//  motorshopper_aanbieder                                           // [N/A]
//  cookiebar                                                        // Last read time for the EU cookie. // Import as a custom user field. ✔️
//  cookies_disallowed                                               // [N/A]

// "React item limits" deduced from 'F_Users.Myreactitemlimits'. // [N/A]
// x,1,10,7,3,5,2,0,5,y
//   notepad: true
//     bookmarks: 10
//        activetopics: 7
//          contacts: 3
//             favourites: 5
//              pushmessages: 2
//                usernotes: 0
//                  postings: 5

type reactUser struct {
	IP                  string
	Email               string // Current valid e-mail
	EmailRegistration   string // Registration e-mail
	Password            string // MD5()'d password
	Nickname            string // Username
	Firstname           string // Import as a custom user field ✔️
	Lastname            string // Import as a custom user field ✔️
	Subtitle            string // Import as a custom user field ✔️
	Title               string // Custom title
	Status              string // Deduce if user must be shown as stuff
	Sex                 string // Import as a custom user field ✔️
	Birthday            *time.Time
	Location            string
	Occupation          string
	Interests           string // About
	Education           string // Import as a custom user field ✔️
	IconURL             string // Online avatar takes precedence over locally saved one
	Icon                string // Avatar
	Signature           string
	MSN                 string // Import as a custom user field ✔️
	Homepage            string // Website
	Myreactforums       string // Import as a custom user field ✔️
	Forumfilter         string // Import as a custom user field ✔️
	Lastaction          string
	Deleted             bool // Banned
	Active              bool // Activated
	EmailPublic         bool // Import as a custom user field ✔️
	Rulesexception      bool // Bypass limits permission
	UserID              int
	EngineGroupID       int // User group
	Banned              int // Ban date
	CreationTime        int // Register date
	ICQ                 int // Import as a custom user field ✔️
	ItemLimit           int // Content items display limit. Import as a custom user field ✔️
	Days                int // [N/A]
	LastVisit           int
	Messagecount        int
	Reactpreferences    int // Preferences deduced from 'F_Users.Reactpreferences'
	LastProfileChange   int // [N/A]
	LastProfileActivity int // Lastactivity
	ExtendedProfile     int // Timestamp for motorsteuk profile creation
	ItemCount           int
	ReviewCount         int // Motorshopper reviews
	ProductCount        int

	Preferences        int    // Preferences deduced from 'F_Userpreferences.Preferences'.
	Textualpreferences string // Preferences deduced from 'F_Userpreferences.Textualpreferences'.
	// Passwordexpires       int // Disabled.
	// Passwordexpiration    int
	// RecordTimestamp       *time.Time // Redundant
	// Preferences           int    // This column is empty
	// Templateset           string // [N/A]
	// Locale                string // [N/A] // Only nl_NL is set on the source data.
	// Lastmessagehash       string // [N/A]
	// TimeSpent             int // [N/A]
	// Lastprofileedit       int // [N/A]
	// SessionTime           int  // [N/A]
	// Notescount            int // [N/A]
	// Pushmessagecount      int // [N/A]
	// Profilemessagecount   int // [N/A]
	// UnreadPrivatemessages int // Not required
	// CCLPassword           bool // [N/A]
	// NoDoublePostCheck     bool // [N/A]
	// Myreactitemlimits     string // [N/A]
	// LastProfilemessage    int // [N/A]
	// Score                 float64 // [N/A] // Banaanscore™, to calculate dynamically

	// Flags and data for subsequent actions.
	DoAvatarImport        bool
	DonatorExpirationDate int
	// GroupUser      *reactGroupUser

	// XenForo related fields.
	PrimaryGroupID         int
	BirthDay               int
	BirthMonth             int
	BirthYear              int
	SecondaryGroupIDs      string
	FacebookUserFieldValue string
	TwitterUserFieldValue  string
	IsModerator            bool
	IsAdmin                bool
	IsStaff                bool
}

type reactGroupUser struct {
	// UserID     int
	GroupID    int
	Expiration int
	// LastReminder int // [N/A]
}

var userIDToUsernameMap = map[int]string{}
var usernameToUserIDMap = map[string]int{}

type importedUserCache struct {
	UserID       int
	Username     string
	ModifiedHash string
	AvatarHash   string
}

func (o *Import) importUsers() error {
	userIDToCacheMap := map[int]*importedUserCache{}

	err = o.prepareUserCacheMap(userIDToCacheMap)
	if err != nil {
		return err
	}

	doImport, ok := o.Config.Get("mfxf2.import_users").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_users' in config")
	}

	if !doImport {
		return nil
	}

	userGroups := map[int]*[]*reactGroupUser{}
	err = o.getUserGroups(userGroups)
	if err != nil {
		return err
	}

	// Create the donators' user upgrade definition.
	err = o.insertDonatorUserUpgrade()
	if err != nil {
		return err
	}

	users := []*reactUser{}

	err = o.prepareUsers(userGroups, &users, userIDToCacheMap)
	if err != nil {
		return err
	}

	// Get the activation keys for all users.
	userActivationKeys := map[int]string{}
	err = o.getActivationKeys(userActivationKeys)
	if err != nil {
		return err
	}

	err = o.insertUsers(&users, userActivationKeys)
	if err != nil {
		return err
	}

	userIDToAvatarCacheMap := map[int]*importedAvatarCache{}

	err = o.prepareAvatarCacheMap(userIDToAvatarCacheMap)
	if err != nil {
		return err
	}

	err = o.insertAvatars(&users, userIDToAvatarCacheMap)
	if err != nil {
		return err
	}

	err = o.insertBookmarks()
	if err != nil {
		return err
	}

	err = o.insertThreadWatches()
	if err != nil {
		return err
	}

	err = o.insertFollowers()

	return err
}

func (o *Import) createUserCache() error {
	_, err = xf2.Txn.Exec(`
        CREATE TABLE IF NOT EXISTS xf_datio_mfxf2_user_cache (
            user_id INT NOT NULL,
            username VARCHAR(50) NOT NULL,
            user_hash VARCHAR(128) NOT NULL,
            avatar_hash VARCHAR(128) NOT NULL,
            date DATETIME NOT NULL ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (user_id)
        )
    `)

	return err
}

func (o *Import) prepareUserCacheMap(ucm map[int]*importedUserCache) error {
	var (
		userCacheRows *sql.Rows
		userID        int
		username      string
		modifiedHash  string
		avatarHash    string
	)

	userCacheRows, err = xf2.Txn.Query(`
        SELECT user_id,
               username,
               user_hash,
               avatar_hash
        FROM xf_datio_mfxf2_user_cache
        ORDER BY date
    `)
	if err != nil {
		return err
	}

	for userCacheRows.Next() {
		err = userCacheRows.Scan(&userID, &username, &modifiedHash, &avatarHash)
		if err != nil {
			return err
		}

		ucm[userID] = &importedUserCache{UserID: userID, Username: username, ModifiedHash: modifiedHash, AvatarHash: avatarHash}
		userIDToUsernameMap[userID] = username
		usernameToUserIDMap[username] = userID
	}

	return err
}

const sqlDateForm = "2006-01-02"

func (o *Import) getUserGroups(userGroups map[int]*[]*reactGroupUser) error {
	var (
		groupUserRows *sql.Rows
		userID        int
		groupID       int
		expiration    int
	)

	groupUserRows, err = react.Txn.Query(`
        SELECT UserID, GroupID, Expiration
        FROM F_Groupusers
        WHERE GroupID <> 1 # All users will get the 'Registered' usergroup as their primary anyway.
        ORDER BY GroupID, Expiration
    `)
	if err != nil {
		return err
	}

	for groupUserRows.Next() {
		err = groupUserRows.Scan(&userID, &groupID, &expiration)
		if err != nil {
			return err
		}

		reactGroupUsers, ok := userGroups[userID]
		if !ok {
			userGroups[userID] = &[]*reactGroupUser{}
			reactGroupUsers = userGroups[userID]
		}

		*reactGroupUsers = append(*reactGroupUsers, &reactGroupUser{GroupID: groupID, Expiration: expiration})
	}

	return err
}

func (o *Import) prepareUsers(reactUserGroups map[int]*[]*reactGroupUser, u *[]*reactUser, ucm map[int]*importedUserCache) error {
	var (
		userRows            *sql.Rows
		userID              int
		engineGroupID       int
		ip                  string
		email               string
		emailRegistration   string
		password            string
		deleted             bool
		active              bool
		banned              int
		creationTime        int
		nickname            string
		firstname           string
		lastname            string
		subtitle            string
		title               string
		status              string
		sex                 string
		birthday            string // *time.Time // https://github.com/go-sql-driver/mysql/issues/683
		location            string
		occupation          string
		interests           string
		education           string
		iconURL             string
		icon                string
		emailPublic         bool
		signature           string
		icq                 int
		msn                 string
		homepage            string
		myreactforums       string
		forumFilter         string
		itemLimit           int
		days                int
		lastVisit           int
		lastaction          string
		messagecount        int
		rulesexception      bool
		reactpreferences    int
		lastProfileChange   int
		lastProfileActivity int
		extendedProfile     int
		itemCount           int
		reviewCount         int
		productCount        int
		preferences         int
		textualpreferences  string
	)

	userRows, err = react.Txn.Query(`
        SELECT F_Users.UserID, engineGroupID, IP, email, emailRegistration, Password, CAST(Deleted AS UNSIGNED), CAST(Active AS UNSIGNED), Banned, CreationTime, Nickname,
               Firstname, Lastname, Subtitle, Title, Status, Sex, IF(Birthday='0000-00-00','0001-01-01',Birthday) AS Birthday, Location, Occupation, Interests, education, IconURL,
               Icon, IF(EmailPublic>0,1,0) as EmailPublic, Signature, ICQ, MSN, Homepage, Myreactforums, forumFilter, ItemLimit, Days, LastVisit, Lastaction, Messagecount,
               CAST(Rulesexception AS UNSIGNED), Reactpreferences, LastProfileChange, LastProfileActivity, extendedProfile, ItemCount, ReviewCount, ProductCount,
               COALESCE(F_Userpreferences.Preferences, 0), COALESCE(F_Userpreferences.Textualpreferences, '')
        FROM F_Users
        LEFT JOIN F_Userpreferences ON F_Userpreferences.UserID = F_Users.UserID AND F_Userpreferences.Templateset = 'motor-forum-4'
        ORDER BY F_Users.UserID
    `)
	if err != nil {
		return err
	}

	var lastNonZeroCreationTime int

	fmt.Print("scanning users.")
	var countThousand int
	var countTen int

	for userRows.Next() {
		countThousand++
		if countThousand == 1000 {
			countTen++
			if countTen == 10 {
				fmt.Print("\nscanning users")
				countTen = 0
			}

			fmt.Print(".")
			countThousand = 1
		}

		err = userRows.Scan(
			&userID, &engineGroupID, &ip, &email, &emailRegistration, &password, &deleted, &active, &banned, &creationTime, &nickname, &firstname, &lastname, &subtitle,
			&title, &status, &sex, &birthday, &location, &occupation, &interests, &education, &iconURL, &icon, &emailPublic, &signature, &icq, &msn, &homepage, &myreactforums,
			&forumFilter, &itemLimit, &days, &lastVisit, &lastaction, &messagecount, &rulesexception, &reactpreferences, &lastProfileChange, &lastProfileActivity, &extendedProfile,
			&itemCount, &reviewCount, &productCount, &preferences, &textualpreferences,
		)

		if err != nil {
			return err
		}

		if len(email) == 0 && len(emailRegistration) > 0 {
			email = emailRegistration
		}

		if creationTime == 0 && lastNonZeroCreationTime == 0 {
			lastNonZeroCreationTime = int(currentTimeEpoch)
			creationTime = lastNonZeroCreationTime
		} else if creationTime != 0 {
			lastNonZeroCreationTime = creationTime
		} else if creationTime == 0 {
			creationTime = lastNonZeroCreationTime
		}

		if lastProfileActivity == 0 {
			if lastProfileChange != 0 {
				lastProfileActivity = lastProfileChange
			} else {
				lastProfileActivity = creationTime
			}
		}
		if lastProfileActivity < lastVisit {
			lastProfileActivity = lastVisit
		}

		var birthdayParsed time.Time
		birthdayParsed, err = time.Parse(sqlDateForm, birthday)
		if err != nil {
			birthdayParsed, _ = time.Parse(sqlDateForm, "0001-01-01")
		}

		var birthdayDay, birthdayMonth, birthdayYear int
		birthdayParsedYear := birthdayParsed.Year()

		if birthdayParsedYear < currentTime.Year()-13 && birthdayParsedYear > 1917 {
			birthdayYear = birthdayParsedYear
			birthdayMonth = int(birthdayParsed.Month())
			birthdayDay = birthdayParsed.Day()
		}

		primaryGroupID := 2
		var secondaryGroupIDs string
		var userUpgradeExpirations string
		var donatorExpirationDate int
		var isModerator bool
		var isAdmin bool
		var isStaff bool

		rug, ok := reactUserGroups[userID]
		if ok {
			var userGroups []int
			for _, reactUserGroup := range *rug {
				mappedID, okk := reactUserGroupToXf2Map[reactUserGroup.GroupID]
				if !okk {
					mappedID = reactUserGroup.GroupID
				}

				if mappedID == 4 {
					isModerator = true
					isStaff = true
				} else if mappedID == 3 {
					isAdmin = true
					isStaff = true
				}

				userGroups = append(userGroups, mappedID)

				if reactUserGroup.Expiration > 0 {
					userUpgradeExpirations += strconv.Itoa(reactUserGroup.Expiration)
				}

				if reactUserGroup.GroupID == 6 && reactUserGroup.Expiration > 0 {
					donatorExpirationDate = reactUserGroup.Expiration
				}
			}

			sort.Ints(userGroups)
			for _, userGroupID := range userGroups {
				if userGroupID == 2 {
					continue
				}

				if len(secondaryGroupIDs) == 0 {
					secondaryGroupIDs = strconv.Itoa(userGroupID)
				} else {
					secondaryGroupIDs = secondaryGroupIDs + "," + strconv.Itoa(userGroupID)
				}
			}
		}

		if strings.Contains(status, "crew") {
			isStaff = true
		}

		firstname = html.UnescapeString(firstname)
		lastname = html.UnescapeString(lastname)
		nickname = html.UnescapeString(nickname)

		icon = html.UnescapeString(icon)

		latestUserHashValue := fmt.Sprintf(
			"%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%d|%02d|%02d|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v",
			userID, engineGroupID, ip, email, emailRegistration, password, deleted, active, banned, creationTime, nickname, firstname, lastname, subtitle, title, status, sex,
			birthdayYear, birthdayMonth, birthdayDay, location, occupation, interests, education, emailPublic, signature, icq, msn, homepage, myreactforums, forumFilter, itemLimit,
			days, lastVisit, lastaction, messagecount, rulesexception, reactpreferences, lastProfileChange, lastProfileActivity, extendedProfile, itemCount, reviewCount,
			productCount, preferences, textualpreferences, secondaryGroupIDs, primaryGroupID, userUpgradeExpirations,
		)

		homepageLen := len(homepage)
		if homepageLen <= 1 || strings.Contains(homepage, "@") || !strings.Contains(homepage, ".") {
			homepage = ""
		}

		// Workaround to remove spam links.
		if messagecount == 0 {
			homepage = ""
			icq = 0
			interests = ""
			location = ""
			signature = ""
		}

		education = html.UnescapeString(education)
		interests = html.UnescapeString(interests)
		occupation = html.UnescapeString(occupation)
		signature = o.fixWhitespaceAndPunctuation(o.convertSizeRelatedBBCodes(o.normalizeSlashOnlyClosingTag(html.UnescapeString(signature))), true)
		subtitle = html.UnescapeString(subtitle)

		location = html.UnescapeString(location)
		if len(location) > 37 {
			location = ""
		}

		sex = strings.ToLower(sex)

		var facebookUserFieldValue string
		var twitterUserFieldValue string

		if homepageLen > 0 {
			if strings.Contains(homepage, "facebook") {
				facebookUserFieldValue = strings.TrimLeft(homepage, "/")
				homepage = ""
			}

			if strings.Contains(homepage, "twitter") {
				split := strings.SplitN(homepage, "/", 3)
				twitterUserFieldValue = split[len(split)-1]
				homepage = ""
			}
		}

		latestUserHashValueB := []byte(latestUserHashValue)
		uh := make([]byte, 64)
		sha3.ShakeSum256(uh, latestUserHashValueB)
		latestUserHashValue = fmt.Sprintf("%x", uh)

		latestAvatarHashValue := fmt.Sprintf("%s|%s", iconURL, icon)
		latestAvatarHashValueB := []byte(latestAvatarHashValue)
		ah := make([]byte, 64)
		sha3.ShakeSum256(ah, latestAvatarHashValueB)
		latestAvatarHashValue = fmt.Sprintf("%x", ah)

		// Check cache and decide if the user or their avatar needs to be re-imported.
		userCache, isInCache := ucm[userID]

		if isInCache {
			var avatarNeedsReImport bool

			// Check if it matches to the cache value.
			if userCache.ModifiedHash != latestUserHashValue {
				// The user needs to be re-imported.
				if userCache.AvatarHash != latestAvatarHashValue {
					// The avatar also needs to be re-imported.
					avatarNeedsReImport = true
				}

				fmt.Print("(queued for re-import) ")
				fmt.Printf("data of user id '%d' has been changed since the previous import ", userID)
				if avatarNeedsReImport {
					fmt.Print("in addition to their avatar\n")
				} else {
					fmt.Print("\n")
				}

				*u = append(*u, &reactUser{
					UserID: userID, EngineGroupID: engineGroupID, IP: ip, Email: email, EmailRegistration: emailRegistration, Password: password, Deleted: deleted, Active: active,
					Banned: banned, CreationTime: creationTime, Nickname: nickname, Firstname: firstname, Lastname: lastname, Subtitle: subtitle, Title: title, Status: status,
					Sex: sex, Birthday: &birthdayParsed, Location: location, Occupation: occupation, Interests: interests, Education: education, IconURL: iconURL, Icon: icon,
					EmailPublic: emailPublic, Signature: signature, ICQ: icq, MSN: msn, Homepage: homepage, Myreactforums: myreactforums, ItemLimit: itemLimit, Days: days,
					LastVisit: lastVisit, Lastaction: lastaction, Messagecount: messagecount, Rulesexception: rulesexception, Reactpreferences: reactpreferences,
					LastProfileChange: lastProfileChange, LastProfileActivity: lastProfileActivity, ExtendedProfile: extendedProfile, ItemCount: itemCount,
					ReviewCount: reviewCount, ProductCount: productCount, Preferences: preferences, Textualpreferences: textualpreferences, DoAvatarImport: avatarNeedsReImport,
					DonatorExpirationDate: donatorExpirationDate, PrimaryGroupID: primaryGroupID, SecondaryGroupIDs: secondaryGroupIDs,
					FacebookUserFieldValue: facebookUserFieldValue, TwitterUserFieldValue: twitterUserFieldValue, IsModerator: isModerator, IsAdmin: isAdmin, IsStaff: isStaff,
					BirthDay: birthdayDay, BirthMonth: birthdayMonth, BirthYear: birthdayYear,
				})

				err = o.upsertUserCache(userID, nickname, latestUserHashValue, latestAvatarHashValue)
				if err != nil {
					return err
				}
			}

			continue // The user and their avatar don't need to be updated.
		}

		// The user does not exist in the cache, import them and their avatar.
		*u = append(*u, &reactUser{
			UserID: userID, EngineGroupID: engineGroupID, IP: ip, Email: email, EmailRegistration: emailRegistration, Password: password, Deleted: deleted, Active: active,
			Banned: banned, CreationTime: creationTime, Nickname: nickname, Firstname: firstname, Lastname: lastname, Subtitle: subtitle, Title: title, Status: status, Sex: sex,
			Birthday: &birthdayParsed, Location: location, Occupation: occupation, Interests: interests, Education: education, IconURL: iconURL, Icon: icon,
			EmailPublic: emailPublic, Signature: signature, ICQ: icq, MSN: msn, Homepage: homepage, Myreactforums: myreactforums, ItemLimit: itemLimit, Days: days,
			LastVisit: lastVisit, Lastaction: lastaction, Messagecount: messagecount, Rulesexception: rulesexception, Reactpreferences: reactpreferences,
			LastProfileChange: lastProfileChange, LastProfileActivity: lastProfileActivity, ExtendedProfile: extendedProfile, ItemCount: itemCount, ReviewCount: reviewCount,
			ProductCount: productCount, Preferences: preferences, Textualpreferences: textualpreferences, DoAvatarImport: true, DonatorExpirationDate: donatorExpirationDate,
			PrimaryGroupID: primaryGroupID, SecondaryGroupIDs: secondaryGroupIDs, FacebookUserFieldValue: facebookUserFieldValue, TwitterUserFieldValue: twitterUserFieldValue,
			IsModerator: isModerator, IsAdmin: isAdmin, IsStaff: isStaff, BirthDay: birthdayDay, BirthMonth: birthdayMonth, BirthYear: birthdayYear,
		})

		err = o.upsertUserCache(userID, nickname, latestUserHashValue, latestAvatarHashValue)
		if err != nil {
			return err
		}
	}

	fmt.Print("\n")

	return err
}

func (o *Import) upsertUserCache(userID int, username string, userHash string, avatarHash string) error {
	_, err = xf2.Txn.Exec(`
        INSERT INTO xf_datio_mfxf2_user_cache
        SET user_id = ?,
            username = ?,
            user_hash = ?,
            avatar_hash = ?,
            date = ?
        ON DUPLICATE KEY
        UPDATE username = ?,
               user_hash = ?,
               avatar_hash = ?,
               date = ?
    `, userID, username, userHash, avatarHash, currentTime,
		username, userHash, avatarHash, currentTime,
	)

	return err
}

func (o *Import) getActivationKeys(activationKeys map[int]string) error {
	var (
		activationKeyRows *sql.Rows
		userID            int
		activationKey     string
	)

	activationKeyRows, err = react.Txn.Query(`
        SELECT u.UserID,
               a.PasswordKey
        FROM F_Users AS u
        LEFT JOIN F_Useractivationkeys a
          ON a.UserID = u.UserID
        WHERE Active <> 1
          AND Deleted <> 1
          AND a.PasswordKey IS NOT NULL
    `)
	if err != nil {
		return err
	}

	for activationKeyRows.Next() {
		err = activationKeyRows.Scan(&userID, &activationKey)
		if err != nil {
			return err
		}

		activationKeys[userID] = activationKey
	}

	return err
}

func (o *Import) insertDonatorUserUpgrade() error {
	_, err = xf2.Txn.Exec(`
        INSERT IGNORE INTO xf_user_upgrade
        SET user_upgrade_id = 6,
            title = 'Donateurs',
            display_order = 10,
            extra_group_ids = 6,
            cost_amount = 15,
            cost_currency = 'EUR',
            length_amount = 1,
            length_unit = 'year',
            can_purchase = 0
    `)

	return err
}

func (o *Import) insertActiveUserUpgradeDonator(userID int, endDate int) error {
	startDate := int64(endDate) - 31556926 // Seconds in a year.
	for startDate > currentTimeEpoch {
		startDate = startDate - 31556926
	}

	_, err = xf2.Txn.Exec(`
        INSERT INTO xf_user_upgrade_active
        SET user_id = ?,
            user_upgrade_id = 6,
            extra = 'a:4:{s:11:"cost_amount";s:5:"15.00";s:13:"cost_currency";s:3:"EUR";s:13:"length_amount";i:1;s:11:"length_unit";s:4:"year";}',
            start_date = ?,
            end_date = ?
        ON DUPLICATE KEY
        UPDATE end_date = ?
    `, userID, startDate, endDate,
		endDate,
	)
	if err != nil {
		return AddErrorInfo(err, "insert into xf_user_upgrade_active")
	}

	_, err = xf2.Txn.Exec(`
        INSERT IGNORE INTO xf_user_group_change
        SET user_id = ?,
            change_key = 'userUpgrade-6',
            group_ids = 6
    `, userID)

	return err
}

var activeTopicsFilterRe = regexp.MustCompile(`"activetopics_filter";a:[0-9]*:\{(.*?)\}`)
var maximumMessageCountRe = regexp.MustCompile(`maximum_messagecount";s:[0-9]*:"([0-9]*)"`)
var itemLimitMotorShopperRe = regexp.MustCompile(`"myreact_item_limit_motorshopper";s:[0-9]*:"([0-9]*)"`)
var itemLimitMotorShopperReviewsRe = regexp.MustCompile(`"myreact_item_limit_motorshopper_reviews";s:[0-9]*:"([0-9]*)"`)
var cookiebarTimestampRe = regexp.MustCompile(`"cookiebar";s:10:"([0-9]{10})"`)

var serializedPhpIntReplacer = strings.NewReplacer(";i:", ",", "i:", "", ";", "")

func (o *Import) insertUsers(u *[]*reactUser, activationKeys map[int]string) error {
	var (
		displayOnlineStatus       bool
		frontPageNodesFilter      bool
		displayCurrentActivity    bool
		startConversationsWith    bool
		conversationsFollowedOnly bool

		showSignatures       bool
		showHomepages        bool
		showSubtitles        bool
		showNodeDescriptions bool
		showMS               bool
		showMM               bool
		showAds              bool
		activeTopicsFilter   bool
		showShop             bool

		activeTopicsFilterItems      string
		maximumMessageCount          int
		itemLimitMotorShopper        int
		itemLimitMotorShopperReviews int
		cookiebarTimestamp           int
	)

	fmt.Print("inserting users.")
	var countThousand int
	var countTen int
	for _, user := range *u {
		countThousand++
		if countThousand == 1000 {
			countTen++
			if countTen == 10 {
				fmt.Print("\ninserting users")
				countTen = 0
			}

			fmt.Print(".")
			countThousand = 1
		}

		// Determine the user preferences.
		conversationsFollowedOnly = user.Reactpreferences&allowPmBuddiesOnly > 0
		displayCurrentActivity = user.Reactpreferences&showLastAction > 0
		displayOnlineStatus = user.Reactpreferences&showOnline > 0
		frontPageNodesFilter = user.Reactpreferences&filterNodes > 0
		startConversationsWith = user.Reactpreferences&allowPM > 0

		activeTopicsFilter = user.Preferences&activeTopicFilter > 0
		showAds = user.Preferences&showBanners > 0
		showHomepages = user.Preferences&showWWW > 0
		showMM = user.Preferences&showMotormeuk > 0
		showMS = user.Preferences&showMotorshopper > 0
		showNodeDescriptions = user.Preferences&showNodeDescription > 0
		showShop = user.Preferences&showShopping > 0
		showSignatures = user.Preferences&showSignature > 0
		showSubtitles = user.Preferences&showSubtitle > 0

		activeTopicsFilterItemsSubmatch := activeTopicsFilterRe.FindStringSubmatch(user.Textualpreferences)
		if len(activeTopicsFilterItemsSubmatch) > 0 {
			activeTopicsFilterItems = serializedPhpIntReplacer.Replace(activeTopicsFilterItemsSubmatch[1])
		}

		cookiebarTimestampSubmatch := cookiebarTimestampRe.FindStringSubmatch(user.Textualpreferences)
		if len(cookiebarTimestampSubmatch) > 0 && len(cookiebarTimestampSubmatch[1]) > 0 {
			cookiebarTimestamp, err = strconv.Atoi(cookiebarTimestampSubmatch[1])
			if err != nil {
				return err
			}
		}

		itemLimitMotorShopperSubmatch := itemLimitMotorShopperRe.FindStringSubmatch(user.Textualpreferences)
		if len(itemLimitMotorShopperSubmatch) > 0 && len(itemLimitMotorShopperSubmatch[1]) > 0 {
			itemLimitMotorShopper, err = strconv.Atoi(itemLimitMotorShopperSubmatch[1])
			if err != nil {
				return err
			}
		}

		itemLimitMotorShopperReviewsSubmatch := itemLimitMotorShopperReviewsRe.FindStringSubmatch(user.Textualpreferences)
		if len(itemLimitMotorShopperReviewsSubmatch) > 0 && len(itemLimitMotorShopperReviewsSubmatch[1]) > 0 {
			itemLimitMotorShopperReviews, err = strconv.Atoi(itemLimitMotorShopperReviewsSubmatch[1])
			if err != nil {
				return err
			}
		}

		maximumMessageCountSubmatch := maximumMessageCountRe.FindStringSubmatch(user.Textualpreferences)
		if len(maximumMessageCountSubmatch) > 0 && len(maximumMessageCountSubmatch[1]) > 0 {
			maximumMessageCount, err = strconv.Atoi(maximumMessageCountSubmatch[1])
			if err != nil {
				return err
			}
		}

		icqStr := strconv.Itoa(user.ICQ)
		if len(icqStr) != 10 {
			icqStr = ""
		}

		// Insert the custom user field values.
		if len(user.Firstname) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "firstname", user.Firstname)
		}
		if len(user.Lastname) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "lastname", user.Lastname)
		}
		if len(user.Sex) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "gender", user.Sex)
		}
		if len(user.Occupation) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "occupation", user.Occupation)
		}
		if len(user.Education) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "education", user.Education)
		}
		if len(user.Subtitle) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "subtitle", user.Subtitle)
		}
		if len(icqStr) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "icq", icqStr)
		}
		if len(user.MSN) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "msn", user.MSN)
		}
		if len(user.Forumfilter) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "filterNodesSelected", user.Forumfilter)
		}
		if len(activeTopicsFilterItems) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "filterATNodesSelected", activeTopicsFilterItems)
		}
		if len(user.Myreactforums) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "favoriteNodesSelected", user.Myreactforums)
		}
		if len(user.FacebookUserFieldValue) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "facebook", user.FacebookUserFieldValue)
		}
		if len(user.TwitterUserFieldValue) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "twitter", user.TwitterUserFieldValue)
		}

		if maximumMessageCount != 0 {
			_ = o.insertUserFieldValue(user.UserID, "maxATMessagesCount", strconv.Itoa(maximumMessageCount))
		}
		if itemLimitMotorShopper != 0 {
			_ = o.insertUserFieldValue(user.UserID, "motorshopperLimit", strconv.Itoa(itemLimitMotorShopper))
		}
		if itemLimitMotorShopperReviews != 0 {
			_ = o.insertUserFieldValue(user.UserID, "motorshopperReviewsLimit", strconv.Itoa(itemLimitMotorShopperReviews))
		}
		if cookiebarTimestamp != 0 {
			_ = o.insertUserFieldValue(user.UserID, "euCookieReadDate", strconv.Itoa(cookiebarTimestamp))
		}
		if user.ItemLimit != 0 {
			_ = o.insertUserFieldValue(user.UserID, "contentItemLimit", strconv.Itoa(user.ItemLimit))
		}

		if showMS {
			_ = o.insertUserFieldValue(user.UserID, "showMotorshopper", `a:1:{s:4:"true";s:4:"true";}`)
		}
		if showMM {
			_ = o.insertUserFieldValue(user.UserID, "showMotormeuk", `a:1:{s:4:"true";s:4:"true";}`)
		}
		if showHomepages {
			_ = o.insertUserFieldValue(user.UserID, "showWebsiteIconInPosts", `a:1:{s:4:"true";s:4:"true";}`)
		}
		if showSubtitles {
			_ = o.insertUserFieldValue(user.UserID, "showSubtitleInPosts", `a:1:{s:4:"true";s:4:"true";}`)
		}
		if showNodeDescriptions {
			_ = o.insertUserFieldValue(user.UserID, "showNodesDescription", `a:1:{s:4:"true";s:4:"true";}`)
		}
		if showAds {
			_ = o.insertUserFieldValue(user.UserID, "showBanners", `a:1:{s:4:"true";s:4:"true";}`)
		}
		if showShop {
			_ = o.insertUserFieldValue(user.UserID, "showShopping", `a:1:{s:4:"true";s:4:"true";}`)
		}
		if user.EmailPublic {
			_ = o.insertUserFieldValue(user.UserID, "displayEmail", `a:1:{s:4:"true";s:4:"true";}`)
		}
		if activeTopicsFilter {
			_ = o.insertUserFieldValue(user.UserID, "filterATNodes", `a:1:{s:4:"true";s:4:"true";}`)
		}
		if frontPageNodesFilter {
			_ = o.insertUserFieldValue(user.UserID, "filterNodes", `a:1:{s:4:"true";s:4:"true";}`)
		}

		// Give donators the corresponding user upgrade.
		if user.DonatorExpirationDate > 0 {
			err = o.insertActiveUserUpgradeDonator(user.UserID, user.DonatorExpirationDate)
			if err != nil {
				return err
			}
		}

		// Watch any favorite (MyReact) forums.
		if len(user.Myreactforums) > 0 {
			_ = o.insertUserFieldValue(user.UserID, "favoriteNodesSelected", user.Myreactforums)
			forumsToWatch := strings.Split(user.Myreactforums, ",")
			for _, forumIDToWatchStr := range forumsToWatch {
				var forumIDToWatch int
				forumIDToWatch, err = strconv.Atoi(forumIDToWatchStr)
				if err != nil {
					return err
				}

				_, err = xf2.Txn.Exec(`
					INSERT IGNORE INTO xf_forum_watch
					SET user_id = ?,
						node_id = ?,
						notify_on = '',
						send_alert = 0,
						send_email = 0
				`, user.UserID, forumIDToWatch,
				)
				if err != nil {
					return AddErrorInfo(err, "insert into xf_forum_watch")
				}
			}
		}

		// Add e-mail changes into the change log.
		if user.EmailRegistration != "" && user.Email != user.EmailRegistration {
			_, err = xf2.Txn.Exec(`
                DELETE FROM xf_change_log
                WHERE content_type = 'user'
                  AND content_id = ?
                  AND edit_user_id = ?
                  AND field = 'email'
            `, user.UserID, user.UserID,
			)
			if err != nil {
				return AddErrorInfo(err, "delete from xf_change_log")
			}

			_, err = xf2.Txn.Exec(`
                INSERT INTO xf_change_log
                SET content_type = 'user',
                    content_id = ?,
                    edit_user_id = ?,
                    edit_date = ?,
                    field = 'email',
                    old_value = ?,
                    new_value = ?
            `, user.UserID, user.UserID, user.CreationTime, user.EmailRegistration, user.Email,
			)
			if err != nil {
				return AddErrorInfo(err, "insert into xf_change_log")
			}
		}

		// Re-use a cut version of the react confirmation key for any unconfirmed users.
		// The related xf table field has a 16 char limit, while the react one provides a 32 hex string.
		if !user.Active {
			confirmationKey, ok := activationKeys[user.UserID]
			if ok {
				_, err = xf2.Txn.Exec(`
                    INSERT IGNORE INTO xf_user_confirmation
                    SET user_id = ?,
                        confirmation_type = 'email',
                        confirmation_key = ?,
                        confirmation_date = ?
                `, user.UserID, confirmationKey[0:16], user.CreationTime,
				)
				if err != nil {
					return AddErrorInfo(err, "insert into xf_user_confirmation")
				}
			}
		}

		defaultUserLanguageID, ok := o.Config.Get("mfxf2.default_user_language_id").(int64)
		if !ok {
			return errors.New("could not find value of 'mfxf2.default_user_language_id' in config")
		}

		defaultUserTimezone, ok := o.Config.Get("mfxf2.default_user_timezone").(string)
		if !ok {
			return errors.New("could not find value of 'mfxf2.default_user_timezone' in config")
		}

		var userState string
		// if the user is Deleted, disable their account.
		if user.Deleted {
			userState = "disabled"
		} else if !user.Active {
			userState = "email_confirm"
		} else {
			userState = "valid"
		}

		var secretKey string
		secretKey, err = randomstrings.GenerateRandomString(24)
		if err != nil {
			return AddErrorInfo(err, "random string generation failure")
		}

		// Insert user.
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_user
            SET user_id = ?,
                username = ?,
                email = ?,
                custom_title = ?,
                language_id = ?,
                style_id = 0,
                timezone = ?,
                visible = ?,
                activity_visible = ?,
                user_group_id= ?,
                secondary_group_ids = ?,
                display_style_group_id = ?,
                permission_combination_id = 0, # leave blank until better workaround.
                message_count = ?,
                conversations_unread = 0, # calc later or leave to cache rebuild
                register_date = ?,
                last_activity = ?,
                trophy_points = 0,
                alerts_unread = 0,
                avatar_date = 0,
                avatar_width = 0,
                avatar_height = 0,
                avatar_highdpi = 0,
                gravatar = '',
                user_state = ?, # valid, email_confirm, email_confirm_edit, moderated, email_bounce, rejected, disabled
                is_moderator = ?,
                is_admin = ?,
                # is_banned = 0,
                # like_count = 0,
                # warning_points = 0,
                is_staff = ?,
                secret_key = ?
                # xfmg_album_count = 0,
                # xfmg_media_count = 0,
                # xfmg_media_quota = 0
            ON DUPLICATE KEY
            UPDATE username = ?,
                   email = ?,
                   custom_title = ?,
                   language_id = ?,
                   timezone = ?,
                   visible = ?,
                   activity_visible = ?,
                   user_group_id= ?,
                   secondary_group_ids = ?,
                   display_style_group_id = ?,
                   message_count = ?,
                   register_date = ?,
                   last_activity = ?,
                   user_state = ?,
                   is_moderator = ?,
                   is_admin = ?,
                   is_staff = ?
        `, user.UserID, user.Nickname, user.Email, user.Title, defaultUserLanguageID, defaultUserTimezone, displayOnlineStatus, displayCurrentActivity, user.PrimaryGroupID,
			user.SecondaryGroupIDs, user.EngineGroupID, user.Messagecount, user.CreationTime, user.LastProfileActivity, userState, user.IsModerator, user.IsAdmin, user.IsStaff,
			secretKey,
			user.Nickname, user.Email, user.Title, defaultUserLanguageID, defaultUserTimezone, displayOnlineStatus, displayCurrentActivity, user.PrimaryGroupID,
			user.SecondaryGroupIDs, user.EngineGroupID, user.Messagecount, user.CreationTime, user.LastProfileActivity, userState, user.IsModerator, user.IsAdmin, user.IsStaff,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_user")
		}

		const adminPermissionsAll = `a:27:{s:5:"addOn";b:1;s:11:"advertising";b:1;s:10:"attachment";b:1;s:3:"ban";b:1;s:12:"bbCodeSmilie";b:1;s:4:"cron";b:1;s:4:"help";b:1;s:6:"import";b:1;s:8:"language";b:1;s:10:"navigation";b:1;s:4:"node";b:1;s:6:"notice";b:1;s:6:"option";b:1;s:7:"payment";b:1;s:12:"rebuildCache";b:1;s:5:"style";b:1;s:4:"tags";b:1;s:6:"thread";b:1;s:6:"trophy";b:1;s:4:"user";b:1;s:9:"userField";b:1;s:9:"userGroup";b:1;s:11:"userUpgrade";b:1;s:8:"viewLogs";b:1;s:14:"viewStatistics";b:1;s:7:"warning";b:1;s:6:"widget";b:1;}`

		if user.IsAdmin {
			_, err = xf2.Txn.Exec(`
                INSERT INTO xf_admin
                SET user_id = ?,
                    extra_user_group_ids = 3,
                    last_login = 0,
                    permission_cache = ?,
                    is_super_admin = 1
                ON DUPLICATE KEY
                UPDATE extra_user_group_ids = 3,
                    -- permission_cache = ?,
                       is_super_admin = 1
            `, user.UserID, adminPermissionsAll,
				// adminPermissionsAll,
			)
			if err != nil {
				return AddErrorInfo(err, "insert into xf_admin")
			}
		}

		if user.IsAdmin || user.IsModerator {
			_, err = xf2.Txn.Exec(`
                INSERT INTO xf_moderator
                SET user_id = ?,
                    is_super_moderator = 1,
                    extra_user_group_ids = 4
                ON DUPLICATE KEY
                UPDATE is_super_moderator = 1,
                       extra_user_group_ids = 4
            `, user.UserID,
			)
			if err != nil {
				return AddErrorInfo(err, "insert into xf_moderator")
			}
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_user_profile
            SET user_id = ?,
                dob_day = ?,
                dob_month = ?,
                dob_year = ?,
                signature = ?,
                website = ?,
                location = ?,
                following = '',
                ignored = 'a:0:{}',
                avatar_crop_x = 0,
                avatar_crop_y = 0,
                about = ?,
                custom_fields = 'a:0:{}',
                connected_accounts = 'a:0:{}',
                password_date = ?
            ON DUPLICATE KEY
            UPDATE dob_day = ?,
                   dob_month = ?,
                   dob_year = ?,
                   signature = ?,
                   website = ?,
                   location = ?,
                   about = ?,
                   password_date = ?
        `, user.UserID, user.BirthDay, user.BirthMonth, user.BirthYear, user.Signature, user.Homepage, user.Location, user.Interests, user.CreationTime,
			user.BirthDay, user.BirthMonth, user.BirthYear, user.Signature, user.Homepage, user.Location, user.Interests, user.CreationTime,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_user_profile")
		}

		allowSendPersonalConversation := "everyone"
		if conversationsFollowedOnly {
			allowSendPersonalConversation = "followed"
		} else if !startConversationsWith {
			allowSendPersonalConversation = "none"
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_user_privacy
            SET user_id = ?,
                allow_send_personal_conversation = ?
            ON DUPLICATE KEY
            UPDATE allow_send_personal_conversation = ?
        `, user.UserID, allowSendPersonalConversation,
			allowSendPersonalConversation,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_user_privacy")
		}

		serializedPasswordData := fmt.Sprintf(`a:1:{s:4:"hash";s:%d:"%s";}`, len(user.Password), user.Password)

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_user_authenticate
            SET user_id = ?,
                scheme_class = 'Datio\\React:React',
                data = ?
            ON DUPLICATE KEY
            UPDATE scheme_class = 'Datio\\React:React',
                   data = ?
        `, user.UserID, serializedPasswordData,
			serializedPasswordData,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_user_authenticate")
		}

		showBirthYear := true

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_user_option
            SET user_id = ?,
                show_dob_year = ?,
                content_show_signature = ?,
                creation_watch_state = 'watch_email',
                interaction_watch_state = 'watch_email',
                alert_optout = ''				
            ON DUPLICATE KEY
            UPDATE show_dob_year = ?,
                   content_show_signature = ?
        `, user.UserID, showBirthYear, showSignatures,
			showBirthYear, showSignatures,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_user_option")
		}

		// Insert user group relations.
		usergroups := strings.Split(user.SecondaryGroupIDs, ",")
		usergroups = append(usergroups, "2")

		_, err = xf2.Txn.Exec(`
            DELETE FROM xf_user_group_relation
            WHERE user_id = ?
        `, user.UserID,
		)
		if err != nil {
			return AddErrorInfo(err, "delete from xf_user_option")
		}

		for _, usergroup := range usergroups {
			if usergroup == "" {
				continue
			}

			_, err = xf2.Txn.Exec(`
                INSERT INTO xf_user_group_relation
                SET user_id = ?,
                    user_group_id = ?,
                    is_primary = ?				
            `, user.UserID, usergroup, usergroup == "2",
			)
			if err != nil {
				return AddErrorInfo(err, "insert into xf_user_option")
			}
		}

	}
	fmt.Print("\n")

	return err
}

func (o *Import) insertUserFieldValue(userID int, fieldName string, value string) error {
	_, err = xf2.Txn.Exec(`
        INSERT INTO xf_user_field_value
        SET user_id = ?,
            field_id = ?,
            field_value = ?
        ON DUPLICATE KEY
        UPDATE field_id = ?,
               field_value = ?
    `, userID, fieldName, value,
		fieldName, value,
	)

	return err
}

func (o *Import) createAvatarCache() error {
	_, err = xf2.Txn.Exec(`
        CREATE TABLE IF NOT EXISTS xf_datio_mfxf2_avatar_cache (
            user_id INT NOT NULL,
            avatar_date INT NOT NULL,
            avatar_width SMALLINT NOT NULL,
            avatar_height SMALLINT NOT NULL,
            avatar_highdpi TINYINT NOT NULL,
            avatar_crop_x INT NOT NULL,
            avatar_crop_y INT NOT NULL,
            PRIMARY KEY (user_id)
        )
    `)
	if err != nil {
		return AddErrorInfo(err, "create table xf_datio_mfxf2_avatar_cache")
	}

	return err
}

type importedAvatarCache struct {
	UserID        int
	AvatarDate    int
	AvatarWidth   int
	AvatarHeight  int
	AvatarHighDpi bool
	AvatarCropX   int
	AvatarCropY   int
}

func (o *Import) prepareAvatarCacheMap(acm map[int]*importedAvatarCache) error {
	var (
		avatarCacheRows *sql.Rows
		userID          int
		avatarDate      int
		avatarWidth     int
		avatarHeight    int
		avatarHighDpi   bool
		avatarCropX     int
		avatarCropY     int
	)

	avatarCacheRows, err = xf2.Txn.Query(`
        SELECT user_id,
               avatar_date,
               avatar_width,
               avatar_height,
               avatar_highdpi,
               avatar_crop_x,
               avatar_crop_y
        FROM xf_datio_mfxf2_avatar_cache
        ORDER BY avatar_date
    `)
	if err != nil {
		return AddErrorInfo(err, "select from xf_user")
	}

	for avatarCacheRows.Next() {
		err = avatarCacheRows.Scan(&userID, &avatarDate, &avatarWidth, &avatarHeight, &avatarHighDpi, &avatarCropX, &avatarCropY)
		if err != nil {
			return err
		}

		acm[userID] = &importedAvatarCache{UserID: userID, AvatarDate: avatarDate, AvatarWidth: avatarWidth, AvatarHeight: avatarHeight, AvatarHighDpi: avatarHighDpi,
			AvatarCropX: avatarCropX, AvatarCropY: avatarCropY}
	}

	return err
}

var xfAvatarDirs = []string{
	"data/avatars/",
	"data/avatars/s/",
	"data/avatars/m/",
	"data/avatars/l/",
	"data/avatars/h/",
}

var xfAvatarSizesMap = map[string]int{
	"s": 48,
	"m": 96,
	"l": 192,
	"h": 384,
}

const xfSizeMapO = 384
const xfSizeMapDefault = 96 // xfAvatarSizesMap["m"]

var xfAvatarSizes = []string{
	"h",
	"l",
	"m",
	"s",
}

var doImportExternalAvatars bool
var doImportLocalAvatars bool

func (o *Import) insertAvatars(u *[]*reactUser, acm map[int]*importedAvatarCache) error {
	var ok bool
	doImportExternalAvatars, ok = o.Config.Get("mfxf2.import_external_avatars").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_external_avatars' in config")
	}
	doImportLocalAvatars, ok = o.Config.Get("mfxf2.import_local_avatars").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_local_avatars' in config")
	}

	externalDownloadTimeout, ok := o.Config.Get("mfxf2.external_avatar_download_timeout").(int64)
	if !ok {
		return errors.New("could not find value of 'mfxf2.external_avatar_download_timeout' in config")
	}

	cacheDir, ok := o.Config.Get("mfxf2.cache_dir").(string)
	if !ok {
		return errors.New("could not find value of 'mfxf2.cache_dir' in config")
	}

	reactAvatarDir, ok := o.Config.Get("React.avatar_dir").(string)
	if !ok {
		return errors.New("could not find value of 'React.avatar_dir' in config")
	}

	xf2RootDir, ok := o.Config.Get("XenForo2.xenforo_directory").(string)
	if !ok {
		return errors.New("could not find value of 'XenForo2.xenforo_directory' in config")
	}

	downloadedAvatarCacheDir := cacheDir + "downloaded_avatars/"

	err = o.createEmptyDirIfNotExists(downloadedAvatarCacheDir, false)
	if err != nil {
		return err
	}

	for _, xfAvatarDir := range xfAvatarDirs {
		err = o.createEmptyDirIfNotExists(xf2RootDir+xfAvatarDir, true)
		if err != nil {
			return err
		}
	}

	var avatarImage image.Image
	var imageFile *os.File
	var avatarOk bool

	for _, user := range *u {
		if !user.DoAvatarImport {
			continue
		}

		avatarCache, isInCache := acm[user.UserID]
		if isInCache {
			_, err = xf2.Txn.Exec(`
                UPDATE xf_user
                SET avatar_date = ?,
                    avatar_width = ?,
                    avatar_height = ?,
                    avatar_highdpi = ?
                WHERE user_id = ?
            `, avatarCache.AvatarDate, avatarCache.AvatarWidth, avatarCache.AvatarHeight, avatarCache.AvatarHighDpi, user.UserID,
			)
			if err != nil {
				return AddErrorInfo(err, "update xf_user from cache")
			}

			_, err = xf2.Txn.Exec(`
                UPDATE xf_user_profile
                SET avatar_crop_x = ?,
                    avatar_crop_y = ?
                WHERE user_id = ?
            `, avatarCache.AvatarCropX, avatarCache.AvatarCropY, user.UserID,
			)
			if err != nil {
				return AddErrorInfo(err, "update xf_user from cache")
			}

			continue
		}

		if len(user.Icon) == 0 && len(user.IconURL) == 0 {
			continue
		}

		avatarImage, avatarOk, _ = o.getExternalAvatar(user, downloadedAvatarCacheDir, externalDownloadTimeout)

		if !avatarOk {
			avatarImage, avatarOk, _ = o.getLocalAvatar(user, reactAvatarDir)
			if !avatarOk {
				continue
			}
		}

		subDir := int(math.Floor(float64(user.UserID) / 1000))
		var highDpi bool
		var offsetX int
		var offsetY int

		b := avatarImage.Bounds()
		for _, sizeKey := range xfAvatarSizes {
			err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sdata/avatars/%s/%d/", xf2RootDir, sizeKey, subDir), true)
			if err != nil {
				return err
			}

			if sizeKey == "h" {
				err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sdata/avatars/%s/%d/", xf2RootDir, "o", subDir), true)
				if err != nil {
					return err
				}

				if b.Max.X >= b.Max.Y {
					if b.Max.Y >= xfSizeMapO {
						highDpi = true
					}
					avatarImage = resize.Resize(0, uint(xfAvatarSizesMap[sizeKey]), avatarImage, resize.Lanczos3)
				} else {
					if b.Max.X >= xfSizeMapO {
						highDpi = true
					}
					avatarImage = resize.Resize(uint(xfAvatarSizesMap[sizeKey]), 0, avatarImage, resize.Lanczos3)
				}

				imageFile, err = os.Create(fmt.Sprintf("%sdata/avatars/%s/%d/%d.jpg", xf2RootDir, "o", subDir, user.UserID))
				if err != nil {
					return err
				}

				_ = jpeg.Encode(imageFile, avatarImage, &jpeg.Options{Quality: 100})
				_ = imageFile.Close()

				avatarImage, err = cutter.Crop(avatarImage, cutter.Config{
					Width:  xfSizeMapO,
					Height: xfSizeMapO,
					Mode:   cutter.Centered,
				})
				if err != nil {
					return err
				}

				imageFile, err = os.Create(fmt.Sprintf("%sdata/avatars/%s/%d/%d.jpg", xf2RootDir, "h", subDir, user.UserID))
				if err != nil {
					return err
				}

				b = avatarImage.Bounds()
				if b.Max.X >= b.Max.Y {
					offsetX = (((xfSizeMapDefault * b.Max.X) / b.Max.Y) - xfSizeMapDefault)
				} else {
					offsetY = (((xfSizeMapDefault * b.Max.Y) / b.Max.X) - xfSizeMapDefault)
				}

				_ = jpeg.Encode(imageFile, avatarImage, &jpeg.Options{Quality: 100})
				_ = imageFile.Close()

				continue
			}

			avatarImage = resize.Resize(uint(xfAvatarSizesMap[sizeKey]), 0, avatarImage, resize.Lanczos3)

			imageFile, err = os.Create(fmt.Sprintf("%sdata/avatars/%s/%d/%d.jpg", xf2RootDir, sizeKey, subDir, user.UserID))
			if err != nil {
				return err
			}

			_ = jpeg.Encode(imageFile, avatarImage, &jpeg.Options{Quality: 100})
			_ = imageFile.Close()
		}

		_, err = xf2.Txn.Exec(`
            UPDATE xf_user
            SET avatar_date = ?,
                avatar_width = ?,
                avatar_height = ?,
                avatar_highdpi = ?
            WHERE user_id = ?
        `, currentTimeEpoch, xfSizeMapO, xfSizeMapO, highDpi, user.UserID,
		)
		if err != nil {
			return AddErrorInfo(err, "update xf_user")
		}

		_, err = xf2.Txn.Exec(`
            UPDATE xf_user_profile
            SET avatar_crop_x = ?,
                avatar_crop_y = ?
            WHERE user_id = ?
        `, offsetX, offsetY, user.UserID,
		)
		if err != nil {
			return AddErrorInfo(err, "update xf_user")
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_datio_mfxf2_avatar_cache
            SET user_id = ?,
                avatar_date = ?,
                avatar_width = ?,
                avatar_height = ?,
                avatar_highdpi = ?,
                avatar_crop_x = ?,
                avatar_crop_y = ?
            ON DUPLICATE KEY
            UPDATE avatar_date = ?,
                   avatar_width = ?,
                   avatar_height = ?,
                   avatar_highdpi = ?,
                   avatar_crop_x = ?,
                   avatar_crop_y = ?
        `, user.UserID, currentTimeEpoch, xfSizeMapO, xfSizeMapO, highDpi, offsetX, offsetY,
			currentTimeEpoch, xfSizeMapO, xfSizeMapO, highDpi, offsetX, offsetY,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_datio_mfxf2_avatar_cache")
		}
	}

	return err
}

func (o *Import) getExternalAvatar(user *reactUser, downloadedAvatarCacheDir string, externalDownloadTimeout int64) (image.Image, bool, error) {
	var avatarImage image.Image
	var imageFile *os.File
	var avatarOk bool
	var avatarErr error

	if !doImportExternalAvatars {
		return avatarImage, avatarOk, avatarErr
	}

	if len(user.IconURL) == 0 || strings.Contains(user.IconURL, "photobucket") {
		return avatarImage, avatarOk, avatarErr
	}

	var h string
	x := xxhash.New()
	_, avatarErr = x.Write([]byte(user.IconURL))
	if avatarErr != nil {
		return avatarImage, avatarOk, avatarErr
	}
	h = fmt.Sprintf("%x", x.Sum64())

	downloadedAvatarPath := downloadedAvatarCacheDir + fmt.Sprintf("%06d_%s", user.UserID, h)

	imageFile, avatarErr = os.OpenFile(downloadedAvatarPath, os.O_RDONLY, 0644)

	if avatarErr == nil {
		avatarImage, _, avatarErr = image.Decode(imageFile)
		if avatarErr == nil {
			avatarOk = true
		}
		_ = imageFile.Close()
		return avatarImage, avatarOk, avatarErr
	}

	defer func() {
		_ = imageFile.Close()
	}()

	if len(user.IconURL) > 0 {
		imageFile, avatarErr = os.Create(downloadedAvatarPath)
		if avatarErr != nil {
			return avatarImage, avatarOk, avatarErr
		}

		req, avatarErr := http.NewRequest("GET", user.IconURL, nil)
		if avatarErr != nil {
			return avatarImage, avatarOk, avatarErr
		}
		req.Header.Set("User-Agent", "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0")
		req.Header.Set("Referer", user.IconURL)

		timeout := time.Duration(externalDownloadTimeout) * time.Second
		client := http.Client{
			Timeout: timeout,
		}

		response, avatarErr := client.Do(req)
		if avatarErr != nil {
			return avatarImage, avatarOk, avatarErr
		}
		defer func() {
			_ = response.Body.Close()
		}()

		_, avatarErr = io.Copy(imageFile, response.Body)
		if avatarErr != nil {
			return avatarImage, avatarOk, avatarErr
		}

		_, _ = imageFile.Seek(0, 0)

		avatarImage, _, avatarErr = image.Decode(imageFile)
		if avatarErr != nil {
			return avatarImage, avatarOk, avatarErr
		}

		avatarOk = true
		return avatarImage, avatarOk, avatarErr
	}

	return avatarImage, avatarOk, avatarErr
}

func (o *Import) getLocalAvatar(user *reactUser, reactAvatarDir string) (image.Image, bool, error) {
	var avatarImage image.Image
	var imageFile *os.File
	var avatarOk bool
	var avatarErr error

	if !doImportLocalAvatars {
		return avatarImage, avatarOk, avatarErr
	}

	if len(user.Icon) == 0 {
		return avatarImage, avatarOk, avatarErr
	}

	avatarPath := reactAvatarDir + fmt.Sprintf("%d/%s", user.UserID, user.Icon)

	imageFile, avatarErr = os.OpenFile(avatarPath, os.O_RDONLY, 0644)
	if avatarErr != nil {
		return avatarImage, avatarOk, avatarErr
	}

	avatarImage, _, avatarErr = image.Decode(imageFile)
	if avatarErr != nil {
		return avatarImage, avatarOk, avatarErr
	}

	avatarOk = true
	return avatarImage, avatarOk, avatarErr
}

type reactBookmark struct {
	TopicID   int
	UserID    int
	MessageID int
	Name      string
	// Deleted      bool
	PostReminder bool
	// Folder       int
	// Description  string
}

var titleCleaner = strings.NewReplacer("[sup]", "", "[/sup]", "", "[b]", "", "[/b]", "", "[s]", "", "[/s]", "", "[i]", "", "[/i]", "")

func (o *Import) insertBookmarks() error {
	doImport, ok := o.Config.Get("mfxf2.import_bookmarks").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_bookmarks' in config")
	}

	if !doImport {
		return nil
	}

	privateByDefault, ok := o.Config.Get("mfxf2.bookmarks_private_by_default").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.bookmarks_private_by_default' in config")
	}

	fmt.Print("preparing bookmarks...\n")

	var (
		bookmarkRows *sql.Rows
		topicID      int
		userID       int
		messageID    int
		name         string
		postReminder bool
	)

	bookmarkRows, err = react.Txn.Query(`
        SELECT TopicID, UserID, MessageID, Name, Postreminder
        FROM F_Bookmarks
        WHERE Deleted <> 1
        ORDER BY UserID
    `)
	if err != nil {
		return err
	}

	bookmarks := []*reactBookmark{}

	for bookmarkRows.Next() {
		err = bookmarkRows.Scan(&topicID, &userID, &messageID, &name, &postReminder)
		if err != nil {
			return err
		}

		bookmarks = append(bookmarks, &reactBookmark{
			TopicID:      topicID,
			UserID:       userID,
			MessageID:    messageID,
			Name:         titleCleaner.Replace(html.UnescapeString(name)),
			PostReminder: postReminder,
		})
	}

	_, err = xf2.Txn.Exec(`TRUNCATE TABLE xf_th_bookmark`)
	if err != nil {
		return err
	}

	fmt.Print("inserting bookmarks...\n")
	for _, bookmark := range bookmarks {
		var contentType string
		var contentID int

		if bookmark.MessageID != 0 {
			contentType = "post"
			contentID = bookmark.MessageID
		} else {
			contentType = "thread"
			contentID = bookmark.TopicID
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_th_bookmark
            SET note = ?,
                sticky = ?,
                public = ?,
                user_id = ?,
                content_id = ?,
                content_type = ?,
                bookmark_date = 0
        `, bookmark.Name, bookmark.PostReminder, !privateByDefault, bookmark.UserID, contentID, contentType,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_th_bookmark")
		}
	}

	return err
}
func (o *Import) insertThreadWatches() error {
	doImport, ok := o.Config.Get("mfxf2.import_user_thread_watches").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_user_thread_watches' in config")
	}

	if !doImport {
		return nil
	}

	fmt.Print("inserting thread watches...\n")

	var (
		watchRows *sql.Rows
		userID    int
		topicID   int
		method    string
	)

	watchRows, err = react.Txn.Query(`
		SELECT UserID,
			   TargetID,
			   Method
		FROM F_Trackers
		WHERE Active = 1
		  AND Untill = 0
		  AND DELETED <> 1
		  AND Userstatus <> 'away'
		  AND Filter = ''
		  -- AND Frequency <> 'once'
		  AND Target = 'topic'
    `)
	if err != nil {
		return err
	}

	for watchRows.Next() {
		err = watchRows.Scan(&userID, &topicID, &method)
		if err != nil {
			return AddErrorInfo(err, "select from F_Trackers")
		}

		emailSubscribe := strings.Contains(method, "email")

		_, err = xf2.Txn.Exec(`
			INSERT IGNORE INTO xf_thread_watch
			SET user_id = ?,
				thread_id = ?,
				email_subscribe = ?
		`, userID, topicID, emailSubscribe,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_thread_watch")
		}
	}

	return err
}

func (o *Import) insertFollowers() error {
	doImport, ok := o.Config.Get("mfxf2.import_followers").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_followers' in config")
	}

	if !doImport {
		return nil
	}

	fmt.Print("preparing followers...\n")

	var (
		contactRows *sql.Rows
		userID      int
		contactID   int
	)

	contactRows, err = react.Txn.Query(`
        SELECT UserID, ContactID
        FROM F_Usercontacts
    `)
	if err != nil {
		return err
	}

	userFollows := map[int][]int{}

	for contactRows.Next() {
		err = contactRows.Scan(&userID, &contactID)
		if err != nil {
			return err
		}

		userFollows[userID] = append(userFollows[userID], contactID)
	}

	importMotorstekBuddies, ok := o.Config.Get("mfxf2.import_motorstek_buddies_as_followers").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_motorstek_buddies_as_followers' in config")
	}
	importMotorstekBuddiesBidirectional, ok := o.Config.Get("mfxf2.import_motorstek_buddies_bidirectional").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_motorstek_buddies_bidirectional' in config")
	}
	var (
		buddyRows    *sql.Rows
		buddyID      int
		allowConfirm bool
	)

	if importMotorstekBuddies {
		buddyRows, err = react.Txn.Query(`
            SELECT BuddyID, UserID, AllowConfirm
            FROM S_BuddyUsers
        `)
		if err != nil {
			return err
		}

		for buddyRows.Next() {
			err = buddyRows.Scan(&buddyID, &userID, &allowConfirm)
			if err != nil {
				return err
			}

			userFollows[userID] = append(userFollows[userID], buddyID)

			if importMotorstekBuddiesBidirectional && allowConfirm {
				userFollows[buddyID] = append(userFollows[buddyID], userID)
			}
		}
	}

	fmt.Print("inserting followers...\n")
	for userID, followUserIDs := range userFollows {
		for _, followUserID := range followUserIDs {
			_, err = xf2.Txn.Exec(`
                INSERT IGNORE INTO xf_user_follow
                SET user_id = ?,
                    follow_user_id = ?,
                    follow_date = ?
            `, userID, followUserID, currentTimeEpoch,
			)
			if err != nil {
				return AddErrorInfo(err, "insert into xf_user_follow")
			}
		}
	}

	return err
}
