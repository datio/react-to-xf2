package mfxf2

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"html"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/Machiel/slugify"
	"github.com/cespare/xxhash"
)

type reactTopic struct {
	Name                 string
	TopicID              int
	ForumID              int
	PollID               int
	UserID               int
	LastPosterID         int
	Status               string // multiple: Sticky, Open, Closed, Closed_Sticky
	Date                 int
	PublicationDate      int
	Deleted              bool
	Note                 string
	Data                 string // Contains thread tags.
	MessageCount         int
	ReportCount          int
	LastMessageTimestamp int
	LastMessageID        int
	ViewCount            int
	FirstMessageID       int
	TopicAliasID         int

	Tags      []*tag
	CacheHash string
}

type reportComment struct {
	ReportCommentID int
	ReportID        int
	CommentDate     int
	UserID          int
	Username        string
	Message         string
	IsReport        bool
}

type reactTopicReport struct {
	ReportID int
	TopicID  int
	UserID   int  // The user who first reported the thread.
	Deleted  bool // The report has been resolved if this is true.

	FirstMessageID       int
	FirstMessageUserID   int
	ContentInfo          string
	FirstReportDate      int
	ReportState          string // open, assigned, resolved, rejected
	LastModifiedUserID   int
	LastModifiedUsername string
	Comments             []*reportComment
}

type xfPollResponse struct {
	PollResponseID    int
	Response          string
	ResponseVoteCount int
	Voters            string
}

type xfPoll struct {
	PollID              int
	ContentID           int
	Question            string
	ResponsesSerialized string
	VoterCount          int

	Responses []*xfPollResponse
	Votes     map[int][]int // VoteID -> []UserID
}

var mediaBBCodeURL string

func (o *Import) importPosts() error {
	doImportThreads, ok := o.Config.Get("mfxf2.import_threads").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_threads' in config")
	}

	doImportPosts, ok := o.Config.Get("mfxf2.import_posts").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_posts' in config")
	}

	mediaBBCodeURL, ok = o.Config.Get("mfxf2.media_bbcode_url").(string)
	if !ok {
		return errors.New("could not find value of 'mfxf2.media_bbcode_url' in config")
	}

	if doImportThreads {
		fmt.Println("preparing threads...")
		threadIDToHashCacheMap := map[int]string{}
		err = o.prepareThreadCacheMap(threadIDToHashCacheMap)
		if err != nil {
			return err
		}

		threads := []*reactTopic{}
		err = o.prepareThreads(&threads, threadIDToHashCacheMap)
		if err != nil {
			return err
		}

		threadReports := map[int]*reactTopicReport{}
		err = o.prepareThreadReports(threadReports)
		if err != nil {
			return err
		}

		threadPolls := map[int]*xfPoll{}
		err = o.prepareThreadPolls(threadPolls)
		if err != nil {
			return err
		}

		err = o.insertThreadData(threadPolls, threadReports, &threads)
		if err != nil {
			return err
		}
	}

	if doImportPosts {
		err = o.prepareMediaReplacementsMap()
		if err != nil {
			return err
		}

		err = o.preparePostCacheMap()
		if err != nil {
			return err
		}

		err = o.insertPosts()
		if err != nil {
			return err
		}

		err = o.rebuildThreadReplyCounts()
		if err != nil {
			return err
		}
	}

	return err
}

func (o *Import) createThreadCache() error {
	_, err = xf2.Txn.Exec(`
        CREATE TABLE IF NOT EXISTS xf_datio_mfxf2_thread_cache (
            thread_id INT NOT NULL,
            thread_hash VARCHAR(16) NOT NULL DEFAULT '',
            PRIMARY KEY (thread_id)
        )
    `)

	return err
}

func (o *Import) prepareThreadCacheMap(tcm map[int]string) error {
	var (
		threadCacheRows *sql.Rows
		threadID        int
		threadHash      string
	)

	threadCacheRows, err = xf2.Txn.Query(`
        SELECT thread_id,
               thread_hash
        FROM xf_datio_mfxf2_thread_cache
        ORDER BY thread_id
    `)
	if err != nil {
		return err
	}

	for threadCacheRows.Next() {
		err = threadCacheRows.Scan(&threadID, &threadHash)
		if err != nil {
			return err
		}

		tcm[threadID] = threadHash
	}

	return err
}

func (o *Import) prepareThreads(threads *[]*reactTopic, tcm map[int]string) error {
	var (
		threadRows           *sql.Rows
		name                 string
		topicID              int
		forumID              int
		pollID               int
		userID               int
		lastPosterID         int
		status               string
		date                 int
		publicationDate      int
		deleted              bool
		note                 string
		data                 string
		messageCount         int
		reportCount          int
		lastMessageTimestamp int
		lastMessageID        int
		viewCount            int
		firstMessageID       int
		topicAliasID         int
		firstMessageContents string
	)

	threadRows, err = react.Txn.Query(`
        SELECT Name,
               TopicID,
               ForumID,
               PollID,
               UserID,
               LastposterID,
               Status,
               Date,
               PublicationDate,
               IF(Deleted = '', 0, Deleted) AS Deleted,
               Note,
               Data,
               Messagecount,
               Reportcount,
               Lastmessage,
               LastmessageID,
               Viewcount,
               FirstmessageID,
               TopicaliasID,
               AverageRating
        FROM F_Topics
    `)
	if err != nil {
		// if strings.Contains(err.Error(), "driver: bad connection" {
		//  todo: Re-connect.
		// }
		return AddErrorInfo(err, "select from F_Topics")
	}

	for threadRows.Next() {
		err = threadRows.Scan(&name, &topicID, &forumID, &pollID, &userID, &lastPosterID, &status, &date, &publicationDate, &deleted, &note, &data, &messageCount, &reportCount,
			&lastMessageTimestamp, &lastMessageID, &viewCount, &firstMessageID, &topicAliasID, &firstMessageContents)
		if err != nil {
			return err
		}

		currentRecordHash := fmt.Sprintf("%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v", name, topicID, forumID, pollID, userID, lastPosterID, status, date,
			publicationDate, deleted, note, data, messageCount, reportCount, lastMessageTimestamp, lastMessageID, viewCount, firstMessageID, topicAliasID, firstMessageContents)

		x := xxhash.New()
		_, err = x.Write([]byte(currentRecordHash))
		if err != nil {
			return err
		}
		currentRecordHash = fmt.Sprintf("%x", x.Sum64())

		hash, ok := tcm[topicID]
		if ok && hash == currentRecordHash {
			continue
		}

		if ok {
			fmt.Printf("data of thread id '%d' has been changed since the previous import\n", topicID)
		}

		name = o.removeTitleBBCodes(html.UnescapeString(name))
		if len(name) == 0 {
			name = fmt.Sprintf("Topic #%d", topicID)
		}

		if date == 0 {
			date = publicationDate
		}

		if lastMessageTimestamp == 0 {
			lastMessageTimestamp = date
		}

		*threads = append(*threads, &reactTopic{
			Name:                 name,
			TopicID:              topicID,
			ForumID:              forumID,
			PollID:               pollID,
			UserID:               userID,
			LastPosterID:         lastPosterID,
			Status:               status,
			Date:                 date,
			PublicationDate:      publicationDate,
			Deleted:              deleted,
			Note:                 note,
			Data:                 data,
			MessageCount:         messageCount,
			ReportCount:          reportCount,
			LastMessageTimestamp: lastMessageTimestamp,
			LastMessageID:        lastMessageID,
			ViewCount:            viewCount,
			FirstMessageID:       firstMessageID,
			TopicAliasID:         topicAliasID,

			CacheHash: currentRecordHash,
		})
	}

	// Prepare tags
	for _, thread := range *threads {
		foundTags := map[string]string{} // The key is the slugified title, while the value is the original one.

		o.determineThreadDataTags(foundTags, thread.Data)
		o.determineThreadTitleTags(foundTags, thread.Name)

		err = o.prepareTags()
		if err != nil {
			return err
		}

		for slugifiedTitle, tagTitle := range foundTags {
			t, ok := tagTitleToTagMap[slugifiedTitle]
			if !ok {
				_, err = o.insertTag(&tag{
					TagTitle: strings.Trim(tagTitle, " 	?!"),
					TagURL:      slugifiedTitle,
					LastUseDate: thread.Date,
				})
				if err != nil {
					return err
				}

				t = tagTitleToTagMap[slugifiedTitle]
			}

			thread.Tags = append(thread.Tags, t)
		}
	}

	return err
}

var threadTagDataRe = regexp.MustCompile(`topic_keywords";s:[0-9]+:"(.*?)";`)

func (o *Import) determineThreadDataTags(foundTags map[string]string, data string) {
	if len(data) < 5 { // Ignores the 'b:0;' value.
		return
	}

	threadTagDataMatch := threadTagDataRe.FindStringSubmatch(data)
	if len(threadTagDataMatch) == 0 {
		return
	}

	tags := strings.Fields(threadTagDataMatch[1])

	for _, t := range tags {
		slugifiedTitle := slugify.Slugify(t)

		_, ok := foundTags[slugifiedTitle]
		if !ok {
			foundTags[slugifiedTitle] = t
		}
	}
}

var threadTagFromTitleBrackets = regexp.MustCompile(`\[\s*([^\[]*?)\s*\]`)

func (o *Import) determineThreadTitleTags(foundTags map[string]string, title string) {
	threadTagTitleMatch := threadTagFromTitleBrackets.FindAllStringSubmatch(title, -1)

	if len(threadTagTitleMatch) == 0 {
		return
	}

	for _, tSubmatch := range threadTagTitleMatch {
		if strings.Contains(tSubmatch[1], `/`) {
			return
		}

		slugifiedTitle := slugify.Slugify(tSubmatch[1])

		_, ok := foundTags[slugifiedTitle]
		if !ok {
			foundTags[slugifiedTitle] = tSubmatch[1]
		}
	}
}

func (o *Import) prepareThreadReports(threadReports map[int]*reactTopicReport) error {
	var (
		reportRows          *sql.Rows
		reportID            int
		topicID             int
		userID              int
		date                int
		content             string
		reportDeleted       bool
		topicDeleted        bool
		firstMessageID      int
		firstMessageContent string
		firstMessageUserID  int
		forumID             int
		forumName           string // todo: Add a cache map for forum IDs to names.
		topicName           string
	)

	reportRows, err = react.Txn.Query(`
        SELECT F_Topicreports.ReportID,
               F_Topicreports.TopicID,
               F_Topicreports.UserID,
               F_Topicreports.Date,
               F_Topicreportsraw.Content,
               IF(F_Topicreports.Deleted = '', 0, F_Topicreports.Deleted) AS ReportDeleted,
               IF(F_Topics.Deleted = '', 0, F_Topics.Deleted) AS TopicDeleted,
               F_Topics.FirstmessageID,
               F_Messagesraw.Content AS FirstmessageContent, # NULL error for index 8 is explicitly checked below.
               F_Messages.UserID AS FirstMessageUserID, # Also NULL when F_Messagesraw.Content is NULL.
               F_Topics.ForumID,
               F_Forums.Name AS ForumName,
               F_Topics.Name AS TopicName
        FROM F_Topicreports
        LEFT  JOIN F_Topicreportsraw ON F_Topicreportsraw.ReportID = F_Topicreports.ReportID
        LEFT JOIN F_Topics ON F_Topics.TopicID = F_Topicreports.TopicID
        LEFT JOIN F_Messages ON F_Messages.MessageID = F_Topics.FirstMessageID
        LEFT JOIN F_Messagesraw ON F_Messagesraw.MessageID = F_Topics.FirstMessageID
        LEFT JOIN F_Forums ON F_Forums.ForumID = F_Topics.ForumID
        ORDER BY TopicID, Date
    `)
	if err != nil {
		return err
	}

	lastReportTopicID := 0
	for reportRows.Next() {
		err = reportRows.Scan(&reportID, &topicID, &userID, &date, &content, &reportDeleted, &topicDeleted, &firstMessageID, &firstMessageContent, &firstMessageUserID, &forumID, &forumName, &topicName)
		if err != nil {
			if err.Error() == "sql: Scan error on column index 8: unsupported Scan, storing driver.Value type <nil> into type *string" {
				continue
			}
			if strings.Contains(err.Error(), "sql: Scan error on column index 6: sql/driver: couldn't convert <nil> (<nil>) into type bool") {
				continue
			}
			return AddErrorInfo(err, "select reports")
		}

		isDeleted := reportDeleted
		if topicDeleted {
			isDeleted = true
		}

		reportState := "open"
		if isDeleted {
			reportState = "resolved"
		}

		username, ok := userIDToUsernameMap[userID]
		if !ok {
			username = "[N/A]"
		}

		forumName = html.UnescapeString(forumName)

		topicName = html.UnescapeString(topicName)

		// todo: Requires full BBCode replacements (quotes, img->smilies, etc.). Investigate further as soon the post import action is ready.
		firstMessageContent = o.convertSizeRelatedBBCodes(o.convertListRelatedBBCodes(o.normalizeSlashOnlyClosingTag(firstMessageContent)))

		contentInfo := fmt.Sprintf(`a:9:{s:7:"message";s:%d:"%s";s:7:"node_id";i:%d;s:9:"node_name";N;s:10:"node_title";s:%d:"%s";s:7:"post_id";i:%d;s:9:"thread_id";i:%d;s:12:"thread_title";s:%d:"%s";s:7:"user_id";i:%d;s:8:"username";s:%d:"%s";}`,
			len(firstMessageContent), firstMessageContent, forumID, len(forumName), forumName, firstMessageID, topicID, len(topicName), topicName, userID, len(username), username)

		if lastReportTopicID != topicID {
			threadReports[topicID] = &reactTopicReport{
				ReportID:             reportID,
				TopicID:              topicID,
				FirstMessageID:       firstMessageID,
				FirstMessageUserID:   firstMessageUserID,
				UserID:               userID,
				Deleted:              isDeleted,
				ContentInfo:          contentInfo,
				FirstReportDate:      date,
				ReportState:          reportState,
				LastModifiedUserID:   userID,
				LastModifiedUsername: userIDToUsernameMap[userID],
			}
		}

		threadReports[topicID].Comments = append(threadReports[topicID].Comments, &reportComment{
			ReportCommentID: reportID,
			ReportID:        threadReports[topicID].ReportID,
			CommentDate:     date,
			UserID:          userID,
			Username:        username,
			Message:         content,
			IsReport:        true,
		})

		lastReportTopicID = topicID
	}

	return err
}

func (o *Import) prepareThreadPolls(threadPolls map[int]*xfPoll) error {
	var (
		pollRows     *sql.Rows
		pollID       int
		title        string
		count        int
		start        int
		topicID      int
		fixedTopicID int

		voteID int // PollOptions
		userID int // PollVotes
	)

	pollRows, err = react.Txn.Query(`
        SELECT F_Polls.PollID,
               Title,
               Count,
               Start,
               F_Polls.TopicID,
               F_Topics.TopicID AS FixedTopicID
        FROM F_Polls
        INNER JOIN F_Topics ON F_Topics.PollID = F_Polls.PollID
        ORDER BY PollID
    `)
	if err != nil {
		return err
	}

	for pollRows.Next() {
		err = pollRows.Scan(&pollID, &title, &count, &start, &topicID, &fixedTopicID)
		if err != nil {
			return err
		}

		if topicID == 0 {
			topicID = fixedTopicID
		}

		threadPolls[pollID] = &xfPoll{
			PollID:     pollID,
			ContentID:  topicID,
			Question:   html.UnescapeString(title),
			VoterCount: count,

			Responses: []*xfPollResponse{},
			Votes:     map[int][]int{},
		}
	}

	pollRows, err = react.Txn.Query(`
        SELECT UserID,
               PollID,
               VoteID
        FROM F_Pollvotes
        WHERE UserID > 0 AND VoteID > 0 
        ORDER BY PollID
    `)
	if err != nil {
		return err
	}

	for pollRows.Next() {
		err = pollRows.Scan(&userID, &pollID, &voteID)
		if err != nil {
			return err
		}

		_, ok := threadPolls[pollID]
		if !ok {
			continue
		}

		threadPolls[pollID].Votes[voteID] = append(threadPolls[pollID].Votes[voteID], userID)
	}

	pollRows, err = react.Txn.Query(`
        SELECT VoteID,
               PollID,
               Title,
               Count
        FROM F_Polloptions
        ORDER BY PollID
    `)
	if err != nil {
		return err
	}

	for pollRows.Next() {
		err = pollRows.Scan(&voteID, &pollID, &title, &count)
		if err != nil {
			return err
		}

		_, ok := threadPolls[pollID]
		if !ok {
			continue
		}

		voters := fmt.Sprintf("a:%d:{", len(threadPolls[pollID].Votes[voteID]))
		for _, userID := range threadPolls[pollID].Votes[voteID] {
			voters += fmt.Sprintf("i:%d;b:1;", userID)
		}
		voters += "}"

		threadPolls[pollID].Responses = append(threadPolls[pollID].Responses, &xfPollResponse{
			PollResponseID:    voteID,
			Response:          html.UnescapeString(title),
			ResponseVoteCount: count,
			Voters:            voters,
		})
	}

	for _, threadPoll := range threadPolls {
		responses := fmt.Sprintf("a:%d:{", len(threadPoll.Responses))
		for _, response := range threadPoll.Responses {
			responses += fmt.Sprintf(`i:%d;a:3:{s:8:"response";s:%d:"%s";s:19:"response_vote_count";i:%d;s:6:"voters";%s}`,
				response.PollResponseID, len(response.Response), response.Response, response.ResponseVoteCount, response.Voters)
		}
		responses += "}"

		threadPoll.ResponsesSerialized = responses
	}

	return err
}

func (o *Import) insertThreadData(threadPolls map[int]*xfPoll, threadReports map[int]*reactTopicReport, threads *[]*reactTopic) error {
	fmt.Print("inserting threads.")
	var countThousand int
	var countTen int
	for _, thread := range *threads {
		countThousand++
		if countThousand == 1000 {
			countTen++
			if countTen == 10 {
				fmt.Print("\ninserting threads")
				countTen = 0
			}

			fmt.Print(".")
			countThousand = 1
		}

		threadTags := fmt.Sprintf("a:%d:{", len(thread.Tags))
		for _, t := range thread.Tags {
			threadTags += fmt.Sprintf(`i:%d;a:2:{s:3:"tag";s:%d:"%s";s:7:"tag_url";s:%d:"%s";}`,
				t.TagID, len(t.TagTitle), t.TagTitle, len(t.TagURL), t.TagURL,
			)
			_, err = xf2.Txn.Exec(`
                INSERT IGNORE INTO xf_tag_content
                SET content_type = 'thread',
                    content_id = ?,
                    tag_id = ?,
                    add_user_id = ?,
                    add_date = ?,
                    visible = 1,
                    content_date = ?
                ON DUPLICATE KEY
                UPDATE content_date = ?
            `,
				thread.TopicID, t.TagID, thread.UserID, thread.Date, thread.Date,
				thread.Date,
			)
			if err != nil {
				return err
			}

		}
		threadTags += "}"

		username, ok := userIDToUsernameMap[thread.UserID]
		if !ok {
			username = "N/A"
		}

		lastPostUsername, ok := userIDToUsernameMap[thread.LastPosterID]
		if !ok {
			lastPostUsername = "N/A"
		}

		threadDate := thread.Date
		if thread.PublicationDate > 0 && thread.PublicationDate != thread.Date {
			threadDate = thread.PublicationDate
		}

		isSticky := strings.Contains(thread.Status, "Sticky")

		discussionState := "visible"
		if thread.Deleted {
			discussionState = "deleted"
		}

		isOpen := strings.Contains(thread.Status, "Open") || !strings.Contains(thread.Status, "Closed")

		discussionType := ""
		if thread.TopicAliasID != 0 {
			discussionType = "redirect"

			_, err = xf2.Txn.Exec(`
                INSERT IGNORE INTO xf_thread_redirect
                SET thread_id = ?,
                    target_url = ?,
                    redirect_key = ?
            `,
				thread.TopicID,
				fmt.Sprintf("threads/%d", thread.TopicAliasID),
				fmt.Sprintf("threads-%d", thread.TopicAliasID),
			)
			if err != nil {
				return err
			}
		} else if thread.PollID != 0 {
			discussionType = "poll"
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_thread
            SET thread_id = ?,
                node_id = ?,
                title = ?,
                reply_count = ?,
                view_count = ?,
                user_id = ?,
                username = ?,
                post_date = ?,
                sticky = ?,
                discussion_state = ?,
                discussion_open = ?,
                discussion_type = ?,
                first_post_id = ?,
                last_post_date = ?,
                last_post_id = ?,
                last_post_user_id = ?,
                last_post_username = ?,
                tags = ?,
                custom_fields = 'a:0:{}'
            ON DUPLICATE KEY
            UPDATE node_id = ?,
                   title = ?,
                   reply_count = ?,
                   view_count = ?,
                   user_id = ?,
                   username = ?,
                   post_date = ?,
                   sticky = ?,
                   discussion_state = ?,
                   discussion_open = ?,
                   discussion_type = ?,
                   first_post_id = ?,
                   last_post_date = ?,
                   last_post_id = ?,
                   last_post_user_id = ?,
                   last_post_username = ?,
                   tags = ?
        `,
			thread.TopicID, thread.ForumID, thread.Name, thread.MessageCount, thread.ViewCount, thread.UserID, username, threadDate, isSticky, discussionState, isOpen,
			discussionType, thread.FirstMessageID, thread.LastMessageTimestamp, thread.LastMessageID, thread.LastPosterID, lastPostUsername, threadTags,
			thread.ForumID, thread.Name, thread.MessageCount, thread.ViewCount, thread.UserID, username, threadDate, isSticky, discussionState, isOpen, discussionType, thread.FirstMessageID, thread.LastMessageTimestamp, thread.LastMessageID, thread.LastPosterID, lastPostUsername, threadTags,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_thread")
		}

		// Insert a dummy post.
		_, err = xf2.Txn.Exec(`
            INSERT IGNORE INTO xf_post
            SET post_id = ?,
                thread_id = ?,
                user_id = ?,
                username = ?,
                post_date = ?,
                message = '',
                position = 0,
                like_users = 'a:0:{}',
                warning_message = '',
                embed_metadata = '[]'
        `, thread.FirstMessageID, thread.TopicID, thread.UserID, username, threadDate)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_post")
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_datio_mfxf2_thread_cache
            SET thread_id = ?,
            thread_hash = ?
            ON DUPLICATE KEY
            UPDATE thread_hash = ?
        `,
			thread.TopicID, thread.CacheHash,
			thread.CacheHash,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_datio_mfxf2_thread_cache")
		}
	}
	fmt.Print("\n")

	fmt.Println("inserting thread reports and polls...")
	for _, threadReport := range threadReports {
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_report
            SET report_id = ?,
                content_type = 'post',
                content_id = ?,
                content_user_id = ?,
                content_info = ?,
                first_report_date = ?,
                report_state = ?,
                assigned_user_id = 0,
                comment_count = ?,
                last_modified_date = ?,
                last_modified_user_id = ?,
                last_modified_username = ?,
                report_count = ?
            ON DUPLICATE KEY
            UPDATE content_type = 'post',
                   content_id = ?,
                   content_user_id = ?,
                   content_info = ?,
                   first_report_date = ?,
                   report_state = ?,
                   assigned_user_id = 0,
                   comment_count = ?,
                   last_modified_date = ?,
                   last_modified_user_id = ?,
                   last_modified_username = ?,
                   report_count = ?
        `,
			threadReport.ReportID, threadReport.FirstMessageID, threadReport.FirstMessageUserID, threadReport.ContentInfo, threadReport.FirstReportDate, threadReport.ReportState,
			len(threadReport.Comments), threadReport.FirstReportDate, threadReport.LastModifiedUserID, threadReport.LastModifiedUsername, len(threadReport.Comments),
			threadReport.FirstMessageID, threadReport.FirstMessageUserID, threadReport.ContentInfo, threadReport.FirstReportDate, threadReport.ReportState,
			len(threadReport.Comments), threadReport.FirstReportDate, threadReport.LastModifiedUserID, threadReport.LastModifiedUsername, len(threadReport.Comments),
		)
		if err != nil {
			return err
		}

		isFirstComment := true
		for _, comment := range threadReport.Comments {
			stateChange := ""
			if isFirstComment {
				stateChange = "open"
				isFirstComment = false
			}

			_, err = xf2.Txn.Exec(`
                INSERT INTO xf_report_comment
                SET report_comment_id = ?,
                    report_id = ?,
                    comment_date = ?,
                    user_id = ?,
                    username = ?,
                    message = ?,
                    state_change = ?,
                    is_report = 1
                ON DUPLICATE KEY
                UPDATE report_id = ?,
                       comment_date = ?,
                       user_id = ?,
                       username = ?,
                       message = ?,
                       state_change = ?,
                       is_report = 1
            `,
				comment.ReportCommentID, threadReport.ReportID, comment.CommentDate, comment.UserID, comment.Username, comment.Message, stateChange,
				threadReport.ReportID, comment.CommentDate, comment.UserID, comment.Username, comment.Message, stateChange,
			)
			if err != nil {
				return err
			}
		}
	}

	for _, threadPoll := range threadPolls {
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_poll
            SET poll_id = ?,
                content_type = 'thread',
                content_id = ?,
                question = ?,
                responses = ?,
                voter_count = ?
            ON DUPLICATE KEY
            UPDATE question = ?,
                   responses = ?,
                   voter_count = ?
        `,
			threadPoll.PollID, threadPoll.ContentID, threadPoll.Question, threadPoll.ResponsesSerialized, threadPoll.VoterCount,
			threadPoll.Question, threadPoll.ResponsesSerialized, threadPoll.VoterCount,
		)
		if err != nil {
			return err
		}

		for _, response := range threadPoll.Responses {
			_, err = xf2.Txn.Exec(`
                INSERT INTO xf_poll_response
                SET poll_response_id = ?,
                    poll_id = ?,
                    response = ?,
                    response_vote_count = ?,
                    voters = ?
                ON DUPLICATE KEY
                UPDATE poll_id = ?,
                       response = ?,
                       response_vote_count = ?,
                       voters = ?
            `,
				response.PollResponseID, threadPoll.PollID, response.Response, response.ResponseVoteCount, response.Voters,
				threadPoll.PollID, response.Response, response.ResponseVoteCount, response.Voters,
			)
			if err != nil {
				return err
			}
		}

		for voteID, userIDs := range threadPoll.Votes {
			for _, userID := range userIDs {
				_, err = xf2.Txn.Exec(`
                    INSERT IGNORE INTO xf_poll_vote
                    SET user_id = ?,
                        poll_response_id = ?,
                        poll_id = ?,
                        vote_date = ?
                `,
					userID, voteID, threadPoll.PollID, currentTimeEpoch,
				)
				if err != nil {
					return err
				}
			}
		}
	}

	return err
}

func (o *Import) createPostCache() error {
	_, err = xf2.Txn.Exec(`
        CREATE TABLE IF NOT EXISTS xf_datio_mfxf2_post_cache (
            post_id INT NOT NULL,
            post_hash VARCHAR(16) NOT NULL DEFAULT '',
            PRIMARY KEY (post_id)
        )
    `)

	return err
}

// var postIDToUserIDMap = map[int]int{}
var postIDToUserIDCache map[int]int

type quote struct {
	PostID  int
	UserID  int
	Content string
}

var fixOldQuotes bool

func (o *Import) insertPosts() error {
	var ok bool
	fixOldQuotes, ok = o.Config.Get("mfxf2.fix_old_quotes").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.fix_old_quotes' in config")
	}

	var (
		reactPostRows *sql.Rows
		messageID     int
		userID        int
	)

	var cancel context.CancelFunc
	cancel, err = o.create2ndReactTxn()
	if err != nil {
		return err
	}
	defer cancel()

	var (
		topicID           int
		date              int
		content           string
		deleted           bool
		lastUpdateUserID  int
		updateDescription string
		updateTime        int

		cachedPostExists bool

		postPositionInThread int
		lastRecordThreadID   int

		diffRows        *sql.Rows
		diffID          int
		diffUserID      int
		patch           string
		recordtimestamp string
	)

	fmt.Print("inserting posts.")
	reactPostRows, err = react.Txn.Query(`
        SELECT F_Messages.MessageID,
               TopicID,
               UserID,
               Date,
               COALESCE(F_Messagesraw.Content, '') AS Content,
               IF(Deleted = '', 0, Deleted) AS Deleted,
               UpdateUserID AS LastUpdateUserID,
               UpdateDescription, -- This must be concat'd to a new line at the end of the existing content, inside an [EDIT] BBCode.
               UpdateTime
        FROM F_Messages
        LEFT JOIN F_Messagesraw ON F_Messagesraw.MessageID = F_Messages.MessageID
		#WHERE TopicID = 388240 # debug -- todo: Unit tests?
		#WHERE F_Messages.messageID = 9996626 -- Preserve these IDs for diff testing!
		#   OR F_Messages.messageID = 12885986
		#   OR F_Messages.messageID = 4227645
		#   OR F_Messages.messageID = 28315642
		#   OR F_Messages.messageID = 28384885
		#   OR F_Messages.messageID = 6382694
		#   OR F_Messages.messageID = 6383976
		#   OR F_Messages.messageID = 23045987
		#   OR F_Messages.messageID = 28351482 -- A post that contains both img and doclib bbcode tags.
		#   OR F_Messages.messageID = 27577799 -- Posts containing unescaped characters.
		#   OR F_Messages.messageID = 26581096
		ORDER BY TopicID, MessageID
    `)
	if err != nil {
		return AddErrorInfo(err, "select from F_Messagesraw")
	}

	var countThousand int
	var countTen int
	var countThousandCacheHit int
	var countTenCacheHit int
	var commitGuard int
	var lastThreadPostsContent = []*quote{}
	for reactPostRows.Next() {
		err = reactPostRows.Scan(&messageID, &topicID, &userID, &date, &content, &deleted, &lastUpdateUserID, &updateDescription, &updateTime)
		if err != nil {
			if strings.Contains(err.Error(), "sql: Scan error on column index 4: unsupported Scan, storing driver.Value type <nil> into type *string") {
				continue
			}

			return AddErrorInfo(err, "scan F_Messages")
		}

		content = html.UnescapeString(content)

		currentRecordHash := fmt.Sprintf("%v|%v|%v|%v|%v|%v|%v|%v|%v", topicID, messageID, userID, date, content, deleted, lastUpdateUserID, updateDescription, updateTime)
		x := xxhash.New()
		_, err = x.Write([]byte(currentRecordHash))
		if err != nil {
			return err
		}
		currentRecordHash = fmt.Sprintf("%x", x.Sum64())

		if lastRecordThreadID != topicID {
			lastRecordThreadID = topicID
			postPositionInThread = 0
			lastThreadPostsContent = lastThreadPostsContent[:0]
			postIDToUserIDCache = map[int]int{}
		}

		cachedPostExists = postCacheMap[fmt.Sprintf("%d|%s", messageID, currentRecordHash)]

		if cachedPostExists {
			countThousandCacheHit++
			if countThousandCacheHit == 1000 {
				countTenCacheHit++
				if countTenCacheHit == 10 {
					fmt.Print("\ninserting posts")
					countTenCacheHit = 0
				}

				fmt.Print(" X")
				countThousandCacheHit = 1
			}

			postPositionInThread++
			continue
		}

		countThousand++
		if countThousand == 1000 {
			countTen++
			if countTen == 10 {
				fmt.Printf(" (inserted new or updated posts until thread: %d post: %d)", topicID, messageID)
				fmt.Print("\ninserting posts")
				countTen = 0
			}

			fmt.Print(".")
			countThousand = 1
		}

		commitGuard++
		if commitGuard >= 10000 {
			err = xf2.Txn.Commit()
			if err != nil {
				return err
			}

			xf2.Txn, err = xf2.Db.Begin()
			if err != nil {
				return err
			}

			commitGuard = 0
		}

		username, ok := userIDToUsernameMap[userID]
		if !ok {
			username = "[N/A]"
		}

		messageState := "visible"
		if deleted {
			messageState = "deleted"
		}

		if userID < 0 {
			userID = 0
		}
		if lastUpdateUserID < 0 {
			lastUpdateUserID = 0
		}

		initialContent := content
		content = o.normalizeSlashOnlyClosingTag(content)
		content = o.convertListRelatedBBCodes(content)
		content = o.convertSizeRelatedBBCodes(content)
		content = imgBBSmileyRe.ReplaceAllString(content, ":$1:")
		content = noHTMLBBCodeRe.ReplaceAllString(content, "")

		var contentWithQuotesRemoved string

		content, contentWithQuotesRemoved, err = o.normalizeQuotes(content, &lastThreadPostsContent)
		if err != nil {
			return err
		}

		content = o.convertMediaRelatedBBCodes(content)

		diffRows, err = react.Txn2.Query(`
            SELECT DiffID, -- Gets imported as the edit_history_id.
                   UserID,
                   Patch,
                   recordtimestamp
            FROM F_Diffs
            WHERE SourceID = ?
              AND Patch <> ''
            ORDER BY DiffID DESC
        `, messageID)
		if err != nil {
			return err
		}

		editCount := 0
		diffContentWithQuotesRemoved := ""
		for diffRows.Next() {
			err = diffRows.Scan(&diffID, &diffUserID, &patch, &recordtimestamp)
			if err != nil {
				return err
			}

			initialContent = o.patchUnified(initialContent, patch, diffID)

			diffContentWithQuotesRemoved += "\n" + o.stripQuotes(initialContent)

			_ = diffContentWithQuotesRemoved

			var t time.Time
			t, err = time.Parse(timestampLayout, recordtimestamp)
			if err != nil {
				return err
			}

			_, err = xf2.Txn.Exec(`
                INSERT INTO xf_edit_history
                SET edit_history_id = ?,
                    content_type = 'post',
                    content_id = ?,
                    edit_user_id = ?,
                    edit_date = ?,
					old_text = ?
				ON DUPLICATE KEY
				UPDATE content_type = 'post',
                       content_id = ?,
                       edit_user_id = ?,
                       edit_date = ?,
                       old_text = ?
			`,
				diffID, messageID, diffUserID, t.Unix(), initialContent,
				messageID, diffUserID, t.Unix(), initialContent,
			)
			if err != nil {
				return err
			}

			editCount++
		}

		if fixOldQuotes {
			lastThreadPostsContent = append(lastThreadPostsContent, &quote{
				PostID:  messageID,
				UserID:  userID,
				Content: contentWithQuotesRemoved + diffContentWithQuotesRemoved,
			})
		}

		// Insert the post.
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_post
            SET post_id = ?,
                thread_id = ?,
                user_id = ?,
                username = ?,
                post_date = ?,
                message = ?,
                message_state = ?,
                position = ?,
                like_users = 'a:0:{}',
                warning_message = '',
                last_edit_date = ?,
				last_edit_user_id = ?,
				edit_count = ?,
                embed_metadata = '[]'
            ON DUPLICATE KEY
            UPDATE thread_id = ?,
                   user_id = ?,
                   username = ?,
                   post_date = ?,
                   message = ?,
                   message_state = ?,
                   position = ?,
                   last_edit_date = ?,
				   last_edit_user_id = ?,
				   edit_count = ?
        `,
			messageID, topicID, userID, username, date, content, messageState, postPositionInThread, updateTime, lastUpdateUserID, editCount,
			topicID, userID, username, date, content, messageState, postPositionInThread, updateTime, lastUpdateUserID, editCount,
		)
		if err != nil {
			return err
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_datio_mfxf2_post_cache
            SET post_id = ?,
                post_hash = ?
            ON DUPLICATE KEY
            UPDATE post_hash = ?
        `,
			messageID, currentRecordHash,
			currentRecordHash,
		)
		if err != nil {
			return err
		}

		postPositionInThread++
	}
	fmt.Print("\n")

	return err
}

var quoteWithMessageIDRe1 = regexp.MustCompile(`(?is)\[\s*quote\s*\]\s*(?:\[\s*b\s*\])?\s*\[\s*message\s*=\s*([0-9]*)\s*\]\s*Op\s*[a-zA-z]+\s*[0-9]{2}\s*[a-zA-Z]+\s*2[0-9]{3}\s*[0-9]{2}\s*:\s*[0-9]{2}\s*schreef\s*.*?\[\/message\]\s*(?:\[\s*\/b\s*\])?`)

var quoteWithMessageIDRe2 = regexp.MustCompile(`(?is)\[\s*quote\s*\]\s*\[\s*message\s*=\s*([0-9]*)\s*\]\s*(?:\[\s*b\s*\])?Op.*?\[\/b\].*?schreef.*?:\s*\[\/message\]`)

var quoteWithMessageIDRe3 = regexp.MustCompile(`(?is)\[\s*quote\s*\]\s*(?:\[\s*b\s*\])?\s*\[\s*message\s*=\s*([0-9]*)\s*,\s*noline\s*\].*?\s*schreef\s*op\s*[0-9]{2}\s*[a-zA-Z]+\s*2[0-9]{3}\s*@.*?\[\s*\/message\s*\]\s*:\s*\[\s*\/b\s*\]`)

var quoteNoMessageIDRe1 = regexp.MustCompile(`(?is)(\[\s*quote\s*\]\s*Op\s*2[0-9]{3}\s*-\s*[0-9]{2}\s*-\s*[0-9]{2}\s*[0-9]{2}\s*:\s*[0-9]{2}\s*,\s*schreef\s*.*?:\s*)(.*?)\s*\[\s*\/\s*quote\s*\]`)

var quoteNoMessageIDRe2 = regexp.MustCompile(`(?is)(\[\s*quote\s*\]\s*\[\s*b\s*]\s*Op\s*[a-zA-Z]+\s*[0-9]{2}\s*[a-zA-Z]+\s*2[0-9]{3}\s*[0-9]{2}\s*:\s*[0-9]{2}\s*schreef\s*.*?\[\s*/b\s*]\s*)(.*?)\s*\[\s*\/\s*quote\s*\]`)

var quotedElipsisRe = regexp.MustCompile(`\s*\[\s*\.\s*\.\s*\.\s*]\s*`)

func (o *Import) normalizeQuotes(content string, lastThreadPostsContent *[]*quote) (string, string, error) {
	lowercaseContent := strings.ToLower(content)

	containsQuoteTag := strings.Contains(lowercaseContent, "[quote")
	containsQuoteInfo := strings.Contains(lowercaseContent, "schreef")

	if !containsQuoteTag || !containsQuoteInfo {
		return content, content, nil
	}

	content = quotedElipsisRe.ReplaceAllString(content, "")

	contentWithQuotesRemoved := o.stripQuotes(content)

	var quotedMessageID, quotedUserID int
	initialContentLength := len(content)

	if strings.Contains(lowercaseContent, "message") {
		// todo: Refactor the copy-pasted blocks of matching the messageID regexps into a separate function.
		subMatch := quoteWithMessageIDRe1.FindStringSubmatch(content)
		if len(subMatch) == 2 && len(subMatch[1]) > 0 {
			quotedMessageID, _ = strconv.Atoi(subMatch[1])
		}
		if quotedMessageID > 0 {
			quotedUserID, err = o.getUserIDForPost(quotedMessageID)
			if err != nil {
				return content, contentWithQuotesRemoved, err
			}
			quotedUsername, ok := userIDToUsernameMap[quotedUserID]
			if !ok {
				quotedUsername = "[N/A]"
			}

			content = quoteWithMessageIDRe1.ReplaceAllString(content, fmt.Sprintf(`[quote="%s, post: %d, member: %d"]`, quotedUsername, quotedMessageID, quotedUserID))
		}

		if len(content) != initialContentLength && !strings.Contains(strings.ToLower(content), "schreef") {
			return content, "", err
		}

		subMatch = quoteWithMessageIDRe2.FindStringSubmatch(content)
		if len(subMatch) == 2 && len(subMatch[1]) > 0 {
			quotedMessageID, _ = strconv.Atoi(subMatch[1])
		}
		if quotedMessageID > 0 {
			quotedUserID, err = o.getUserIDForPost(quotedMessageID)
			if err != nil {
				return content, contentWithQuotesRemoved, err
			}
			quotedUsername, ok := userIDToUsernameMap[quotedUserID]
			if !ok {
				quotedUsername = "[N/A]"
			}

			content = quoteWithMessageIDRe2.ReplaceAllString(content, fmt.Sprintf(`[quote="%s, post: %d, member: %d"]`, quotedUsername, quotedMessageID, quotedUserID))
		}

		if len(content) != initialContentLength && !strings.Contains(strings.ToLower(content), "schreef") {
			return content, "", err
		}

		subMatch = quoteWithMessageIDRe3.FindStringSubmatch(content)
		if len(subMatch) == 2 && len(subMatch[1]) > 0 {
			quotedMessageID, _ = strconv.Atoi(subMatch[1])
		}
		if quotedMessageID > 0 {
			quotedUserID, err = o.getUserIDForPost(quotedMessageID)
			if err != nil {
				return content, contentWithQuotesRemoved, err
			}
			quotedUsername, ok := userIDToUsernameMap[quotedUserID]
			if !ok {
				quotedUsername = "[N/A]"
			}

			content = quoteWithMessageIDRe3.ReplaceAllString(content, fmt.Sprintf(`[quote="%s, post: %d, member: %d"]`, quotedUsername, quotedMessageID, quotedUserID))
		}

		if len(content) != initialContentLength {
			return content, "", err
		}
	}

	// todo: Replace any quotes that do not contain a post ID.

	if !fixOldQuotes {
		return content, contentWithQuotesRemoved, err
	}

	subMatch := quoteNoMessageIDRe1.FindStringSubmatch(content)
	if len(subMatch) == 3 && len(subMatch[2]) > 5 {
		longestLineMatchFound := false

		for _, lastThreadPostContent := range *lastThreadPostsContent {
			if strings.Contains(lastThreadPostContent.Content, subMatch[2]) {
				quotedUserID = lastThreadPostContent.UserID
				quotedMessageID = lastThreadPostContent.PostID
				break
			}

			if !longestLineMatchFound && strings.Contains(lastThreadPostContent.Content, o.getLongestLine(subMatch[2])) {
				quotedUserID = lastThreadPostContent.UserID
				quotedMessageID = lastThreadPostContent.PostID

				longestLineMatchFound = true
			}
		}

		if quotedUserID != 0 {
			quotedUsername, ok := userIDToUsernameMap[quotedUserID]
			if !ok {
				quotedUsername = "[N/A]"
			}

			content = strings.Replace(content, subMatch[1], fmt.Sprintf(`[quote="%s, post: %d, member: %d"]`, quotedUsername, quotedMessageID, quotedUserID), -1)
		}
	}

	if len(content) != initialContentLength && !strings.Contains(strings.ToLower(content), "schreef") {
		return content, contentWithQuotesRemoved, err
	}

	subMatch = quoteNoMessageIDRe2.FindStringSubmatch(content)
	if len(subMatch) == 3 && len(subMatch[2]) > 5 {
		longestLineMatchFound := false

		for _, lastThreadPostContent := range *lastThreadPostsContent {
			if strings.Contains(lastThreadPostContent.Content, subMatch[2]) {
				quotedUserID = lastThreadPostContent.UserID
				quotedMessageID = lastThreadPostContent.PostID
				break
			}

			if !longestLineMatchFound && strings.Contains(lastThreadPostContent.Content, o.getLongestLine(subMatch[2])) {
				quotedUserID = lastThreadPostContent.UserID
				quotedMessageID = lastThreadPostContent.PostID

				longestLineMatchFound = true
			}
		}

		if quotedUserID != 0 {
			quotedUsername, ok := userIDToUsernameMap[quotedUserID]
			if !ok {
				quotedUsername = "[N/A]"
			}

			content = strings.Replace(content, subMatch[1], fmt.Sprintf(`[quote="%s, post: %d, member: %d"]`, quotedUsername, quotedMessageID, quotedUserID), -1)
		}
	}

	return content, contentWithQuotesRemoved, err
}

// todo: Create a proper parser (not a high priority as it works well enough).
var quoteTagAndContentsRe = regexp.MustCompile(`(?is)\[quote.*\[\/quote\]`)

func (o *Import) stripQuotes(content string) string {
	return o.getLongestLine(quoteTagAndContentsRe.ReplaceAllString(content, ""))
}

func (o *Import) getUserIDForPost(postID int) (int, error) {
	userID, ok := postIDToUserIDCache[postID]
	if ok {
		return userID, nil
	}

	err = react.Txn2.QueryRow(`
        SELECT UserID
        FROM F_Messages
        WHERE MessageID = ?
    `, postID).Scan(&userID)

	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			return 0, err
		}
	}

	return userID, err
}

func (o *Import) getLongestLine(content string) string {
	lines := strings.Split(content, "\n")

	longestLineIndex := -1
	longestLineLength := -1

	for i, line := range lines {
		lineLower := strings.ToLower(line)
		if strings.Contains(lineLower, "[img") || strings.Contains(lineLower, "[url") {
			continue
		}

		lineLength := len(line)
		if lineLength > longestLineLength {
			longestLineIndex = i
			longestLineLength = lineLength
		}
	}

	if longestLineLength < 4 {
		return content
	}

	return lines[longestLineIndex]
}

var mediaReplacementsMap = map[int]int{}
var newMediaIDRe = regexp.MustCompile(`([0-9])+\)$`)

func (o *Import) prepareMediaReplacementsMap() error {
	var (
		mediaCacheRows *sql.Rows
		mediaID        int
		log            string
	)

	mediaCacheRows, err = xf2.Txn.Query(`
        SELECT media_id,
               log
        FROM xf_datio_mfxf2_media_cache
        WHERE log LIKE 'failed: duplicate media (%'
    `)
	if err != nil {
		return err
	}

	fmt.Println("preparing media ids to replace in posts...")
	for mediaCacheRows.Next() {
		err = mediaCacheRows.Scan(&mediaID, &log)
		if err != nil {
			return err
		}

		newMediaIDMatch := newMediaIDRe.FindStringSubmatch(log)
		if len(newMediaIDMatch) != 2 {
			continue
		}

		newMediaID, _ := strconv.Atoi(newMediaIDMatch[1])

		mediaReplacementsMap[mediaID] = newMediaID
	}

	return err
}

var doclibBBCodeRe = regexp.MustCompile(`(?is)\[\s*doclib\s*=([0-9]+)\s*,\s*[a-z0-9]+\s*,\s*[0-9]+\s*,\s*[a-z0-9]+\s*,\s*[0-9]]`)
var imgMediaBBCodeRe = regexp.MustCompile(`(?is)http[:a-z0-9.\-/]+php/download_document/([0-9]+)/[a-z0-9]{32}`)

func (o *Import) convertMediaRelatedBBCodes(content string) string {
	contentLower := strings.ToLower(content)

	var doclibExists, linkExists bool
	var submatches = [][]string{}

	if strings.Contains(contentLower, "[doclib") {
		submatches = append(submatches, doclibBBCodeRe.FindAllStringSubmatch(content, -1)...)
		doclibExists = true
	}
	if strings.Contains(contentLower, "/download_document") {
		submatches = append(submatches, imgMediaBBCodeRe.FindAllStringSubmatch(content, -1)...)
		linkExists = true
	}

	if !doclibExists && !linkExists {
		return content
	}

	for _, submatch := range submatches {
		if len(submatch) != 2 {
			continue
		}

		mediaID, _ := strconv.Atoi(submatch[1])

		newMediaID, ok := mediaReplacementsMap[mediaID]
		if ok {
			mediaID = newMediaID
		}

		if doclibExists && strings.Contains(strings.ToLower(submatch[0]), "doclib") {
			content = strings.Replace(content, submatch[0], fmt.Sprintf("[img]%s%d/full[/img]", mediaBBCodeURL, mediaID), -1)
		}
		if linkExists && strings.Contains(strings.ToLower(submatch[0]), "download_document") {
			content = strings.Replace(content, submatch[0], fmt.Sprintf("%s%d/full", mediaBBCodeURL, mediaID), -1)
		}
	}

	return content
}

var postCacheMap = map[string]bool{}

func (o *Import) preparePostCacheMap() error {
	var (
		postCacheRows *sql.Rows
		postID        int
		postHash      string
	)

	postCacheRows, err = xf2.Txn.Query(`
        SELECT post_id,
               post_hash
        FROM xf_datio_mfxf2_post_cache
    `)
	if err != nil {
		return err
	}

	fmt.Println("preparing post cache ids...")
	for postCacheRows.Next() {
		err = postCacheRows.Scan(&postID, &postHash)
		if err != nil {
			return err
		}

		postCacheMap[fmt.Sprintf("%d|%s", postID, postHash)] = true
	}

	return err
}

func (o *Import) rebuildThreadReplyCounts() error {
	fmt.Println("rebuilding thread reply counts...")

	_, err = xf2.Txn.Exec(`
		UPDATE xf_thread
		SET reply_count = GREATEST((
			SELECT COUNT(*)
			FROM xf_post
			WHERE xf_post.thread_id = xf_thread.thread_id
		) - 1, 0)
    `)

	return err
}
