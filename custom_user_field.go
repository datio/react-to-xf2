// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

import (
	"database/sql"
	"errors"
	"fmt"
)

type userField struct {
	FieldID           string
	DisplayGroup      string // ('personal','contact','preferences')
	FieldType         string
	FieldChoices      string
	MatchType         string // ('none','number','alphanumeric','email','url','regex','callback').
	MatchParams       string // regex, validator callback, etc.
	UserEditable      string // ('yes','once','never')
	DisplayTemplate   string
	DisplayOrder      int // Default: 1.
	MaxLength         int
	Required          bool
	ShowRegistration  bool
	ViewableProfile   bool
	ViewableMessage   bool
	ModeratorEditable bool
}

func (o *Import) importCustomUserFields() error {
	doImport, ok := o.Config.Get("mfxf2.import_custom_user_fields").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_custom_user_fields' in config")
	}

	if !doImport {
		return nil
	}

	// Check which user fields have not been imported. Import them.
	userFieldsToInsert := []*userField{}
	var exists bool

	// Check.
	for _, uf := range reactUserFields {
		err = xf2.Txn.QueryRow(`
            SELECT EXISTS(
                SELECT 1 FROM xf_user_field
                WHERE field_id = ?
                LIMIT 1
            )
        `, uf.FieldID).Scan(&exists)
		switch {
		case err == sql.ErrNoRows:
			exists = false
		case err != nil:
			return AddErrorInfo(err, "select exists from xf_user_field")
		}

		if !exists {
			userFieldsToInsert = append(userFieldsToInsert, uf)
		}
	}

	// Insert.
	for _, uf := range userFieldsToInsert {
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_user_field
            SET field_id = ?,
                display_group = ?,
                display_order = ?,
                field_type = ?,
                field_choices = ?,
                match_type = ?,
                match_params = ?,
                max_length = ?,
                required = ?,
                show_registration = ?,
                user_editable = ?,
                viewable_profile = ?,
                viewable_message = ?,
                display_template = ?,
                moderator_editable = ?
		`, uf.FieldID, uf.DisplayGroup, uf.DisplayOrder, uf.FieldType, uf.FieldChoices, uf.MatchType, uf.MatchParams, uf.MaxLength, uf.Required, uf.ShowRegistration,
			uf.UserEditable, uf.ViewableProfile, uf.ViewableMessage, uf.DisplayTemplate, uf.ModeratorEditable,
		)

		if err != nil {
			return AddErrorInfo(err, "insert into xf_user_field")
		}

		fmt.Printf("new user field: '%s'\n", uf.FieldID)
	}

	err = o.importPhrases(reactUserFieldPhrases)

	return err
}
