// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"html"

	"github.com/cespare/xxhash"
)

func (o *Import) importConversations() error {
	doImportConversations, ok := o.Config.Get("mfxf2.import_conversations").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_conversations' in config")
	}

	if !doImportConversations {
		return nil
	}

	// Insert conversation.
	err = o.insertConversations()
	if err != nil {
		return err
	}

	var cancel context.CancelFunc
	cancel, err = o.create2ndReactTxn()
	if err != nil {
		return err
	}
	defer cancel()

	// Insert conversation messages.
	err = o.insertConversationMessages()
	if err != nil {
		return err
	}

	return o.updateUnreadUserCounters()
}

func (o *Import) insertConversations() error {
	var (
		privateSubscriptionRows       *sql.Rows
		discussionID                  int
		participantUserID             int
		ownerUserID                   int
		deleted                       bool
		lastReadDate                  int
		lastConversationMessageDate   int
		lastConversationMessageID     int
		lastConversationMessageUserID int
		messageCount                  int
	)

	privateSubscriptionRows, err = react.Txn.Query(`
        SELECT F_PrivateSubscriptions.DiscussionID,
               F_PrivateSubscriptions.UserID AS ParticipantUserID,
               F_Privatemessages.UserID AS OwnerUserID,
               IF(F_PrivateSubscriptions.FolderID = 5, 1, 0) AS Deleted,
               COALESCE(lrm.Date, 0) AS LastReadDateNormalized,
               F_PrivateDiscussions.Lastmessage AS LastConversationMessageDate,  
               F_PrivateDiscussions.LastMessageID AS LastConversationMessageID,
               COALESCE(lcm.UserID, 0) AS LastConversationMessageUserID,
               F_PrivateDiscussions.Messagecount
        FROM F_PrivateSubscriptions
        LEFT JOIN F_PrivateDiscussions ON F_PrivateDiscussions.DiscussionID = F_PrivateSubscriptions.DiscussionID
        LEFT JOIN F_Privatemessages ON F_Privatemessages.MessageID = F_PrivateDiscussions.FirstMessageID
        LEFT JOIN F_Privatemessages lrm ON lrm.MessageID = F_PrivateSubscriptions.LastReadMessageID
        LEFT JOIN F_Privatemessages lcm ON lcm.MessageID = F_PrivateDiscussions.LastMessageID
        WHERE F_Privatemessages.UserID IS NOT NULL
        ORDER BY F_PrivateSubscriptions.DiscussionID, ParticipantUserID
    `)
	if err != nil {
		return AddErrorInfo(err, "select from F_PrivateSubscriptions")
	}

	conversationIDToRecipientIDsMap := map[int][]int{} // only recipients without owner
	validConversationIDMap := map[int]int{}

	fmt.Print("inserting/updating conversation participants.")
	var countThousand int
	var countTen int
	for privateSubscriptionRows.Next() {
		countThousand++
		if countThousand == 1000 {
			countTen++
			if countTen == 10 {
				fmt.Print("\ninserting/updating conversation participants")
				countTen = 0
			}

			fmt.Print(".")
			countThousand = 1
		}

		err = privateSubscriptionRows.Scan(&discussionID, &participantUserID, &ownerUserID, &deleted, &lastReadDate, &lastConversationMessageDate, &lastConversationMessageID,
			&lastConversationMessageUserID, &messageCount)
		if err != nil {
			return AddErrorInfo(err, "scan F_PrivateSubscriptions")
		}

		isUnread := false
		if lastReadDate < lastConversationMessageDate {
			isUnread = true
		}

		recipientState := "active"
		if deleted {
			if !isUnread { // The last conversation message has been viewed by the participant.
				recipientState = "deleted"
			} else { // Conversation was kept in the trash while newer messages had been not viewed by the participant.
				recipientState = "deleted_ignored"
			}
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_conversation_recipient
            SET conversation_id = ?,
                user_id = ?,
                recipient_state = ?,
                last_read_date = ?
            ON DUPLICATE KEY
            UPDATE recipient_state = ?,
                   last_read_date = ?
        `, discussionID, participantUserID, recipientState, lastReadDate,
			recipientState, lastReadDate,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_conversation_recipient")
		}

		_, ok := conversationIDToRecipientIDsMap[discussionID]
		if !ok {
			conversationIDToRecipientIDsMap[discussionID] = []int{}
		}
		if participantUserID != ownerUserID {
			conversationIDToRecipientIDsMap[discussionID] = append(conversationIDToRecipientIDsMap[discussionID], participantUserID)
		}

		validConversationIDMap[discussionID]++

		if recipientState == "deleted_ignored" {
			continue
		}

		username, ok := userIDToUsernameMap[lastConversationMessageUserID]
		if !ok {
			username = "N/A"
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_conversation_user
            SET conversation_id = ?,
                owner_user_id = ?,
                is_unread = ?,
                reply_count = ?,
                last_message_date = ?,
                last_message_id = ?,
                last_message_user_id = ?,
                last_message_username = ?
            ON DUPLICATE KEY
            UPDATE is_unread = ?,
                   reply_count = ?,
                   last_message_date = ?,
                   last_message_id = ?,
                   last_message_user_id = ?,
                   last_message_username = ?
        `,
			discussionID, participantUserID, isUnread, messageCount-1, lastConversationMessageDate, lastConversationMessageID, lastConversationMessageUserID, username,
			isUnread, messageCount-1, lastConversationMessageDate, lastConversationMessageID, lastConversationMessageUserID, username,
		)
		if err != nil {
			return err
		}
	}
	fmt.Print("\n")

	var (
		privateDiscussionRows *sql.Rows
		conversationID        int
		title                 string
		userID                int
		startDate             int
		firstMessageID        int
		lastMessageDate       int
		lastMessageID         int
		lastMessageUserID     int
	)

	privateDiscussionRows, err = react.Txn.Query(`
        SELECT F_PrivateDiscussions.DiscussionID,
               fm.Title,
               fm.UserID,
               fm.Date,
               F_PrivateDiscussions.Messagecount,
               F_PrivateDiscussions.FirstMessageID,
               COALESCE(lm.Date, 0) AS Date,
               F_PrivateDiscussions.LastMessageID,
               F_PrivateDiscussions.LastposterID
        FROM F_PrivateDiscussions
        LEFT JOIN F_Privatemessages fm ON fm.MessageID = F_PrivateDiscussions.FirstMessageID
        LEFT JOIN F_Privatemessages lm ON lm.MessageID = F_PrivateDiscussions.LastMessageID
        WHERE fm.UserID IS NOT NULL
    `)
	if err != nil {
		return AddErrorInfo(err, "select from F_PrivateDiscussions")
	}

	fmt.Print("inserting conversations.")
	countThousand = 0
	countTen = 0
	for privateDiscussionRows.Next() {
		countThousand++
		if countThousand == 1000 {
			countTen++
			if countTen == 10 {
				fmt.Print("\ninserting conversations")
				countTen = 0
			}

			fmt.Print(".")
			countThousand = 1
		}

		err = privateDiscussionRows.Scan(&conversationID, &title, &userID, &startDate, &messageCount, &firstMessageID, &lastMessageDate, &lastMessageID, &lastMessageUserID)
		if err != nil {
			return AddErrorInfo(err, "scan F_PrivateDiscussions")
		}

		_, ok := validConversationIDMap[conversationID]
		if !ok {
			continue
		}

		title = html.UnescapeString(title)

		recipients := fmt.Sprintf("a:%d:{", len(conversationIDToRecipientIDsMap[conversationID]))
		for _, r := range conversationIDToRecipientIDsMap[conversationID] {
			username, ok := userIDToUsernameMap[r]
			if !ok {
				username = "[N/A]"
			}

			recipients += fmt.Sprintf(`i:%d;a:2:{s:7:"user_id";i:%d;s:8:"username";s:%d:"%s";}`,
				r, r, len([]byte(username)), username)
		}
		recipients += "}"

		ownerUsername, ok := userIDToUsernameMap[userID]
		if !ok {
			ownerUsername = "[N/A]"
		}

		lastMessageUsername, ok := userIDToUsernameMap[lastMessageUserID]
		if !ok {
			lastMessageUsername = "[N/A]"
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_conversation_master
            SET conversation_id = ?,
                title = ?,
                user_id = ?,
                username = ?,
                start_date = ?,
                open_invite = 0,
                conversation_open = 1,
                reply_count = ?,
                recipient_count = ?, -- participant count, including the conversation creator.
                first_message_id = ?,
                last_message_date = ?,
                last_message_id = ?,
                last_message_user_id = ?,
                last_message_username = ?,
                recipients = ?
            ON DUPLICATE KEY
            UPDATE title = ?,
                   user_id = ?,
                   username = ?,
                   start_date = ?,
                   reply_count = ?,
                   recipient_count = ?,
                   first_message_id = ?,
                   last_message_date = ?,
                   last_message_id = ?,
                   last_message_user_id = ?,
                   last_message_username = ?,
                   recipients = ?
        `,
			conversationID, title, userID, ownerUsername, startDate, messageCount-1, validConversationIDMap[conversationID], firstMessageID, lastMessageDate, lastMessageID,
			lastMessageUserID, lastMessageUsername, recipients,
			title, userID, ownerUsername, startDate, messageCount-1, validConversationIDMap[conversationID], firstMessageID, lastMessageDate, lastMessageID,
			lastMessageUserID, lastMessageUsername, recipients,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_conversation_master")
		}
	}
	fmt.Print("\n")

	return err
}

// Only cache the messages, don't do the same for conversation masters.
func (o *Import) createConversationMessageCache() error {
	_, err = xf2.Txn.Exec(`
        CREATE TABLE IF NOT EXISTS xf_datio_mfxf2_conversation_message_cache (
            conversation_message_id INT NOT NULL,
            conversation_message_hash VARCHAR(16) NOT NULL DEFAULT '',
            PRIMARY KEY (conversation_message_id)
        )
    `)

	return err
}

func (o *Import) insertConversationMessages() error {
	conversationMessageIDToHashCacheMap := map[int]string{}
	err = o.prepareConversationMessageCacheMap(conversationMessageIDToHashCacheMap)
	if err != nil {
		return err
	}

	var (
		privateMessageRows *sql.Rows
		messageID          int
		userID             int
		discussionID       int
		date               int
		title              string
		message            string
	)

	privateMessageRows, err = react.Txn.Query(`
        SELECT F_Privatemessages.MessageID,
               F_Privatemessages.UserID,
               F_Privatemessages.DiscussionID,
               F_Privatemessages.Date,
               F_Privatemessages.Title,
               F_Privatemessagesraw.Content
        FROM F_Privatemessages
        LEFT JOIN F_Privatemessagesraw ON F_Privatemessagesraw.MessageID = F_Privatemessages.messageID
    `)
	if err != nil {
		return err
	}

	err = o.prepareMediaReplacementsMap()
	if err != nil {
		return err
	}

	fmt.Print("inserting conversation messages.")
	var countThousand int
	var countTen int
	emptyQuoteSet := &[]*quote{}
	for privateMessageRows.Next() {
		countThousand++
		if countThousand == 1000 {
			countTen++
			if countTen == 10 {
				fmt.Print("\ninserting conversation messages")
				countTen = 0
			}

			fmt.Print(".")
			countThousand = 1
		}
		err = privateMessageRows.Scan(&messageID, &userID, &discussionID, &date, &title, &message)
		if err != nil {
			return err
		}

		// Check cache here.
		currentRecordHash := fmt.Sprintf("%v|%v|%v|%v|%v|%v", messageID, userID, discussionID, date, title, message)

		x := xxhash.New()
		_, err = x.Write([]byte(currentRecordHash))
		if err != nil {
			return err
		}
		currentRecordHash = fmt.Sprintf("%x", x.Sum64())

		hash, ok := conversationMessageIDToHashCacheMap[messageID]
		if ok && hash == currentRecordHash {
			continue
		}

		if ok {
			fmt.Printf("data of personal conversation id '%d' has been changed since the previous import\n", messageID)
		}

		title = html.UnescapeString(title)

		message = o.normalizeSlashOnlyClosingTag(message)
		message = o.convertListRelatedBBCodes(message)
		message = o.convertSizeRelatedBBCodes(message)
		message = imgBBSmileyRe.ReplaceAllString(message, ":$1:")
		message = noHTMLBBCodeRe.ReplaceAllString(message, "")

		message, _, err = o.normalizeQuotes(message, emptyQuoteSet)
		if err != nil {
			return err
		}

		message = o.convertMediaRelatedBBCodes(message)

		username, ok := userIDToUsernameMap[userID]
		if !ok {
			username = "[N/A]"
		}

		_, err = xf2.Txn.Exec(`
                INSERT INTO xf_conversation_message
                SET message_id = ?,
                    conversation_id = ?,
                    message_date = ?,
                    user_id = ?,
                    username = ?,
                    message = ?,
                    embed_metadata = '[]',
                    like_users = 'a:0:{}'
                ON DUPLICATE KEY
                UPDATE conversation_id = ?,
                       message_date = ?,
                       user_id = ?,
                       username = ?,
                       message = ?
            `,
			messageID, discussionID, date, userID, username, message,
			discussionID, date, userID, username, message,
		)
		if err != nil {
			return err
		}

		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_datio_mfxf2_conversation_message_cache
            SET conversation_message_id = ?,
                conversation_message_hash = ?
            ON DUPLICATE KEY
            UPDATE conversation_message_hash = ?
        `,
			messageID, currentRecordHash,
			currentRecordHash,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_datio_mfxf2_thread_cache")
		}
	}

	return err
}

func (o *Import) prepareConversationMessageCacheMap(pcmcm map[int]string) error {
	var (
		conversationMessageCacheRows *sql.Rows
		conversationMessageID        int
		conversationMessageHash      string
	)

	conversationMessageCacheRows, err = xf2.Txn.Query(`
        SELECT conversation_message_id,
               conversation_message_hash
        FROM xf_datio_mfxf2_conversation_message_cache
        ORDER BY conversation_message_id
    `)
	if err != nil {
		return err
	}

	for conversationMessageCacheRows.Next() {
		err = conversationMessageCacheRows.Scan(&conversationMessageID, &conversationMessageHash)
		if err != nil {
			return err
		}

		pcmcm[conversationMessageID] = conversationMessageHash
	}

	return err
}

func (o *Import) updateUnreadUserCounters() error {
	_, err = xf2.Txn.Exec(`
        UPDATE xf_user
        SET conversations_unread = (
            SELECT COUNT(*) FROM xf_conversation_user
        WHERE owner_user_id = xf_user.user_id
          AND is_unread = 1
        )
    `)

	return err
}

var conversationParticipantsMap = map[int][]int{}
var conversationParticipantsMapPreparedGuard bool

func (o *Import) prepareConversationParticipantsMap() error {
	if conversationParticipantsMapPreparedGuard {
		return nil
	}

	var (
		privateDiscussionParticipantRows *sql.Rows
		discussionID                     int
		userID                           int
	)

	privateDiscussionParticipantRows, err = react.Txn.Query(`
        SELECT DiscussionID, UserID
        FROM F_PrivateSubscriptions
        ORDER BY DiscussionID DESC, UserID
    `)
	if err != nil {
		return AddErrorInfo(err, "select from F_PrivateSubscriptions")
	}

	for privateDiscussionParticipantRows.Next() {
		err = privateDiscussionParticipantRows.Scan(&discussionID, &userID)
		if err != nil {
			return AddErrorInfo(err, "scan F_PrivateSubscriptions")
		}

		conversationParticipantsMap[discussionID] = append(conversationParticipantsMap[discussionID], userID)
	}

	conversationParticipantsMapPreparedGuard = true

	return nil
}
