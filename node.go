// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

import (
	"database/sql"
	"errors"
	"fmt"
	"html"
)

type node struct {
	OldID int // To log category mappings for the import log.

	NodeID                int
	Title                 string
	Description           string
	NodeName              string
	NodeTypeID            string
	ParentNodeID          int
	DisplayOrder          int
	DisplayInList         bool
	Lft                   int
	Rgt                   int
	Depth                 int
	StyleID               int
	EffectiveStyleID      int
	BreadcrumbData        string
	NavigationID          int
	EffectiveNavigationID int
}

type forum struct {
	LastPostUsername          string
	LastThreadTitle           string
	FieldCache                string
	PrefixCache               string
	PromptCache               string
	DefaultSortOrder          string
	DefaultSortDirection      string
	AllowedWatchNotifications string
	NodeID                    int
	DiscussionCount           int
	MessageCount              int
	LastPostID                int
	LastPostDate              int
	LastPostUserID            int
	LastThreadPrefixID        int
	DefaultPrefixID           int
	ListDateLimitDates        int
	MinTags                   int
	ModerateThreads           bool
	ModerateReplies           bool
	AllowPosting              bool
	AllowPoll                 bool
	CountMessages             bool
	FindNew                   bool
	RequirePrefix             bool
}

var reactCategoryToXf2Map = map[int]int{}

// TODO: refactor.
func (o *Import) importNodes() error {

	doImport, ok := o.Config.Get("mfxf2.import_categories_and_forums").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_categories_and_forums' in config")
	}

	if !doImport {
		// Get mappings from cache for use in other procedures.
		var (
			importLogRows *sql.Rows
			oldID         int
			newID         int
		)

		importLogRows, err = xf2.Txn.Query(`
            SELECT old_id,
                   new_id
            FROM xf_import_log
            WHERE content_type = 'category'
        `)
		if err != nil {
			return err
		}

		for importLogRows.Next() {
			err = importLogRows.Scan(&oldID, &newID)
			if err != nil {
				fmt.Println("err")
				return err
			}

			reactCategoryToXf2Map[oldID] = newID
		}

		return nil
	}

	nodesToInsert := []*node{}
	forumsToInsert := []*forum{}

	var (
		nodeRows          *sql.Rows
		name              string
		forumID           int
		categoryID        int
		nodeType          string
		ordernr           int
		days              int
		description       string
		orderTopicsDateBy string
		messageCount      int
		topicCount        int
		lastMessage       int
		lastModified      int
		admins            string
		data              string
	)

	nodeRows, err = react.Txn.Query(`
        SELECT ForumID FROM F_Forums ORDER BY ForumID
    `)
	if err != nil {
		return err
	}

	// Look for available IDs to assign to categories.
	freeIDs := []int{}
	previous := 0
	current := 0
	for nodeRows.Next() {
		err = nodeRows.Scan(&current)
		if err != nil {
			return err
		}

		for i := previous + 1; i < current; i++ {
			freeIDs = append(freeIDs, i)

		}
		previous = current
	}

	nodeRows, err = react.Txn.Query(`
        SELECT CategoryID, Name, Ordernr, Type FROM F_Categories ORDER BY Ordernr
    `)
	if err != nil {
		return err
	}

	for nodeRows.Next() {
		err = nodeRows.Scan(&categoryID, &name, &ordernr, &nodeType)
		if err != nil {
			return err
		}

		if len(freeIDs) > 0 {
			reactCategoryToXf2Map[categoryID] = freeIDs[0]
			freeIDs = append(freeIDs[:0], freeIDs[1:]...)
		} else {
			previous = previous + 1
			reactCategoryToXf2Map[categoryID] = previous
		}

		nodesToInsert = append(nodesToInsert, &node{
			OldID: categoryID,

			NodeID:         reactCategoryToXf2Map[categoryID],
			Title:          name,
			NodeTypeID:     "Category",
			DisplayOrder:   ordernr,
			DisplayInList:  true,
			Lft:            0,
			Rgt:            0,
			Depth:          0,
			BreadcrumbData: "",
		})
	}

	nodeRows, err = react.Txn.Query(`
        SELECT Name, ForumID, CategoryID, Type, Ordernr, Days, Description, OrdertopicsdateBy, MessageCount, TopicCount, LastMessage,
               LastModified, Admins, Data # Redirect
        FROM F_Forums
        WHERE deleted <> 1
        ORDER BY Ordernr
    `)
	if err != nil {
		return err
	}

	ordernrNew := 0
	for nodeRows.Next() {
		err = nodeRows.Scan(
			&name, &forumID, &categoryID, &nodeType, &ordernr, &days, &description, &orderTopicsDateBy, &messageCount, &topicCount, &lastMessage,
			&lastModified, &admins, &data,
		)
		if err != nil {
			return err
		}

		ordernrNew++

		nodesToInsert = append(nodesToInsert, &node{
			NodeID:         forumID,
			Title:          html.UnescapeString(name),
			Description:    html.UnescapeString(description),
			NodeTypeID:     "Forum",
			ParentNodeID:   reactCategoryToXf2Map[categoryID],
			DisplayOrder:   ordernrNew,
			DisplayInList:  true,
			Lft:            0, // TODO
			Rgt:            0, // TODO
			Depth:          1,
			BreadcrumbData: "", // TODO
		})

		if days > 365 {
			days = 365
		} else if days > 182 {
			days = 182
		} else if days > 90 {
			days = 90
		} else if days > 60 {
			days = 60
		} else if days > 30 {
			days = 30
		} else if days > 14 {
			days = 14
		} else if days > 0 {
			days = 7
		} else {
			days = 0
		}

		allowPosting := true
		if nodeType == "archive" {
			allowPosting = false
		}

		defaultSortOrder := "last_post_date"
		if orderTopicsDateBy == "topicstart_desc" {
			defaultSortOrder = "post_date"
		}

		forumsToInsert = append(forumsToInsert, &forum{
			NodeID:                    forumID,
			DiscussionCount:           topicCount,
			MessageCount:              messageCount,
			LastPostID:                0, // On finalize
			LastPostDate:              lastMessage,
			LastPostUserID:            0,  // On finilize
			LastPostUsername:          "", // On finilize
			LastThreadTitle:           "", // On finilize
			LastThreadPrefixID:        0,  // On finilize
			AllowPosting:              allowPosting,
			CountMessages:             true,
			FindNew:                   true,
			FieldCache:                "", // On finilize
			PrefixCache:               "", // On finilize
			PromptCache:               "", // On finilize
			DefaultSortOrder:          defaultSortOrder,
			DefaultSortDirection:      "desc",
			ListDateLimitDates:        days,
			AllowedWatchNotifications: "all",
		})
	}

	// Insert.
	for _, n := range nodesToInsert {
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_node
            SET node_id = ?, title = ?, description = ?, node_type_id = ?, parent_node_id = ?, display_order = ?, display_in_list = ?,
                lft = ?, rgt = ?, depth = ?, breadcrumb_data = ?
            ON DUPLICATE KEY
            UPDATE title = ?, description = ?, node_type_id = ?, parent_node_id = ?, display_order = ?, display_in_list = ?, lft = ?, rgt = ?,
                   depth = ?, breadcrumb_data = ?
        `,
			n.NodeID, n.Title, n.Description, n.NodeTypeID, n.ParentNodeID, n.DisplayOrder, n.DisplayInList, n.Lft, n.Rgt, n.Depth, n.BreadcrumbData,
			n.Title, n.Description, n.NodeTypeID, n.ParentNodeID, n.DisplayOrder, n.DisplayInList, n.Lft, n.Rgt, n.Depth, n.BreadcrumbData,
		)
		if err != nil {
			return err
		}

		if n.NodeTypeID == "Category" {
			_, err = xf2.Txn.Exec(`
                INSERT IGNORE INTO xf_category
                SET node_id = ?
            `, n.NodeID)
			if err != nil {
				return err
			}

			// Log category IDs as it is required that they change when imported (so they do not conflict with forum IDs).
			_, err = xf2.Txn.Exec(`
                INSERT INTO xf_import_log
                SET content_type = 'category',
                    old_id = ?,
                    new_id = ?
                ON DUPLICATE KEY
                UPDATE new_id = ?
            `, n.OldID, n.NodeID, n.NodeID)
			if err != nil {
				return err
			}
		}
	}

	for _, f := range forumsToInsert {
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_forum
            SET node_id = ?, discussion_count = ?, message_count = ?, last_post_id = ?, last_post_date = ?, last_post_user_id = ?, last_post_username = ?,
                last_thread_title = ?, last_thread_prefix_id = ?, moderate_threads = ?, moderate_replies = ?, allow_posting = ?, allow_poll = ?,
                count_messages = ?, find_new = ?, field_cache = ?, prefix_cache = ?, prompt_cache = ?, default_prefix_id = ?, default_sort_order = ?,
                default_sort_direction = ?, list_date_limit_days = ?, require_prefix = ?, allowed_watch_notifications = ?, min_tags = ?
            ON DUPLICATE KEY
            UPDATE discussion_count = ?, message_count = ?, last_post_id = ?, last_post_date = ?, last_post_user_id = ?, last_post_username = ?,
            last_thread_title = ?, last_thread_prefix_id = ?, moderate_threads = ?, moderate_replies = ?, allow_posting = ?, allow_poll = ?,
            count_messages = ?, find_new = ?, field_cache = ?, prefix_cache = ?, prompt_cache = ?, default_prefix_id = ?, default_sort_order = ?,
            default_sort_direction = ?, list_date_limit_days = ?, require_prefix = ?, allowed_watch_notifications = ?, min_tags = ?
        `,
			f.NodeID, f.DiscussionCount, f.MessageCount, f.LastPostID, f.LastPostDate, f.LastPostUserID, f.LastPostUsername,
			f.LastThreadTitle, f.LastThreadPrefixID, f.ModerateThreads, f.ModerateReplies, f.AllowPosting, f.AllowPoll,
			f.CountMessages, f.FindNew, f.FieldCache, f.PrefixCache, f.PromptCache, f.DefaultPrefixID, f.DefaultSortOrder,
			f.DefaultSortDirection, f.ListDateLimitDates, f.RequirePrefix, f.AllowedWatchNotifications, f.MinTags,

			f.DiscussionCount, f.MessageCount, f.LastPostID, f.LastPostDate, f.LastPostUserID, f.LastPostUsername,
			f.LastThreadTitle, f.LastThreadPrefixID, f.ModerateThreads, f.ModerateReplies, f.AllowPosting, f.AllowPoll,
			f.CountMessages, f.FindNew, f.FieldCache, f.PrefixCache, f.PromptCache, f.DefaultPrefixID, f.DefaultSortOrder,
			f.DefaultSortDirection, f.ListDateLimitDates, f.RequirePrefix, f.AllowedWatchNotifications, f.MinTags,
		)
		if err != nil {
			return err
		}
	}

	return err
}
