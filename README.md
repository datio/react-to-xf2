<h3 align="center">React to XenForo 2</h3>

###### Install [goenv](https://github.com/syndbg/goenv#installation) and golang.

###### Create the project directory:
    export GOPATH="$HOME/mfxf2"

###### Install the CLI program:
    go get gitlab.com/datio/react-to-xf2/etc/mfxf2

###### Use the CLI program:
    mfxf2/bin/mfxf2 --config="mfxf2/src/gitlab.com/datio/react-to-xf2/etc/config.toml"