// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

import (
	"database/sql"
	"errors"
	"strings"
)

type reactRuleXf2Permission struct {
	GroupID  int
	NodeType string
	NodeID   int
	Action   string
	Deny     string
	Allow    string
}

type xfPermissionEntryContent struct {
	ContentType        string
	ContentID          int
	UserGroupID        int
	UserID             int
	PermissionGroupID  string
	PermissionID       string
	PermissionValue    string
	PermissionValueInt int
}

type xfPermissionEntry struct {
	UserGroupID        int
	UserID             int
	PermissionGroupID  string
	PermissionID       string
	PermissionValue    string
	PermissionValueInt int
}

func (o *Import) importPermissions() error {
	doImport, ok := o.Config.Get("mfxf2.import_permissions").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_permissions' in config")
	}

	if !doImport {
		return nil
	}

	rules := []*reactRuleXf2Permission{}

	err = o.getPermissions(&rules)
	if err != nil {
		return err
	}

	contentEntryPermissions := []*xfPermissionEntryContent{}
	entryPermissions := []*xfPermissionEntry{}

	err = o.preparePermissions(&rules, &contentEntryPermissions, &entryPermissions)
	if err != nil {
		return err
	}

	err = o.insertContentEntryPermissions(&contentEntryPermissions)
	if err != nil {
		return err
	}

	err = o.insertEntryPermissions(&entryPermissions)

	return err
}

func (o *Import) getPermissions(r *[]*reactRuleXf2Permission) error {
	var (
		ruleRows  *sql.Rows
		targetID  int
		level     string
		levelID   int
		dataLevel string
		deny      string
		allow     string
	)

	ruleRows, err = react.Txn.Query(`
        SELECT TargetID,
               Level,
               LevelID,
               DataLevel,
               Deny,
               Allow
        FROM F_Rules
        WHERE Target = 'group'
          AND Level <> ''
          AND (Deny <> '' OR Allow <> '')
        ORDER BY RuleID ASC # Later rules may overwrite any previous ones.
    `)
	if err != nil {
		return err
	}

	// Content
	for ruleRows.Next() {
		err = ruleRows.Scan(&targetID, &level, &levelID, &dataLevel, &deny, &allow)
		if err != nil {
			return err
		}

		*r = append(*r, &reactRuleXf2Permission{
			GroupID:  targetID,
			NodeType: level,
			NodeID:   levelID,
			Action:   dataLevel,
			Deny:     deny,
			Allow:    allow,
		})
	}

	return err
}

func (o *Import) preparePermissions(rules *[]*reactRuleXf2Permission, cep *[]*xfPermissionEntryContent, ep *[]*xfPermissionEntry) error {
	for _, r := range *rules {
		if r.NodeType == "category" {
			r.NodeID = reactCategoryToXf2Map[r.NodeID]
		}

		switch r.NodeType {
		case "forum", "category":
			// Allow
			if strings.Contains(r.Allow, "view") {
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node", ContentID: r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "general", PermissionID: "viewNode",
					PermissionValue: "content_allow",
				})
			}
			if strings.Contains(r.Allow, "make") {
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node",
					ContentID:   r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "postThread",
					PermissionValue: "content_allow",
				})
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node", ContentID: r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "postReply",
					PermissionValue: "content_allow",
				})
			}
			if strings.Contains(r.Allow, "edit") {
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node", ContentID: r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "editOwnPost",
					PermissionValue: "content_allow",
				})
			}
			if strings.Contains(r.Allow, "del") {
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node", ContentID: r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "deleteOwnThread",
					PermissionValue: "content_allow",
				})
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node", ContentID: r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "deleteOwnPost",
					PermissionValue: "content_allow",
				})
			}
			// Deny
			if strings.Contains(r.Deny, "view") {
				// When administrators are prohibited to view a node, it should become private.
				if r.GroupID == 3 {
					r.GroupID = 0
				}

				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node", ContentID: r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "general", PermissionID: "viewNode",
					PermissionValue: "reset",
				})
			}
			if strings.Contains(r.Deny, "make") {
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node",
					ContentID:   r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "postThread",
					PermissionValue: "reset",
				})
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node", ContentID: r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "postReply",
					PermissionValue: "reset",
				})
			}
			if strings.Contains(r.Deny, "edit") {
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node", ContentID: r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "editOwnPost",
					PermissionValue: "reset",
				})
			}
			if strings.Contains(r.Deny, "del") {
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node", ContentID: r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "deleteOwnThread",
					PermissionValue: "reset",
				})
				*cep = append(*cep, &xfPermissionEntryContent{
					ContentType: "node", ContentID: r.NodeID, UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "deleteOwnPost",
					PermissionValue: "reset",
				})
			}
		case "board":
			// Allow
			if strings.Contains(r.Allow, "view") {
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "general", PermissionID: "viewNode",
					PermissionValue: "allow",
				})
			}
			if strings.Contains(r.Allow, "make") {
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "postThread",
					PermissionValue: "allow",
				})
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "postReply",
					PermissionValue: "allow",
				})
			}
			if strings.Contains(r.Allow, "edit") {
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "editOwnPost",
					PermissionValue: "allow",
				})
			}
			if strings.Contains(r.Allow, "del") {
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "deleteOwnThread",
					PermissionValue: "allow",
				})
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "deleteOwnPost",
					PermissionValue: "allow",
				})
			}
			// Deny
			if strings.Contains(r.Deny, "view") {
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "general", PermissionID: "viewNode",
					PermissionValue: "unset",
				})
			}
			if strings.Contains(r.Deny, "make") {
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "postThread",
					PermissionValue: "unset",
				})
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "postReply",
					PermissionValue: "unset",
				})
			}
			if strings.Contains(r.Deny, "edit") {
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "editOwnPost",
					PermissionValue: "unset",
				})
			}
			if strings.Contains(r.Deny, "del") {
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "deleteOwnThread",
					PermissionValue: "unset",
				})
				*ep = append(*ep, &xfPermissionEntry{
					UserGroupID: r.GroupID, PermissionGroupID: "forum", PermissionID: "deleteOwnPost",
					PermissionValue: "unset",
				})
			}
		case "none":
			// TODO: Disallow private messages from the respective group.
		}
	}

	return err
}

func (o *Import) insertContentEntryPermissions(permissions *[]*xfPermissionEntryContent) error {
	for _, p := range *permissions {
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_permission_entry_content
            SET content_type = 'node',
                content_id = ?,
                user_group_id = ?,
                user_id = ?,
                permission_group_id = ?,
                permission_id = ?,
                permission_value = ?,
                permission_value_int = ?
            ON DUPLICATE KEY
            UPDATE permission_value = ?
        `,
			p.ContentID, p.UserGroupID, p.UserID, p.PermissionGroupID, p.PermissionID, p.PermissionValue, p.PermissionValueInt,
			p.PermissionValue,
		)
		if err != nil {
			return err
		}

	}

	return err
}

func (o *Import) insertEntryPermissions(permissions *[]*xfPermissionEntry) error {
	for _, p := range *permissions {
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_permission_entry
            SET user_group_id = ?,
                user_id = ?,
                permission_group_id = ?,
                permission_id = ?,
                permission_value = ?,
                permission_value_int = ?
            ON DUPLICATE KEY
            UPDATE permission_value = ?
        `,
			p.UserGroupID, p.UserID, p.PermissionGroupID, p.PermissionID, p.PermissionValue, p.PermissionValueInt,
			p.PermissionValue,
		)
		if err != nil {
			return err
		}

	}

	return err
}
