// Copyright 2017 Motormedia. All rights reserved.

package main

import (
	"flag"
	"fmt"
	"os"

	toml "github.com/pelletier/go-toml"
	mfxf2 "gitlab.com/datio/react-to-xf2"
)

type cli struct {
	ConfigFile string
}

var (
	importOptions *mfxf2.Import
	cliOptions    *cli
	config        *toml.Tree
	err           error
)

func init() {
	importOptions = &mfxf2.Import{}
	cliOptions = &cli{}

	flag.StringVar(&importOptions.Action, "action", "", "execute desired operation")

	flag.StringVar(&cliOptions.ConfigFile, "config", "", "configuration file")

	// Parse the above CLI arguments.
	flag.Parse()

	// Initialize the configuration.
	config, err = toml.LoadFile(cliOptions.ConfigFile)
	if err != nil {
		fmt.Println(mfxf2.AddErrorInfo(err, "configuration error"))
		os.Exit(1)
	}
}

func main() {
	err = validateInput(cliOptions, importOptions)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	importOptions.Config = config
	err = importOptions.Begin()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func validateInput(cliOptions *cli, importOptions *mfxf2.Import) error {
	if importOptions.Action == "" {
		importOptions.Action = "import"
	}

	return nil
}
