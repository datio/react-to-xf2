<?php

namespace Datio\React;

class Listener
{
	public static function modifyFormEmail(\XF\Mail\Templater $templater, $type, $template, &$output)
	{
		$ok = preg_match('#<blockquote><div class="bbWrapper">(.*?)<\/div><\/blockquote>#s', $output, $m);

		if (!$ok) {
			return;
		}

		$usernameUnset = false; // Flag for removing the first field when it's related to the username.
		$lines = preg_split("/[\r\n]+/", $m[1]);
		foreach ($lines as $k => $line) {
			if (!$usernameUnset && $k == 0 && strpos($line, ':') !== false) {
				$usernameUnset = true;
				unset($lines[$k]);
				continue;
			}

			if ($line == '<br />') {
				unset($lines[$k]);
				continue;
			}

			$lines[$k] = str_replace('<br />', '', $lines[$k]);
		}

		$values = [];
		foreach ($lines as $line) {
			$ok = preg_match('#<b>.*?</b>\s?(.*)#', $line, $sm);
			if (!$ok) {
				continue;
			}

			array_push($values, $sm[1]);
		}

		$output = str_replace($m[1], implode(',', $values), $output);

		$w2 = 0;
	}
}