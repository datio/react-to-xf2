<?php

namespace Datio\React\Pub\Controller;

use XF\Pub\Controller\AbstractController;
use XF\Mvc\ParameterBag;

class Forum extends AbstractController
{
	public function actionIndex(ParameterBag $params)
	{
		$options = \XF::options();
		// Redirect any non-mappable actions to the specified URL that is set in options.
		$rewriteRedirectUrl = rtrim($options->rewriteRedirectUrl, '/') . '/';
		if (strlen($rewriteRedirectUrl) < 2) {
			$rewriteRedirectUrl = $this->buildLink('index');
		}

		$pathParts = explode('/', $params->get("path"));

		switch ($pathParts[0]) {
			case '' :
			case 'list_categories' :
				return $this->redirectPermanently(
					$this->buildLink('index')
				);
			// Bookmarks.
			case 'bookmarks_rss':
			case 'list_bookmarks':
			case 'replace_bookmark':
			case 'insert_bookmark':
				return $this->redirectPermanently(
					$this->buildLink('bookmarks')
				);
			case 'myreact':
				return $this->redirectMyReactToUserBookmarks($pathParts);
			case 'delete_bookmarks':
			case 'update_bookmark':
				return $this->redirectToAddEditBookmark($pathParts);
			// Media.
			case 'list_documents':
			case 'list_documents_small':
			case 'manage_folders':
				return $this->redirectToVisitorAlbumsList();
			case 'insert_document':
			case 'upload':
				return $this->redirect(
					$this->buildLink('media/add')
				);
			case 'download_document':
			case 'edit_document':
				return $this->redirectToMediaViewEdit($pathParts);
			// Users.
			case 'login':
				return $this->redirectPermanently(
					$this->buildLink('login')
				);
			case 'create_user':
				return $this->redirectPermanently(
					$this->buildLink('register')
				);
			case 'list_users':
			case 'update_publicusergroups':
				return $this->redirectPermanently(
					$this->buildLink('members')
				);
			case 'user_profile':
			case 'vcard':
			case 'list_usernotes': // User notes are not yet being been imported.
			case 'list_setusernotes':
			case 'delete_usernote':
				return $this->redirectToUserProfile($pathParts);
			case 'add_contact':
			case 'delete_contact':
				return $this->redirectToFollowUser($pathParts);
			case 'list_contacts':
				return $this->redirectUserFollowers();
			case 'preferences':
				return $this->redirectPermanently(
					$this->buildLink('account/preferences')
				);
			case 'edit_user':
				return $this->redirectPermanently(
					$this->buildLink('account/account-details')
				);
			case 'change_email':
				return $this->redirectPermanently(
					$this->buildLink('account/email')
				);
			case 'new_password':
				return $this->redirectPermanently(
					$this->buildLink('account/security')
				);
			case 'list_activeusers':
				return $this->redirectPermanently(
					$this->buildLink('whats-new/latest-activity')
				);
			case 'send_activationkey':
			case 'send_passwordkey':
			case 'activate_user':
			case 'activate_password':
				return $this->redirectToUserConfirmationResend($pathParts);
			// Notifications.
			case 'delete_notifications':
			case 'insert_notification':
			case 'list_notifications':
			case 'update_notification':
				return $this->redirectToWatch($pathParts);
			// Threads and posts.
			case 'list_category_topics':
			case 'list_category':
				return $this->redirectToCategory($pathParts);
			case 'list_topics':
				return $this->redirectToForum($pathParts);
			case 'list_activetopics':
				return $this->redirectPermanently(
					$this->buildLink('whats-new/posts')
				);
			case 'insert_topic':
				return $this->redirectToThreadCreate($pathParts);
			case 'list_messages':
			case 'insert_topicreport':
			case 'delete_topicreport':
				return $this->redirectToThread($pathParts);
			case 'list_topicreports':
				return $this->redirectToThreadReportList($pathParts);
			case 'frontend_admin':
				return $this->redirectToThreadAction($pathParts, '/edit');
			case 'insert_message':
				return $this->redirectToThreadAction($pathParts, '/reply');
			case 'view_message':
			case 'list_message':
				return $this->redirectToPost($pathParts);
			case 'update_message':
				return $this->redirectToPost($pathParts, '/edit');
			case 'delete_message':
				return $this->redirectToPost($pathParts, '/delete');
			case 'list_messagehistory':
				return $this->redirectToPost($pathParts, '/history');
			case 'quote_message':
				return $this->redirectToPostReply($pathParts);
			// Other.
			case 'faq':
				return $this->redirectPermanently(
					$this->buildLink('help')
				);
			case 'list_forum_stats':
				return $this->redirectPermanently(
					$this->buildLink('online')
				);
			case 'admin':
				return $this->redirectPermanently(
					$this->buildLink('admin.php')
				);
			case 'find':
				return $this->redirectToSearch($pathParts);
			// Conversations.
			case 'pm_new_message':
			case 'send_pushmessage':
			case 'send_privatemessage':
			case 'list_sentpushmessages':
				return $this->redirectToConversationCreate($pathParts);
			case 'list_discussions':
			case 'list_pushmessages':
				return $this->redirectPermanently(
					$this->buildLink('conversations')
				);
			case 'pm_discussion':
				return $this->redirectToConversation($pathParts);
			// Unmapped actions.
			case 'copy_profile':
			case 'custom':
			case 'delete_calendarevent':
			case 'delete_files':
			case 'impersonate':
			case 'import_users':
			case 'insert_calendarevent':
			case 'insert_profilemessage':
			case 'insert_shoutbox_message':
			case 'delete_profilemessage':
			case 'list_calendar':
			case 'logout': // Security related.
			case 'mailhandler':
			case 'mailing_subscribe':
			case 'mailing_unsubscribe':
			case 'notepad':
			case 'reset':
			case 'retrieve_from_attachedzip':
			case 'rml_preview': // Notepad related.
			case 'rss':
			case 'sherlock':
			case 'sidebar':
			case 'sms_gateway':
			case 'spell_check':
			case 'update_calendarevent':
			case 'update_shoutbox_message':
			case 'user_email_gfx': // Privacy related.
			case 'view_calendarevent':
			case 'view_documentdata':
			case 'delete_shoutbox_message':
			case 'list_shoutbox':
			case 'list_shoutbox_small':
			case 'hottopic_hfd':
			case 'hottopic_xml':
			case 'get_forum':
			case 'get_message':
			case 'get_messageversion':
			case 'http_status_code':
			case 'list_profilemessages':
			case 'list_publicusergroups':
			case 'error_general':
			case 'cast_vote':
			case 'pm_mark_subscription_unread':
			case 'pm_mark_subscription':
			default:
				return $this->app()->response()->redirect($rewriteRedirectUrl, 302)->send();
		}
	}

	protected function redirectMyReactToUserBookmarks(array $pathParts)
	{
		// Check if the user's ID is included. If it isn't, redirect the visitor to their own bookmarks page.
		if (count($pathParts) > 1) {
			$userIdInUrl = $pathParts[1];
			if (is_numeric($userIdInUrl)) {
				$user = $this->app->find('XF:User', (int)$userIdInUrl);

				return $this->redirectPermanently(
					$this->buildLink('members', $user) . '#bookmarks'
				);
			}
		}

		$visitor = \XF::visitor();
		if ( ! $visitor->user_id) {
			return $this->redirect(
				$this->buildLink($this->buildLink('bookmarks'))
			);
		}

		return $this->redirect(
			$this->buildLink('members', $visitor) . '#bookmarks'
		);
	}

	protected function redirectToAddEditBookmark(array $pathParts)
	{
		$visitor = \XF::visitor();

		$postIdInUrl = null;
		if (count($pathParts) > 1) {
			$postIdInUrl = $pathParts[1];
		}

		// Check if the post ID is included. If it isn't, redirect the visitor to their bookmarks.
		if ($visitor->user_id && is_numeric($postIdInUrl)) {
			/** @var \ThemeHouse\Bookmarks\Finder\Bookmark $bookmarkFinder */
			$bookmarkFinder = $this->app->finder('ThemeHouse\Bookmarks:Bookmark')
				->where('user_id', $visitor->user_id)
				->where('content_id', $postIdInUrl)
				->order('content_type');

			/** @var \ThemeHouse\Bookmarks\Entity\Bookmark $bookmark */
			$bookmark = $bookmarkFinder->fetchOne();

			if ($bookmark) {
				return $this->redirectPermanently(
					$this->buildLink('bookmarks/edit', $bookmark)
				);
			}
		}

		return $this->redirect(
			$this->buildLink('bookmarks')
		);
	}

	protected function redirectToVisitorAlbumsList()
	{
		$visitor = \XF::visitor();
		if ($visitor->user_id) {
			return $this->redirectPermanently(
				$this->buildLink('canonical:media/albums/users', $visitor)
			);
		}

		return $this->redirect(
			$this->buildLink('media/albums')
		);
	}

	protected function redirectToMediaViewEdit(array $pathParts)
	{
		$mediaIdInUrl = null;
		if (count($pathParts) > 1) {
			$mediaIdInUrl = $pathParts[1];
		}

		$fullMediaView = false;
		if (count($pathParts) > 2) {
			$fullMediaView = true;
		}

		if (is_numeric($mediaIdInUrl)) {
			// Duplicate media items get removed during the import.
			// The corresponding originals map is found in the xf_datio_mfxf2_media_cache table.
			/** @var \Datio\React\Finder\MediaCache $cachedMediaFinder */
			$cachedMediaFinder = $this->app->finder('Datio\React:MediaCache')
				->where('media_id', (int)$mediaIdInUrl);
			/** @var \Datio\React\Entity\MediaCache $cachedMedia */
			$cachedMedia = $cachedMediaFinder->fetchOne();

			if ( ! $cachedMedia) {
				return $this->redirectPermanently(
					$this->buildLink('canonical:media/index')
				);
			}

			if ($cachedMedia->success) {
				$media = $this->app->find('XFMG:MediaItem', $cachedMedia->media_id);

				if ($fullMediaView) {
					return $this->redirectPermanently(
						$this->buildLink('canonical:media/full', $media)
					);
				}

				return $this->redirectPermanently(
					$this->buildLink('canonical:media/edit', $media)
				);
			}

			if ($media = $this->getOriginalMedia($cachedMedia)) {
				if ($fullMediaView) {
					return $this->redirectPermanently(
						$this->buildLink('canonical:media/full', $media)
					);
				}

				return $this->redirectPermanently(
					$this->buildLink('canonical:media/edit', $media)
				);
			}

			return $this->redirectPermanently(
				$this->buildLink('canonical:media/index')
			);
		}
	}

	protected function getOriginalMedia(\Datio\React\Entity\MediaCache $cachedMedia)
	{
		$newMediaId           = null;
		$duplicateLogTemplate = 'failed: duplicate media (' . $cachedMedia->media_id . ' of ';
		$newMediaIdPosInLog   = strpos($cachedMedia->log, $duplicateLogTemplate);

		if ($newMediaIdPosInLog !== false) {
			$newMediaId = substr($cachedMedia->log, strlen($duplicateLogTemplate), -1);
			$media      = $this->app->find('XFMG:MediaItem', $newMediaId);

			return $media;
		}

		return null;
	}

	protected function redirectToUserProfile(array $pathParts)
	{
		// Check if the user's ID is included.
		// If it isn't, redirect the visitor to their own profile page.
		if (count($pathParts) > 1) {
			$userIdInUrl = $pathParts[1];
			if (is_numeric($userIdInUrl)) {
				$user = $this->app->find('XF:User', (int)$userIdInUrl);

				if ( ! $user) {
					return $this->redirect(
						$this->buildLink('members')
					);
				}

				return $this->redirectPermanently(
					$this->buildLink('members', $user) . '#about'
				);
			}
		}

		$visitor = \XF::visitor();
		if ( ! $visitor->user_id) {
			return $this->redirect(
				$this->buildLink($this->buildLink('members'))
			);
		}

		return $this->redirect(
			$this->buildLink('members', $visitor) . '#about'
		);
	}

	protected function redirectToFollowUser(array $pathParts)
	{
		// Check if the user's ID is included. If it isn't, redirect the visitor to their own followers page.
		if (count($pathParts) > 1) {
			$userIdInUrl = $pathParts[1];
			if (is_numeric($userIdInUrl)) {
				$user = $this->app->find('XF:User', (int)$userIdInUrl);

				if ( ! $user) {
					return $this->redirect(
						$this->buildLink('members')
					);
				}

				return $this->redirectPermanently(
					$this->buildLink('members/follow', $user)
				);
			}
		}

		$visitor = \XF::visitor();
		if ( ! $visitor->user_id) {
			return $this->redirect(
				$this->buildLink($this->buildLink('members'))
			);
		}

		return $this->redirect(
			$this->buildLink('members/followers', $visitor)
		);
	}

	protected function redirectUserFollowers()
	{
		$visitor = \XF::visitor();
		if ( ! $visitor->user_id) {
			return $this->redirect(
				$this->buildLink($this->buildLink('members'))
			);
		}

		return $this->redirectPermanently(
			$this->buildLink('members/followers', $visitor)
		);
	}

	protected function redirectToUserConfirmationResend(array $pathParts)
	{
		$visitor = \XF::visitor();
		if ( ! $visitor->user_id) {
			return $this->redirect(
				$this->buildLink($this->buildLink('account/security'))
			);
		}

		return $this->redirectPermanently(
			$this->buildLink('users/resend-confirmation', $visitor)
		);
	}

	protected function redirectToWatch(array $pathParts)
	{
		if (count($pathParts) > 2) {
			$watchItemId = $pathParts[2];
			if ( ! is_numeric($watchItemId)) {
				return $this->redirectPermanently(
					$this->buildLink('whats-new/news-feed')
				);
			}

			switch ($pathParts[1]) {
				case 'topic':
					$thread = $this->app->find('XF:Thread', (int)$watchItemId);

					return $this->redirectPermanently(
						$this->buildLink('threads/watch', $thread)
					);
				case 'forum':
					$forum = $this->app->find('XF:Forum', (int)$watchItemId);

					return $this->redirectPermanently(
						$this->buildLink('forums/watch', $forum)
					);
			}
		}

		return $this->redirectPermanently(
			$this->buildLink('whats-new/news-feed')
		);
	}

	protected function redirectToThreadCreate(array $pathParts)
	{
		// If a forum ID is provided, create the thread there. Otherwise, redirect to the generic thread create page.
		if (count($pathParts) > 1) {
			$forumIdInUrl = $pathParts[1];
			if (is_numeric($forumIdInUrl)) {
				$forum = $this->app->find('XF:Forum', (int)$forumIdInUrl);

				if ( ! $forum) {
					return $this->redirectPermanently(
						$this->buildLink('index')
					);
				}

				return $this->redirectPermanently(
					$this->buildLink('forums/post-thread', $forum)
				);
			}
		}

		return $this->redirectPermanently(
			$this->buildLink('forums/post-thread')
		);
	}

	protected function redirectToThread(array $pathParts)
	{
		// If a thread ID is provided, view it. Otherwise, return to index.
		if (count($pathParts) > 1) {
			$threadIdInUrl = $pathParts[1];
			if (is_numeric($threadIdInUrl)) {
				$thread = $this->app->find('XF:Thread', (int)$threadIdInUrl);

				if ( ! $thread) {
					return $this->redirectPermanently(
						$this->buildLink('index')
					);
				}

				if (isset($pathParts[2]) && is_numeric($pathParts[2])) {
					$page = (int)$pathParts[2];

					if ($page != 0) {
						return $this->redirectPermanently(
							$this->buildLink('threads', $thread, [
								'page' => ++$page,
							])
						);
					}
				}

				return $this->redirectPermanently(
					$this->buildLink('threads', $thread)
				);
			}
		}

		return $this->redirectPermanently(
			$this->buildLink('index')
		);
	}

	protected function redirectToThreadReportList(array $pathParts)
	{
		$threadIdInUrl = null;
		if (count($pathParts) > 1) {
			$threadIdInUrl = $pathParts[1];
		}

		if ( ! is_numeric($threadIdInUrl)) {
			return $this->redirectPermanently(
				$this->buildLink('reports')
			);
		}

		/** @var \XF\Entity\Thread $thread */
		$thread = $this->app->find('XF:Thread', (int)$threadIdInUrl);
		/** @var \XF\Finder\Report $reportFinder */
		$reportFinder = $this->app->finder('XF:Report')
			->where('content_type', 'post')
			->where('content_id', $thread->first_post_id);

		/** @var \XF\Entity\Report $report */
		$report = $reportFinder->fetchOne();

		return $this->redirectPermanently(
			$this->buildLink('reports', $report)
		);
	}

	protected function redirectToThreadAction(array $pathParts, $threadAction = '')
	{
		// If a thread ID is provided, redirect to the applicable action. Otherwise, return to index.
		if (count($pathParts) > 1) {
			$threadIdInUrl = $pathParts[1];
			if (is_numeric($threadIdInUrl)) {
				$thread = $this->app->find('XF:Thread', (int)$threadIdInUrl);

				if ( ! $thread) {
					return $this->redirectPermanently(
						$this->buildLink('index')
					);
				}

				return $this->redirectPermanently(
					$this->buildLink('threads' . $threadAction, $thread)
				);
			}
		}

		return $this->redirectPermanently(
			$this->buildLink('index')
		);
	}

	protected function redirectToPost(array $pathParts, $postAction = '')
	{
		// If a post ID is provided, redirect to the post. Otherwise, return to index.
		if (count($pathParts) > 1) {
			$postIdInUrl = $pathParts[1];
			if (is_numeric($postIdInUrl)) {
				$post = $this->app->find('XF:Post', (int)$postIdInUrl);
				if ( ! $post) {
					return $this->redirectPermanently(
						$this->buildLink('index')
					);
				}

				return $this->redirectPermanently(
					$this->buildLink('posts' . $postAction, $post)
				);
			}
		}

		return $this->redirectPermanently(
			$this->buildLink('index')
		);
	}

	protected function redirectToPostReply(array $pathParts)
	{
		// If a post ID is provided, reply to it. Otherwise, return to index.
		$postIdInUrl = null;
		if (count($pathParts) > 1) {
			$postIdInUrl = $pathParts[1];
		}

		if ( ! is_numeric($postIdInUrl)) {
			return $this->redirectPermanently(
				$this->buildLink('index')
			);
		}
		/** @var \XF\Entity\Post $post */
		$post = $this->app->find('XF:Post', (int)$postIdInUrl);
		/** @var \XF\Entity\Thread $thread */
		$thread = $this->app->find('XF:Thread', $post->thread_id);

		if ($thread) {
			return $this->redirectPermanently(
				$this->buildLink('threads/reply', $thread, [
					'quote' => $post->post_id,
				])
			);
		}
	}

	protected function redirectToCategory(array $pathParts)
	{
		$oldCategoryIdInUrl = null;
		if (count($pathParts) > 1) {
			$oldCategoryIdInUrl = $pathParts[1];
		}

		if ( ! is_numeric($oldCategoryIdInUrl)) {
			return $this->redirectPermanently(
				$this->buildLink('index')
			);
		}

		$db                 = \XF::db();
		$newCategoryIdInUrl = $db->fetchOne('
			SELECT new_id
			FROM xf_import_log
			WHERE content_type = ?
			  AND old_id = ?
		', [
				'category',
				(int)$oldCategoryIdInUrl,
			]
		);

		/** @var \XF\Entity\Category $category */
		$category = $this->app->find('XF:Category', $newCategoryIdInUrl);

		return $this->redirectPermanently(
			$this->buildLink('categories', $category)
		);
	}

	protected function redirectToForum(array $pathParts)
	{
		$forumIdInUrl = null;
		if (count($pathParts) > 1) {
			$forumIdInUrl = $pathParts[1];
		}

		if ( ! is_numeric($forumIdInUrl)) {
			return $this->redirectPermanently(
				$this->buildLink('index')
			);
		}

		/** @var \XF\Entity\Forum $forum */
		$forum = $this->app->find('XF:Forum', (int)$forumIdInUrl);

		return $this->redirectPermanently(
			$this->buildLink('forums', $forum)
		);
	}

	protected function redirectToSearch(array $pathParts)
	{
		if (count($pathParts) == 1) {
			return $this->redirectPermanently(
				$this->buildLink('search')
			);
		}

		switch ($pathParts[1]) {
			case 'poster':
				return $this->redirectToUserSearch($pathParts);
		}

		// todo: Add additional types: 'keyword', 'poster', 'search', 'morelike', 'results'.

		return $this->redirectPermanently(
			$this->buildLink('search')
		);
	}

	protected function redirectToUserSearch($pathParts)
	{
		$userId = null;
		if (count($pathParts) > 2) {
			$userId = $pathParts[2];
		}

		if ( ! is_numeric($userId)) {
			return $this->redirectPermanently(
				$this->buildLink('search')
			);
		}

		/** @var \XF\Entity\User $user */
		$user = $this->app->find('XF:User', (int)$userId);

		if ( ! $user) {
			return $this->redirectPermanently(
				$this->buildLink('search')
			);
		}

		if (count($pathParts) == 3) {
			return $this->redirectPermanently(
				$this->buildLink('members', $user) . '#recent-content'
			);
		}

		switch ($pathParts[3]) {
			case 'topics':
				return $this->redirect(
					$this->buildLink('search/member', $user, [
						'user_id' => $user->user_id,
						'content' => 'thread',
					])
				);
			case 'messages':
				return $this->redirect(
					$this->buildLink('search/member', $user, [
						'user_id' => $user->user_id,
						'content' => 'post',
					])
				);
		}

		return $this->redirectPermanently(
			$this->buildLink('search')
		);
	}

	protected function redirectToConversationCreate(array $pathParts)
	{
		$recipientUserId = null;
		if (count($pathParts) > 1) {
			$recipientUserId = end($pathParts);
		}

		if ( ! is_numeric($recipientUserId)) {
			return $this->redirectPermanently(
				$this->buildLink('conversations/add')
			);
		}

		/** @var \XF\Entity\User $recipient */
		$recipient = $this->app->find('XF:User', (int)$recipientUserId);

		if ( ! $recipient) {
			return $this->redirectPermanently(
				$this->buildLink('conversations/add')
			);
		}

		return $this->redirectPermanently(
			$this->buildLink('conversations/add', $recipient, [
				'to' => $recipient->username,
			])
		);
	}

	protected function redirectToConversation(array $pathParts)
	{
		$conversationId = null;
		if (count($pathParts) > 1) {
			$conversationId = $pathParts[1];
		}

		if ( ! is_numeric($conversationId)) {
			return $this->redirectPermanently(
				$this->buildLink('conversations')
			);
		}

		/** @var \XF\Entity\ConversationMaster $conversation */
		$conversation = $this->app->find('XF:ConversationMaster', (int)$conversationId);

		if ( ! $conversation) {
			return $this->redirectPermanently(
				$this->buildLink('conversations')
			);
		}

		return $this->redirectPermanently(
			$this->buildLink('conversations', $conversation)
		);
	}
}

// todo:
//"pm_insert_reply",
//"pm_discussion_message",
//"pm_unsubscribe",
//"pm_update_subscription",