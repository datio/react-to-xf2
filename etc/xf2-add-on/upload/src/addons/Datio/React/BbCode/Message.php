<?php

namespace Datio\React\BbCode;

use \XF\BbCode\Renderer\AbstractRenderer;

class Message
{
	public static function renderTagMessage(
		$tagChildren,
		$tagOption,
		$tag,
		array $options,
		AbstractRenderer $renderer
	) {
		$id = intval($tagOption);
		if (!$id) {
			return $renderer->renderSubTree($tagChildren, $options);
		}

		$router = \XF::app()->router('public');
		$link = $router->buildLink('posts', ['post_id' => $id]);

		return '<a href="' . htmlspecialchars($link) . '">'
			. $renderer->renderSubTree($tagChildren, $options) . '</a>';
	}
}