<?php

namespace Datio\React\Authentication;

class React extends \XF\Authentication\AbstractAuth
{
	public function generate($password)
	{
		throw new \LogicException('Cannot generate authentication for this type.');
	}

	public function authenticate($userId, $password)
	{
		if ( ! is_string($password) || $password === '' || empty($this->data)) {
			return false;
		}

		return (md5($password) == $this->data['hash']);
	}

	public function getAuthenticationName()
	{
		return 'Datio\React:React';
	}
}