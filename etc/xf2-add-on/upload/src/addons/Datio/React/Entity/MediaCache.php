<?php

namespace Datio\React\Entity;

use XF\Mvc\Entity\Structure;

/**
 * COLUMNS
 *
 * @property int|null    media_id
 * @property string|null media_hash
 * @property int|null    album_id
 * @property int|null    user_id
 * @property string|null source_file_hash
 * @property bool|null   success
 * @property string|null log
 */
class MediaCache extends \XF\Mvc\Entity\Entity
{
	public static function getStructure(Structure $structure)
	{
		$structure->table      = 'xf_datio_mfxf2_media_cache';
		$structure->shortName  = 'Datio\React:MediaCache';
		$structure->primaryKey = 'media_id';
		$structure->columns    = [
			'media_id'         => ['type' => self::UINT, 'nullable' => true],
			'media_hash'       => ['type' => self::STR, 'maxLength' => 32, 'nullable' => true],
			'album_id'         => ['type' => self::UINT, 'nullable' => true],
			'user_id'          => ['type' => self::UINT, 'nullable' => true],
			'source_file_hash' => ['type' => self::STR, 'maxLength' => 16, 'nullable' => true],
			'success'          => ['type' => self::BOOL, 'nullable' => true],
			'log'              => ['type' => self::STR, 'nullable' => true],
		];

		return $structure;
	}
}