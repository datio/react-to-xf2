<?php

namespace Datio\React\XF\Pub\Controller;

use XF\Mvc\ParameterBag;

class Conversation extends XFCP_Conversation
{
	public function actionView(ParameterBag $params)
	{
		$viewResponse = parent::actionView($params);

		if ( ! ($viewResponse instanceof \XF\Mvc\Reply\View)) {
			return $viewResponse;
		}

		foreach ($viewResponse->getparam('messages') as $key => $conversationMessage) {
			/** @var \XF\Entity\ConversationMessage $conversationMessage */
			if (strpos($conversationMessage->message, 'http') !== false
				&& preg_match('/(?:\s|^)(?:\[\*])?(?:http|www).*?(?:\s|$)/im', $conversationMessage->message)
			) {
				$bbCodeContainer = $this->app->bbCode();
				$parser          = $bbCodeContainer->parser();
				$rules           = $bbCodeContainer->rules('base');

				$processor = $bbCodeContainer->processor();
				$usage     = $bbCodeContainer->processorAction('usage');

				/** @var \XF\BbCode\ProcessorAction\LimitTags $limit */
				$limit = $bbCodeContainer->processorAction('limit');

				$processor->addProcessorAction('usage', $usage)
					->addProcessorAction('limit', $limit);

				$processor->addProcessorAction('autolink', $bbCodeContainer->processorAction('autolink'));

				$conversationMessage->message = $processor->render($conversationMessage->message, $parser, $rules,
					[]);
				$conversationMessage->save();
			}
		}

		return $viewResponse;
	}
}

// ******************** FOR IDE AUTO COMPLETE ********************
if (false) {
	class XFCP_Conversation extends \XF\Pub\Controller\Conversation
	{
	}
}