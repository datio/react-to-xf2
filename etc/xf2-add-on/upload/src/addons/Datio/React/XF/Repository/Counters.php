<?php

namespace Datio\React\XF\Repository;

class Counters extends XFCP_Counters
{
	public function getForumStatisticsCacheData()
	{
		$cache = parent::getForumStatisticsCacheData();

		$cache['users']    = $this->finder('XF:User')->total();
		$cache['threads']  = $this->finder('XF:Thread')->total();
		$cache['messages'] = $this->finder('XF:Post')->total();

		return $cache;
	}
}

// ******************** FOR IDE AUTO COMPLETE ********************
if (false) {
	class XFCP_Counters extends \XF\Repository\Counters
	{
	}
}