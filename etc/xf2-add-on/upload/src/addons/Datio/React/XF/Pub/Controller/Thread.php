<?php

namespace Datio\React\XF\Pub\Controller;

use XF\Mvc\ParameterBag;

class Thread extends XFCP_Thread
{
	public function actionIndex(ParameterBag $params)
	{
		$viewResponse = parent::actionIndex($params);

		if ( ! ($viewResponse instanceof \XF\Mvc\Reply\View)) {
			return $viewResponse;
		}

		foreach ($viewResponse->getparam('posts') as $key => $post) {
			/** @var \XF\Entity\Post $post */
			if (strpos($post->message, 'http') !== false
				&& preg_match('/(?:\s|^)(?:\[\*])?(?:http|www).*?(?:\s|$)/im', $post->message)
			) {
				$bbCodeContainer = $this->app->bbCode();
				$parser          = $bbCodeContainer->parser();
				$rules           = $bbCodeContainer->rules('base');

				$processor = $bbCodeContainer->processor();
				$usage     = $bbCodeContainer->processorAction('usage');

				/** @var \XF\BbCode\ProcessorAction\LimitTags $limit */
				$limit = $bbCodeContainer->processorAction('limit');

				$processor->addProcessorAction('usage', $usage)
					->addProcessorAction('limit', $limit);

				$processor->addProcessorAction('autolink', $bbCodeContainer->processorAction('autolink'));

				$post->message = $processor->render($post->message, $parser, $rules, []);
				$post->save();
			}
		}

		return $viewResponse;
	}
}

// ******************** FOR IDE AUTO COMPLETE ********************
if (false) {
	class XFCP_Thread extends \XF\Pub\Controller\Thread
	{
	}
}