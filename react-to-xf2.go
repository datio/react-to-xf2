// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	toml "github.com/pelletier/go-toml"

	_ "github.com/go-sql-driver/mysql" // The MySQL-MariaDB driver.
)

// The Import options are populated in main() via the CLI.
type Import struct {
	Action string
	Config *toml.Tree
}

type xf2Installation struct {
	Db  *sql.DB
	Txn *sql.Tx

	XfDir string
}

type reactInstallation struct {
	Db   *sql.DB
	Db2  *sql.DB
	Txn  *sql.Tx
	Txn2 *sql.Tx

	AttachmentDir string
	AvatarDir     string
}

var (
	err error
	xf2 *xf2Installation

	react    *reactInstallation
	reactDsn string

	currentTime      = time.Now()
	currentTimeEpoch = currentTime.Unix()
)

// Begin populates the installation related variables and initiates the database connections.
func (o *Import) Begin() error {
	xf2 = &xf2Installation{}

	xf2Dsn := fmt.Sprintf(
		"%s:%s@(%s:%d)/%s?autocommit=true&charset=utf8mb4,utf8&collation=utf8mb4_unicode_ci&parseTime=true&timeout=48h",
		o.Config.Get("XenForo2.db_user").(string),
		o.Config.Get("XenForo2.db_password").(string),
		o.Config.Get("XenForo2.db_host").(string),
		o.Config.Get("XenForo2.db_port").(int64),
		o.Config.Get("XenForo2.db_name").(string),
	)

	xf2.Db, err = sql.Open("mysql", xf2Dsn)
	if err != nil {
		return AddErrorInfo(err, "xenforo2 db")
	}

	xf2.Db.SetMaxIdleConns(0)
	// xf2.Db.SetMaxOpenConns(1)
	xf2.Db.SetConnMaxLifetime(48 * time.Hour)

	err = xf2.Db.Ping()
	if err != nil {
		return AddErrorInfo(err, "xenforo2 db")
	}

	xf2.Txn, err = xf2.Db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		_ = xf2.Txn.Rollback()
		_ = xf2.Db.Close()
	}()

	react = &reactInstallation{}

	reactDsn = fmt.Sprintf(
		"%s:%s@(%s:%d)/%s?parseTime=false&timeout=48h",
		o.Config.Get("React.db_user").(string),
		o.Config.Get("React.db_password").(string),
		o.Config.Get("React.db_host").(string),
		o.Config.Get("React.db_port").(int64),
		o.Config.Get("React.db_name").(string),
	)

	react.Db, err = sql.Open("mysql", reactDsn)
	if err != nil {
		return AddErrorInfo(err, "react db")
	}

	react.Db.SetMaxIdleConns(0)
	react.Db.SetConnMaxLifetime(48 * time.Hour)

	err = react.Db.Ping()
	if err != nil {
		return AddErrorInfo(err, "react db")
	}

	readOnly := &sql.TxOptions{ReadOnly: true}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	react.Txn, err = react.Db.BeginTx(ctx, readOnly)
	if err != nil {
		return err
	}

	err = o.routeAction()

	return err
}

func (o *Import) create2ndReactTxn() (context.CancelFunc, error) {
	react.Db2, err = sql.Open("mysql", reactDsn)
	if err != nil {
		return nil, AddErrorInfo(err, "react db")
	}

	react.Db2.SetMaxIdleConns(0)
	react.Db2.SetConnMaxLifetime(48 * time.Hour)

	readOnly := &sql.TxOptions{ReadOnly: true}

	ctx, cancel := context.WithCancel(context.Background())
	react.Txn2, err = react.Db2.BeginTx(ctx, readOnly)

	return cancel, err
}

func (o *Import) routeAction() error {
	switch o.Action {
	case "import":
		err = o.migrate()
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("the specified action does not exist")
	}

	return nil
}

// AddErrorInfo concats an error value with a custom message, forming a new error.
// The message string is appended (in parentheses) at the start of the result.
func AddErrorInfo(err error, msg string) error {
	if len(msg) > 0 {
		return fmt.Errorf("(%s) %s", msg, err.Error())
	}
	return err
}
