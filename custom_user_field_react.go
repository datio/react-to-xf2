// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

// The custom user field data.
var reactUserFields = []*userField{
	&userField{FieldID: "firstname", DisplayGroup: "personal", DisplayOrder: 10, FieldType: "textbox", FieldChoices: "[]", MatchType: "none", MatchParams: "[]", MaxLength: 15,
		UserEditable: "yes", ViewableProfile: true, ModeratorEditable: true},

	&userField{FieldID: "lastname", DisplayGroup: "personal", DisplayOrder: 20, FieldType: "textbox", FieldChoices: "[]", MatchType: "none", MatchParams: "[]", MaxLength: 15,
		UserEditable: "yes", ViewableProfile: true, ModeratorEditable: true},

	&userField{FieldID: "gender", DisplayGroup: "personal", DisplayOrder: 30, FieldType: "select", FieldChoices: `{"male":"Male","female":"Female"}`, MatchType: "none",
		MatchParams: "[]", UserEditable: "yes", ViewableProfile: true},

	&userField{FieldID: "occupation", DisplayGroup: "personal", DisplayOrder: 40, FieldType: "textbox", FieldChoices: "[]", MatchType: "none", MatchParams: "[]", MaxLength: 40,
		UserEditable: "yes", ViewableProfile: true, ModeratorEditable: true},

	&userField{FieldID: "education", DisplayGroup: "personal", DisplayOrder: 50, FieldType: "textbox", FieldChoices: "[]", MatchType: "none", MatchParams: "[]", MaxLength: 50,
		UserEditable: "yes", ViewableProfile: true, ModeratorEditable: true},

	&userField{FieldID: "subtitle", DisplayGroup: "personal", DisplayOrder: 60, FieldType: "textbox", FieldChoices: "[]", MatchType: "none", MatchParams: "[]", MaxLength: 50,
		UserEditable: "yes", ModeratorEditable: true},

	&userField{FieldID: "motorshopper", DisplayGroup: "personal", DisplayOrder: 70, FieldType: "checkbox", FieldChoices: `{"true":"Has a motorshopper profile"}`,
		MatchType: "none", MatchParams: "[]", UserEditable: "never"},

	&userField{FieldID: "motorstek", DisplayGroup: "personal", DisplayOrder: 80, FieldType: "checkbox", FieldChoices: `{"true":"Has a motorstek profile"}`, MatchType: "none",
		MatchParams: "[]", UserEditable: "never"},

	&userField{FieldID: "icq", DisplayGroup: "contact", DisplayOrder: 90, FieldType: "textbox", FieldChoices: "[]", MatchType: "regex", MatchParams: `{"regex":"^[0-9]{9}$"}`,
		MaxLength: 9, UserEditable: "yes", ViewableProfile: true},

	&userField{FieldID: "msn", DisplayGroup: "contact", DisplayOrder: 100, FieldType: "textbox", FieldChoices: "[]", MatchType: "none", MatchParams: "[]", MaxLength: 55,
		UserEditable: "yes", ViewableProfile: true, ModeratorEditable: true},

	// &userField{FieldID: "registrationEmail", DisplayGroup: "contact", DisplayOrder: 110, FieldType: "textbox", FieldChoices: "[]", MatchType: "none", MatchParams: "[]",
	// 	MaxLength: 120, UserEditable: "never"},

	&userField{FieldID: "filterNodes", DisplayGroup: "preferences", DisplayOrder: 120, FieldType: "checkbox", FieldChoices: `{"true":"Filter forums"}`, MatchType: "none",
		MatchParams: "[]", UserEditable: "never"}, // the userEditable setting can be changed to 'yes' as soon as the filtering functionality gets implemented.

	&userField{FieldID: "filterNodesSelected", DisplayGroup: "preferences", DisplayOrder: 125, FieldType: "textbox", FieldChoices: "[]", MatchType: "none", MatchParams: "[]",
		MaxLength: 250, UserEditable: "never"},

	&userField{FieldID: "filterATNodes", DisplayGroup: "preferences", DisplayOrder: 140, FieldType: "checkbox", FieldChoices: `{"true":"Filter active topic forums"}`,
		MatchType: "regex", MatchParams: `{"regex":"^[0-9]{0,3}$"}`, UserEditable: "never"},

	&userField{FieldID: "filterATNodesSelected", DisplayGroup: "preferences", DisplayOrder: 145, FieldType: "textbox", FieldChoices: "[]", MatchType: "none",
		MatchParams: "[]", MaxLength: 250, UserEditable: "never"},

	&userField{FieldID: "maxATMessagesCount", DisplayGroup: "preferences", DisplayOrder: 150, FieldType: "textbox", FieldChoices: "[]", MatchType: "regex",
		MatchParams: `{"regex":"^[0-9]{0,3}$"}`, MaxLength: 3, UserEditable: "never"},

	&userField{FieldID: "favoriteNodesSelected", DisplayGroup: "preferences", DisplayOrder: 160, FieldType: "textbox", FieldChoices: "[]", MatchType: "none",
		MatchParams: "[]", MaxLength: 250, UserEditable: "never"},

	&userField{FieldID: "showMotorshopper", DisplayGroup: "preferences", DisplayOrder: 170, FieldType: "checkbox", FieldChoices: `{"true":"Display Motorshopper reviews"}`,
		MatchType: "none", MatchParams: "[]", UserEditable: "never"},

	&userField{FieldID: "motorshopperLimit", DisplayGroup: "preferences", DisplayOrder: 175, FieldType: "textbox", FieldChoices: "[]", MatchType: "regex",
		MatchParams: `{"regex":"^[0-9]{0,3}$"}`, MaxLength: 3, UserEditable: "never"},

	&userField{FieldID: "motorshopperReviewsLimit", DisplayGroup: "preferences", DisplayOrder: 180, FieldType: "textbox", FieldChoices: "[]", MatchType: "regex",
		MatchParams: `{"regex":"^[0-9]{0,3}$"}`, MaxLength: 3, UserEditable: "never"},

	&userField{FieldID: "showMotormeuk", DisplayGroup: "preferences", DisplayOrder: 185, FieldType: "checkbox", FieldChoices: `{"true":"Display Motormeuk headlines"}`,
		MatchType: "none", MatchParams: "[]", UserEditable: "never"},

	&userField{FieldID: "showWebsiteIconInPosts", DisplayGroup: "preferences", DisplayOrder: 190, FieldType: "checkbox", FieldChoices: `{"true":"Display user website icons"}`,
		MatchType: "none", MatchParams: "[]", UserEditable: "never"},

	&userField{FieldID: "showSubtitleInPosts", DisplayGroup: "preferences", DisplayOrder: 200, FieldType: "checkbox", FieldChoices: `{"true":"Display user subtitles"}`,
		MatchType: "none", MatchParams: "[]", UserEditable: "never"},

	&userField{FieldID: "showNodesDescription", DisplayGroup: "preferences", DisplayOrder: 210, FieldType: "checkbox", FieldChoices: `{"true":"Display forum descriptions"}`,
		MatchType: "none", MatchParams: "[]", UserEditable: "never"},

	&userField{FieldID: "showBanners", DisplayGroup: "preferences", DisplayOrder: 220, FieldType: "checkbox", FieldChoices: `{"true":"Display banners"}`, MatchType: "none",
		MatchParams: "[]", UserEditable: "never"},

	&userField{FieldID: "showShopping", DisplayGroup: "preferences", DisplayOrder: 230, FieldType: "checkbox", FieldChoices: `{"true":"Display shopping categories"}`,
		MatchType: "none", MatchParams: "[]", UserEditable: "never"},

	&userField{FieldID: "euCookieReadDate", DisplayGroup: "preferences", DisplayOrder: 240, FieldType: "textbox", FieldChoices: "[]", MatchType: "regex",
		MatchParams: `{"regex":"^[0-9]{10}$"}`, MaxLength: 10, UserEditable: "never"},

	&userField{FieldID: "contentItemLimit", DisplayGroup: "preferences", DisplayOrder: 250, FieldType: "textbox", FieldChoices: "[]", MatchType: "regex",
		MatchParams: `{"regex":"^[0-9]{0,3}$"}`, MaxLength: 3, UserEditable: "never"},

	&userField{FieldID: "displayEmail", DisplayGroup: "preferences", DisplayOrder: 260, FieldType: "checkbox", FieldChoices: `{"true":"Show my e-mail address to other users"}`,
		MatchType: "none", MatchParams: "[]", UserEditable: "never"},
}

var reactUserFieldPhrases = []*phrase{
	&phrase{Title: "user_field_title.firstname", PhraseText: "First name", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.firstname", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.lastname", PhraseText: "Last name", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.lastname", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.gender_male", PhraseText: "Male", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_choice.gender_female", PhraseText: "Female", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.gender", PhraseText: "Gender", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.gender", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.occupation", PhraseText: "Occupation", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.occupation", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.education", PhraseText: "Education", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.education", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.subtitle", PhraseText: "Subtitle", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.subtitle", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.motorshopper_true", PhraseText: "Has a motorshopper profile", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.motorshopper", PhraseText: "Motorshopper", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.motorshopper", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.motorstek_true", PhraseText: "Has a motorstek profile", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.motorstek", PhraseText: "Motorstek", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.motorstek", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.icq", PhraseText: "ICQ", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.icq", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.msn", PhraseText: "MSN", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.msn", PhraseGroup: "user_field_desc"},

	// &phrase{Title: "user_field_title.registrationEmail", PhraseText: "Registration e-mail", PhraseGroup: "user_field_title"},
	// &phrase{Title: "user_field_desc.registrationEmail", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.filterNodes_true", PhraseText: "Filter forums", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.filterNodes", PhraseText: "Forum filter", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.filterNodes", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.filterNodesSelected", PhraseText: "Filtered forums", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.filterNodesSelected", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.filterATNodes_true", PhraseText: "Filter active topic forums", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.filterATNodes", PhraseText: "Forum filter", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.filterATNodes", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.filterATNodesSelected", PhraseText: "Filtered active topic forums", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.filterATNodesSelected", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.maxATMessagesCount", PhraseText: "Maximum reply count filter", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.maxATMessagesCount", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.favoriteNodesSelected", PhraseText: "Favorite forums", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.favoriteNodesSelected", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.showMotorshopper_true", PhraseText: "Display Motorshopper reviews", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.showMotorshopper", PhraseText: "Motorshopper", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.showMotorshopper", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.motorshopperLimit", PhraseText: "Motorshopper limit", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.motorshopperLimit", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.motorshopperReviewsLimit", PhraseText: "Motorshopper reviews limit", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.motorshopperReviewsLimit", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.showMotormeuk_true", PhraseText: "Display Motormeuk headlines", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.showMotormeuk", PhraseText: "Motormeuk", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.showMotormeuk", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.showWebsiteIconInPosts_true", PhraseText: "Display user website icons", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.showWebsiteIconInPosts", PhraseText: "User website icons", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.showWebsiteIconInPosts", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.showSubtitleInPosts_true", PhraseText: "Display user subtitles", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.showSubtitleInPosts", PhraseText: "User subtitles", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.showSubtitleInPosts", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.showNodesDescription_true", PhraseText: "Display forum descriptions", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.showNodesDescription", PhraseText: "Forum descriptions", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.showNodesDescription", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.showBanners_true", PhraseText: "Display banners", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.showBanners", PhraseText: "Advertisements", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.showBanners", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.showShopping_true", PhraseText: "Display shopping categories", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.showShopping", PhraseText: "Shopping", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.showShopping", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.euCookieReadDate", PhraseText: "EU cookie read timestamp", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.euCookieReadDate", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_title.contentItemLimit", PhraseText: "Maximum content items to display", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.contentItemLimit", PhraseGroup: "user_field_desc"},

	&phrase{Title: "user_field_choice.displayEmail_true", PhraseText: "Show my e-mail address to other users", PhraseGroup: "user_field_choice"},
	&phrase{Title: "user_field_title.displayEmail", PhraseText: "Display e-mail to others", PhraseGroup: "user_field_title"},
	&phrase{Title: "user_field_desc.displayEmail", PhraseGroup: "user_field_desc"},
}
