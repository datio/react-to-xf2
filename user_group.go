// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

import (
	"database/sql"
	"errors"
)

type userGroup struct {
	UserGroupID          int
	Title                string
	DisplayStylePriority int
	UsernameCSS          string
	UserTitle            string
	BannerCSSClass       string
	BannerText           string
}

// React's user group id '4' does not exist in the motorforum database, which simplifies the mapping.
// All the imported usergroup IDs, over 4, will match to their originals.
var reactUserGroupToXf2Map = map[int]int{
	1: 2, // Registered
	2: 3, // Administrative
	3: 4, // Moderating
}

func (o *Import) importUserGroups() error {
	doImport, ok := o.Config.Get("mfxf2.import_user_groups").(bool)
	if !ok {
		return errors.New("could not find value of 'mfxf2.import_user_groups' in config")
	}

	if !doImport {
		return nil
	}

	userGroupsToInsert := []*userGroup{}

	var (
		userGroupRows *sql.Rows
		usergroupID   int
		name          string
	)

	userGroupRows, err = react.Txn.Query(`
        SELECT UsergroupID,
               Name            # The group title.
        FROM F_Groups
        WHERE UsergroupID <> 4 # See user_group_react.go
    `)
	if err != nil {
		return err
	}

	for userGroupRows.Next() {
		err = userGroupRows.Scan(&usergroupID, &name)
		if err != nil {
			return err
		}

		mappedID, ok := reactUserGroupToXf2Map[usergroupID]
		if !ok {
			mappedID = usergroupID
		}

		userGroupsToInsert = append(userGroupsToInsert, &userGroup{
			UserGroupID: mappedID,
			Title:       name,
		})
	}

	// Insert.
	for _, ug := range userGroupsToInsert {
		_, err = xf2.Txn.Exec(`
            INSERT INTO xf_user_group
            SET user_group_id = ?,
                title = ?,
                display_style_priority = ?,
                username_css = ?,
                user_title = ?,
                banner_css_class = ?,
                banner_text = ?
            ON DUPLICATE KEY
            UPDATE title = ?
        `, ug.UserGroupID, ug.Title, ug.DisplayStylePriority, ug.UsernameCSS, ug.UserTitle, ug.BannerCSSClass, ug.BannerText,
			ug.Title,
		)
	}

	return err
}
