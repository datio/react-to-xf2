// Copyright 2017 Motormedia. All rights reserved.

package mfxf2

import (
	"regexp"
	"strconv"
	"strings"
)

type unifiedDiff struct {
	FindHeader    string
	ReplaceHeader string
	Find          string
	Replace       string
}

type contentLine struct {
	DiffType     string // '+' '-' '!' ' '.
	LineContents string
}

func (o *Import) patchUnified(content string, patch string, diffID int) string {
	contentLines := []*contentLine{}
	lines := strings.Split(content, "\n")
	for _, contents := range lines {
		contentLines = append(contentLines, &contentLine{
			LineContents: contents,
		})
	}

	diffs := o.parsePatch(patch)
	newContentLines := o.patchContentWithDiffs(contentLines, diffs, diffID)

	newContent := ""
	for i, newContentLine := range newContentLines {
		if i != 0 {
			newContent += "\n"
		}

		if newContentLine == nil {
			continue
		}

		newContent += newContentLine.LineContents
	}

	return newContent
}

var lastLineReplacerRe = regexp.MustCompile(`(?s)\n$`)

func (o *Import) parsePatch(patch string) []*unifiedDiff {
	patch = lastLineReplacerRe.ReplaceAllLiteralString(patch, "")
	patchLines := strings.Split(patch, "\n")

	diffs := []*unifiedDiff{}

	var currentBlockIsFind bool
	for _, patchLine := range patchLines {

		if strings.HasPrefix(patchLine, "*** /tmp") || strings.HasPrefix(patchLine, "--- /tmp") {
			continue
		}

		if strings.HasPrefix(patchLine, "***************") {
			diffs = append(diffs, &unifiedDiff{})
			continue
		}
		if len(diffs) == 0 {
			continue
		}

		if strings.HasPrefix(patchLine, "*** ") {
			diffs[len(diffs)-1].FindHeader = patchLine
			currentBlockIsFind = true
			continue
		}

		if strings.HasPrefix(patchLine, "--- ") {
			diffs[len(diffs)-1].ReplaceHeader = patchLine
			currentBlockIsFind = false
			continue
		}

		if strings.HasPrefix(patchLine, `\ `) {
			continue
		}

		if currentBlockIsFind {
			if len(diffs[len(diffs)-1].Find) > 0 {
				diffs[len(diffs)-1].Find += "\n"
			}
			diffs[len(diffs)-1].Find += patchLine
			continue
		}

		if len(diffs[len(diffs)-1].Replace) > 0 {
			diffs[len(diffs)-1].Replace += "\n"
		}
		diffs[len(diffs)-1].Replace += patchLine
	}

	return diffs
}

func (o *Import) patchContentWithDiffs(contentLines []*contentLine, diffs []*unifiedDiff, diffID int) []*contentLine {
	for i := len(diffs) - 1; i >= 0; i-- {
		replaceFrom, replaceTo := o.extractRange(diffs[i].ReplaceHeader)
		replaceLines := strings.Split(diffs[i].Replace, "\n")

		totalReplaceHeader := replaceTo - replaceFrom + 1
		findFrom, findTo := o.extractRange(diffs[i].FindHeader)

		simpleReplace := false
		if totalReplaceHeader > 0 && totalReplaceHeader == len(replaceLines) && replaceLines[0] != "" {
			// Replace by replace block.
			if findTo > len(contentLines) {
				findTo = len(contentLines)
			}

			if findFrom > findTo {
				temp := findFrom
				findFrom = findTo
				findTo = temp
			}

			if findFrom == 0 {
				findFrom = replaceFrom
				findTo = replaceTo
			}

			if len(contentLines) < findTo || findFrom == 0 {
				continue
			}

			contentLines = append(contentLines[:findFrom-1], contentLines[findTo:]...)

			for y := len(replaceLines) - 1; y >= 0; y-- {
				contentLines = append(contentLines, &contentLine{})
				copy(contentLines[findFrom-1+1:], contentLines[findFrom-1:])

				contentLines[findFrom-1] = &contentLine{
					LineContents: replaceLines[y],
				}
			}
		} else {
			// Replace by find block.
			findLines := strings.Split(diffs[i].Find, "\n")
			findLinesIndex := len(findLines) - 1

			for y := findTo; y >= findFrom; y-- {
				simpleReplace = true

				if y > len(contentLines) {
					y = len(contentLines)
				}

				contentLines[y-1].LineContents = findLines[findLinesIndex]

				findLinesIndex--
				if findLinesIndex < 0 {
					break
				}
			}
		}

		if simpleReplace {
			if replaceTo < findTo {
				replaceTo = findTo
			}
			if replaceFrom > findFrom {
				replaceFrom = findFrom
			}
		}

		for z := replaceTo - 1; z >= replaceFrom-1; z-- {
			if z > len(contentLines)-1 || z < 0 {
				continue
			}

			diffType := ""
			if strings.HasPrefix(contentLines[z].LineContents, "+ ") {
				diffType = "+"
			} else if strings.HasPrefix(contentLines[z].LineContents, "! ") {
				diffType = "!"
			} else if strings.HasPrefix(contentLines[z].LineContents, "- ") {
				diffType = "-"
			} else if strings.HasPrefix(contentLines[z].LineContents, "  ") {
				diffType = " "
			} else {
				diffType = ""
			}

			if diffType != "" {
				switch diffType {
				case "+", "!", " ":
					contentLines[z].LineContents = contentLines[z].LineContents[2:]
				case "-":
					if len(contentLines) > 1 {
						contentLines = append(contentLines[:z], contentLines[z+1:]...)
					} else {
						contentLines = []*contentLine{}
					}
				}
			}
		}
	}

	return contentLines
}

var diffLinesRangeRe = regexp.MustCompile("([0-9]+)(?:,([0-9]+))?")

func (o *Import) extractRange(r string) (int, int) {
	submatch := diffLinesRangeRe.FindStringSubmatch(r)

	if len(submatch) != 3 {
		return 0, 0
	}

	from, _ := strconv.Atoi(submatch[1])
	to, _ := strconv.Atoi(submatch[2])

	if to == 0 {
		to = from
	}

	return from, to
}
