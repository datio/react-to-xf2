package mfxf2

import (
	"fmt"
	"regexp"
	"strings"
)

type parsedBBCode struct {
	Name  string
	Depth int
}

func (o *Import) normalizeSlashOnlyClosingTag(input string) string {
	if !strings.Contains(input, "[/]") {
		return input
	}

	var parsedBBCodeTree = []*parsedBBCode{}
	var slashReplaceMap = []string{}

	foundStartBracket := false
	foundStartBracketPosition := 0
	depth := 0

	for pos, char := range input {
		c := fmt.Sprintf("%c", char)
		if c == "[" && !foundStartBracket {
			foundStartBracket = true
			foundStartBracketPosition = pos
		} else if c == "]" && foundStartBracket {
			found := input[foundStartBracketPosition+1 : pos]
			isClosingTag := strings.HasPrefix(found, "/")

			if isClosingTag {
				depth--
			}

			parsedBBCodeTree = append(parsedBBCodeTree, &parsedBBCode{Name: found, Depth: depth})

			if !isClosingTag {
				depth++
			}

			foundStartBracket = false
		}
	}

	for i, bb := range parsedBBCodeTree {
		if bb.Name == "/" {
			previousBB := i - 1
			for {
				if previousBB == -1 {
					break
				}

				if parsedBBCodeTree[i].Depth == parsedBBCodeTree[previousBB].Depth {
					strI := strings.Index(parsedBBCodeTree[previousBB].Name, "=")
					if strI != -1 {
						parsedBBCodeTree[previousBB].Name = parsedBBCodeTree[previousBB].Name[:strI]
					}

					slashReplaceMap = append(slashReplaceMap, parsedBBCodeTree[previousBB].Name)
					break
				}

				previousBB--
			}
		}
	}

	for _, replacement := range slashReplaceMap {
		input = strings.Replace(input, "[/]", fmt.Sprintf("[/%s]", replacement), 1)
	}

	return input
}

var imgBBSmileyRe = regexp.MustCompile(`(?i)\[img\]\S*?\/global\/smileys\/(\S*?)(?:\..{3,4})?\[\/img\]`)
var multipleLineRe = regexp.MustCompile(`(\n){2,}`)
var prefixWhitespaceRe = regexp.MustCompile(`(?m)^\s+`)
var punctuationReplacer = strings.NewReplacer(" ,", ",", " !", "!", " .", ".", " ?", "?", "[br]", "\n")
var semicolonRe = regexp.MustCompile(`\s(:\s)`)

func (o *Import) fixWhitespaceAndPunctuation(input string, replaceMultilines bool) string {
	input = imgBBSmileyRe.ReplaceAllString(input, ":$1:")

	input = punctuationReplacer.Replace(input)
	input = prefixWhitespaceRe.ReplaceAllString(input, "")

	if replaceMultilines {
		input = multipleLineRe.ReplaceAllString(input, "$1")
	}

	return semicolonRe.ReplaceAllString(input, "$1")
}

var titleBBCodeReplacer = regexp.MustCompile(`(?i)\[\/?(?:b|i|large|img)?\]|\[/[0-9a-zA-Z]*?\]|img\]|\.$`)
var sTagReplacer = strings.NewReplacer(`[s]`, "", `[/s]`, "", `[S]`, "", `[/S]`, "")

func (o *Import) removeTitleBBCodes(input string) string {
	if strings.Contains(input, `[/s]`) || strings.Contains(input, `[/S]`) {
		input = sTagReplacer.Replace(input)
	}

	return titleBBCodeReplacer.ReplaceAllString(input, "")
}

var liBBCodeRe = regexp.MustCompile(`(?i)(\[\/?li\])`)

func (o *Import) convertListRelatedBBCodes(input string) string {
	return liBBCodeRe.ReplaceAllStringFunc(input, o.liReplacer)
}

func (o *Import) liReplacer(src string) string {
	if strings.Contains(src, `/`) {
		return ""
	}

	return `[*]`
}

var smallBBCodeRe = regexp.MustCompile(`(?i)(\[small\])`)
var largeBBCodeRe = regexp.MustCompile(`(?i)(\[(?:large|big)\])`)
var smallLargeEndingBBCodeRe = regexp.MustCompile(`(?i)(\[\/(?:small|large)\])`)

func (o *Import) convertSizeRelatedBBCodes(input string) string {
	input = smallBBCodeRe.ReplaceAllString(input, "[size=2]")
	input = largeBBCodeRe.ReplaceAllString(input, "[size=5]")

	return smallLargeEndingBBCodeRe.ReplaceAllString(input, `[/size]`)
}

var noHTMLBBCodeRe = regexp.MustCompile(`(?i)(\[\s*/?\s*nohtml\s*\])`)
